<?php
/*
Plugin Name: WPLMS BrainCert Classes
Plugin URI: https://wpgenius.in
Description: A WPLMS BrainCert Classes Addon is custom plugin written for client Anas Mokayad. This plugin will enable scope for managing BrainCert Virtual class for WPLMS courses. Insctructor can manage classes from front end & eligible student can join them.
Version: 1.0
Author: Team WPGenius (Makarand Mane)
Author URI: https://tycheventures.com
Text Domain: wplms-braincert
*/
/*
Copyright 2019  Team WPGenius  (email : makarand@wpgenius.in)
*/

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

define( 'WBC_DIR_URL', plugin_dir_url( __FILE__ ) );
define( 'WBC_DIR_PATH', plugin_dir_path( __FILE__ ) );

include_once 'includes/class.braincert-init.php';
include_once 'includes/class.database.php';
include_once 'includes/class.wbc-ajax.php';
include_once 'includes/class.wbc-admin.php';
include_once 'includes/class.wbc-settings.php';
include_once 'includes/class.wbc-actions.php';
include_once 'includes/shortcodes/shortcode-class.php';
include_once 'includes/widgets/braincert-calender.php';
include_once 'includes/widgets/braincert-classes.php';

// Add text domain
add_action('plugins_loaded','wplms_braincert_translations');
function wplms_braincert_translations(){
    $locale = apply_filters("plugin_locale", get_locale(), 'wplms-braincert');
    $lang_dir = dirname( __FILE__ ) . '/languages/';
    $mofile        = sprintf( '%1$s-%2$s.mo', 'wplms-braincert', $locale );
    $mofile_local  = $lang_dir . $mofile;
    $mofile_global = WP_LANG_DIR . '/plugins/' . $mofile;

    if ( file_exists( $mofile_global ) ) {
        load_textdomain( 'wplms-braincert', $mofile_global );
    } else {
        load_textdomain( 'wplms-braincert', $mofile_local );
    }  
}

if(class_exists('WPLMS_BrainCert_Actions'))
 	WPLMS_BrainCert_Actions::init();

if(class_exists('WPLMS_BrainCert_Ajax'))
 	WPLMS_BrainCert_Ajax::init();

if(class_exists('WPLMS_BrainCert_Admin') && is_admin())
 	WPLMS_BrainCert_Admin::init();

if(class_exists('WPLMS_BrainCert_Settings'))
 	WPLMS_BrainCert_Settings::init();

register_activation_hook( 	__FILE__, array( $wbcdb, 'activate_braincert' 	) );
register_deactivation_hook( __FILE__, array( $wbcdb, 'deactivate_braincert' ) );
register_activation_hook( __FILE__, function(){ register_uninstall_hook( __FILE__, array( 'WPLMS_BrainCert_DB', 'uninstall_braincert' ) ); });