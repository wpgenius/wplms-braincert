<?php
/**
 *
 * @class       WPLMS_BrainCert_Admin
 * @author      Team WPGenius (Makarand Mane)
 * @category    Admin
 * @package     WPLMS-BrainCert/includes
 * @version     1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if(!class_exists('WP_List_Table')){
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

if( !defined( 'BACKEND_URL' ) )
	define( "BACKEND_URL", get_bloginfo('url').'/wp-admin/' ); 

class WPLMS_BrainCert_Admin extends WPLMS_BrainCert_API{

	public static $instance;
	public $wbc_screen;
	public $wbc_screen_add_class;
	public static function init(){

	    if ( is_null( self::$instance ) )
	        self::$instance = new WPLMS_BrainCert_Admin();
	    return self::$instance;
	}

	private function __construct(){
		//Admin scripts
		add_action('admin_enqueue_scripts', array($this,'braincert_dashboard_scripts') );		
		//All classes - style
		add_action('admin_print_styles-toplevel_page_braincert_classes', array($this,'braincert_dashboard_style') );
		//Schedule class action for WPLMS course
		add_filter( 'page_row_actions',		array($this,'wplms_class_schedule_link'), 10, 2 );
		
		add_action('admin_menu', 			array($this,'braincert_classes_menu'),	10);
		add_filter('set-screen-option', 	array($this,'wbc_set_screen_option'), 10,	3);	
		
	} // END public function __construct
	
	

	public function braincert_dashboard_scripts( $hook_suffix ) {
		
		//All classes - dashboard
		if( $hook_suffix === $this->wbc_screen ) {
			wp_enqueue_style( 'select2', WC()->plugin_url() . '/assets/css/select2.css' );
			wp_enqueue_script( 'wbc-manage', WBC_DIR_URL.'assets/js/wbc-manage.js' ,array( 'jquery', 'select2' ));
			wp_localize_script( 'wbc-manage', 'wbc_manage', $this->get_localise( 'manage' ) );

		}
		//Add/edit class page
		if( $hook_suffix === $this->wbc_screen_add_class ) {	
			//CSS
			wp_enqueue_style( 'select2', WC()->plugin_url() . '/assets/css/select2.css' );
			wp_enqueue_style( 'jquery-ui-calendar', WBC_DIR_URL.'assets/css/jquery-ui-calendar.css' );
			wp_enqueue_style( 'jquery-timepicker', WBC_DIR_URL.'assets/css/jquery.timepicker.css' );
			wp_enqueue_style( 'wbc-form-admin', WBC_DIR_URL.'assets/css/wbc-form-admin.css', array( 'select2', 'jquery-ui-calendar', 'jquery-timepicker' ) );
			//JS
			wp_enqueue_script( 'jquery-ui-datepicker' ); 
			wp_enqueue_script( 'jquery-timepicker' ,WBC_DIR_URL.'assets/js/jquery.timepicker.js' );
			wp_enqueue_script( 'wbc-forms', WBC_DIR_URL.'assets/js/wbc-forms.js' ,array( 'jquery', 'select2', 'jquery-ui-datepicker', 'jquery-timepicker'));
			wp_localize_script( 'wbc-forms', 'wbc_form', $this->get_localise( 'class' ) );
		}

		wp_enqueue_script( 'modal-popup', WBC_DIR_URL.'assets/js/bootstrap-modal.js',array('jquery'));
		wp_enqueue_script( 'jquery-confirm', WBC_DIR_URL.'assets/js/jquery-confirm.js',array('jquery'));
		wp_enqueue_style( 'jquery-confirm', WBC_DIR_URL.'assets/css/jquery-confirm.css' );


		
	}
	
	public function wplms_class_schedule_link( $actions, $post ){
		if( $post->post_type == 'course' ){
			$actions['schedule'] = sprintf('<a href="'.BACKEND_URL.'admin.php?page=add_class&course_id=%1$s">'.__( 'Schedule Class', 'wplms-braincert' ).'</a>', $post->ID);
		}		
		return $actions;
	}
	
	public function braincert_dashboard_style(){
		?>
        <style type="text/css">
			th#class_id 	{ width: 45px; }
			th#class_title 	{ width: 100px; }
			th#course 		{ width: 120px;}
			th#date 		{ width: 80px; }
			th#start_time 	{ width: 86px; }
			th#end_time 	{ width: 49px; }
			th#end_date 	{ width: 60px; }
			th#access 		{ width: 36px; }
			th#record 		{ width: 43px; }
			th#shortcode 	{ width: 90px; }
			th#type 		{ width: 25px; }
			th#status 		{ width: 80px; }
			th#instructor 	{ width: 75px; }
			th#duration 	{ width: 55px; }

			span.public {
			    font-size: 25px;
			    color: #4cae4c;
			}

			span.private {
			    font-size: 25px;
			    color: #a94442;
			}
			
			.select2-wrap{ margin-right:6px;}

			td.record.column-record { text-align: center; }
			td.access.column-access { text-align: center; }

			.copy-click {
			  position: relative;
			  padding-bottom: 2px;
			  text-decoration: none;
			  cursor: copy;
			  color: #484848;
			}
			.copy-click:after {
			  content: attr(data-tooltip-text);
			  position: absolute;
			  bottom: calc(100% + 6px);
			  left: 50%;
			  padding: 8px 16px;
			  white-space: nowrap;
			  opacity: 0;
			  background-color: #b0e0e6;
    		  color: #000000;
			}
			.copy-click.is-hovered:after {
			  box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
			  opacity: 1;
			  -webkit-transform: translate(-50%, 0);
			          transform: translate(-50%, 0);
			}
			.copy-click.is-copied:after {
			  content: attr(data-tooltip-text-copied);
			}

			.wbc-loading {
			    position: fixed;
			    top: 0;
			    bottom: 0;
			    left: 0;
				background: rgba(246, 246, 246, 0.5);
			    right: 0;
			    z-index: 999;
			    text-align: center;
			}

			img#loading-image {
			    width: 50px;
				position: absolute;
				top: 45%;
				height: auto;
			}

			tr.attendance-list {
			    text-align: center;
			}

			table.wbc-report {
			    width: 100%;
			}

			.video-play-area {
			    text-align: center;
			}

			td.center {
			    text-align: center;
			    padding: 30px;
			}

			table.wbc-record {
			    width: 100%;
			}

			.wbc_actions{
				padding-right: 5px;
    			padding-left: 5px;
			}

			button.download {
			    border: none;
			    background: none;
			    color: #0073aa;
			    cursor: pointer;
			}

			button.download:focus {
			    outline: none;
			}

			.download_report {
			    text-align: center;
			    padding-top: 15px;
			}

			.download_report.course_class_report {
			    padding-top: 15px;
			    padding-bottom: 0;
			}

			table.wbc-course-report {
			    width: 100%;
			}

			table.wbc-course-report td, table.wbc-course-report th {
			    padding: 5px 3px;
			    text-align: center;
			    border-bottom: 1px solid #ddd;
			}
			.course_class_report.download_report > .download.button {
			    margin-top: 0;
			}

			.download_report.course_class_report.course-report {
			    padding-top: 0;
			    padding-right: 10px;
			}
		</style>

		<?php
	}

	/*************END**************/
	function braincert_classes_menu(){
		
		$this->wbc_screen = add_menu_page(
			__('Braincert Classes dashboard','wplms-braincert' ), // page title
			__('Braincert','wplms-braincert' ), // menu title
			'manage_options', // capability
			'braincert_classes', // menu slug
			'', // callback function
			'dashicons-rest-api',
			7
		);

		add_submenu_page(
			'braincert_classes',
			__('All braincert Classes','wplms-braincert' ), // page title
			__('All Classes','wplms-braincert' ), // menu title
			'manage_options', // capability
			'braincert_classes', // menu slug
			array( $this, 'braincert_classes_dashboard')
		);

		$this->wbc_screen_add_class = add_submenu_page(
			'braincert_classes',
			__('Add braincert Class','wplms-braincert' ), // page title
			__('Add Class','wplms-braincert' ), // menu title
			'manage_options', // capability
			'add_class', // menu slug
			array( $this, 'braincert_add_classes')
		);

		add_action( "load-".$this->wbc_screen, array( $this,'braincert_screen_options' ) );
	}

	public function braincert_screen_options(){

		$screen = get_current_screen();
 
		if(!is_object($screen) || $screen->id != $this->wbc_screen)
			return;
	 
		$args = array(
			'label' => __('Clases per page', 'wplms-braincert'),
			'default' => 10,
			'option' => 'classes_per_page'
		);
		add_screen_option( 'per_page', $args );
	
	}

	public function wbc_set_screen_option( $status, $option, $value ){
		if ( 'classes_per_page' == $option ) return $value;
	}
	
	public function process_bulk_action( $action, $current_action  ){
		 // security check!
        if ( isset( $_GET['_wpnonce'] ) && ! empty( $_GET['_wpnonce'] ) ) {

            $nonce  = filter_input( INPUT_GET, '_wpnonce', FILTER_SANITIZE_STRING );
            if ( ! wp_verify_nonce( $nonce, $action ) )
                wp_die( __( 'Nope! Security check failed!', 'wplms-braincert') );

        }
        
        switch ( $current_action ) {
        	
            case 'delete':
				if( isset( $_GET['class_id'] ) && !empty( $_GET['class_id'] ) ){
					
					foreach( $_GET['class_id'] as $class_id ){
						$data = array(
									'cid'		=>	$class_id,
									'format'	=>	'',
								);
						$status = $this->removeclass( $data );	
						$this->delete_class( $class_id );
					}					
				}
				wp_safe_redirect( wp_get_referer() );
                break;

            case 'cancel':
				if( isset( $_GET['class_id'] ) && !empty( $_GET['class_id'] ) ){
					
					foreach( $_GET['class_id'] as $class_id ){
						$data = array(
									'cid'		=>	$class_id,
									'format'	=>	'',
								);
						$status = $this->cancelclass( $data );	
						$this->cancel_class( $class_id );
					}					
				}
				wp_safe_redirect( wp_get_referer() );
                break;


            default:
                // do nothing or something else
                return;
                break;
        }

        return;
	}
	
	//callback function for dashboard admin menu
	public function braincert_classes_dashboard(){
		$s= '';

		if(isset( $_GET['s'] ) && $_GET['s'] )
			$s = esc_attr( $_GET['s'] );

		//Create an instance of our package class...
		$WAF_table = new WPLMS_BrainCert_Table();
		//Fetch, prepare, sort, and filter our data...
		$WAF_table->prepare_items();
		?>
			<div class="wrap">            
         
                <h1 class="wp-heading-inline"><?php echo esc_html( get_admin_page_title() ); ?></h1>
                <hr class="wp-header-end">
                <!-- Forms are NOT created automatically, so you need to wrap the table in one to use features like bulk actions -->

                <form id="form-search-filter" method='get' action="<?php $_SERVER['HTTP_REFERER']; ?>">
                    <p class="search-box">
                        <label class="screen-reader-text" for="application-input"><?php _e( 'Search by class title or class id:', 'wplms-braincert') ;?></label>

                        <input type='search' id="class-input" name='s' size="35" placeholder="<?php _e( 'Search by class title or class id', 'wplms-braincert') ;?>" value='<?php echo $s ; ?>'></label>
                        <input type='submit' id="search-submit" class="button" value='<?php _e( 'Search', 'wplms-braincert') ;?>' >
                    </p>
                    <input type="hidden" name="page" value="<?php echo $_GET['page']; ?>" >
                    <input type="hidden" name="filter" value="all" >
                </form>
                <form action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="post" class="">
	                <div class="alignleft select2-wrap">
					<?php
						$course_counts	= $this->get_courses_counts( array() );
						$courses 		= $this->get_courses();
	                    if ( $courses->have_posts() ) {
	                        echo '<select name="courseId" id="course_filter" class="wbc_select2">';
		                        echo "<option value='all'>".__( "All Courses", 'wplms-braincert')."  (".$course_counts['all'].")</option>";
		                        while ( $courses->have_posts() ) {
		                            $courses->the_post();
		                            echo "<option value=".get_the_ID().">".get_the_title( get_the_ID() )." (".( isset($course_counts[get_the_ID()] ) ? $course_counts[get_the_ID()] : '0').")</option>";
		                        }
	                        echo '</select>';
	                    }
	                    wp_reset_postdata();
	                ?>
	            	</div>
	            
					<input type="hidden" name="security" value="<?php echo wp_create_nonce( 'download_courseattendace_nonce' ); ?>">
					<input type="hidden" name="action" value="download_courseattendace_report">
					<div class="download_report course_class_report course-report alignleft actions"><button type="submit" class="download button"><?php _e( "Download Report", 'wplms-braincert' ); ?></div></button>
				</form> 

                <form id="application-filter" method="get" class="alignleft actions">
                    <input type="hidden" name="page" value="braincert_classes">
                    <?php $WAF_table->display(); ?>
                </form>

				<br class="clear">
                <div class="wbc-loading hidden">
                    <img id="loading-image" src="<?php echo WBC_DIR_URL.'assets/images/loading.gif' ?>"/>
                </div>
			</div>            
		<?php
		
	}

	function braincert_add_classes(){
		
		$class_id		= 	isset( $_GET['class_id'] ) && !empty( $_GET['class_id'] )	? $_GET['class_id'] :'';
		$instructors 	= 	$this->get_instructors();
		$locations 		= 	$this->get_locations();
		$courses 		= 	$this->get_courses( array( 'all' => true ) );
		$timezones 		= 	$this->get_timezones();
		$class_repeates = 	$this->get_class_repeat_types();
		$languages 		= 	$this->get_languages();
		$access 		= 	$this->get_whocansee();
		$class_type 	= 	$this->get_class_types();
		$currency_arr	=	$this->get_currency();
		$front			= 	false;
		
		if( !isset( $_GET['class_id'] ) ){
			?><h1 class="wp-heading-inline"><?php echo esc_html( get_admin_page_title() ); ?></h1><?php
		}		
		
		$data = $this->_form_data( $class_id );
        extract( $data );		
		
		include(WBC_DIR_PATH.'templates/form/wbc-class-shedule.php');		
	}

} // END class WPLMS_BrainCert_Admin


class WPLMS_BrainCert_Table extends WP_List_Table {

	var $orderby	= 'id';
	var $order		= 'ASC';
	var $per_page	= '20';
	var $search		= '';
	private $status 	= 'all';
	private	$course 	= 'all';
	private $instructor = 'all';
	private	$type 		= 'all';
	private	$whocansee 	= 'all';
	
	private	$date_format 	= 'Y-m-d';
	private	$time_format 	= 'H:i:s';
	private	$timezone	 	= '';
	
	private $status_filters, $type_filters, $access_filters, $instructors ,$course_filters;	
	private $wbc_api;

    function __construct(){
        //global $status, $page;
		
		$this->wbc_api = WPLMS_BrainCert_Admin::$instance;
		
		$this->date_format 	= get_option( 'date_format' );
		$this->time_format 	= get_option( 'time_format' );
		$this->timezone		= get_option( 'timezone_string' );

		$this->status 		= isset( $_GET['status'] ) 		? sanitize_text_field( $_GET['status'] ) : 'all';
		$this->course 		= isset( $_GET['course'] ) 		? sanitize_text_field( $_GET['course'] ) : 'all';
		$this->instructor	= isset( $_GET['instructor'] )	? sanitize_text_field( $_GET['instructor'] ) : 'all';
		$this->type 		= isset( $_GET['type'] ) 		? sanitize_text_field( $_GET['type'] ) : 'all';
		$this->whocansee	= isset( $_GET['access'] ) 		? sanitize_text_field( $_GET['access'] ) : 'all';
		
		$this->status_filters 	= $this->wbc_api->get_statuses();
		$this->type_filters 	= $this->wbc_api->get_class_types();
		$this->access_filters 	= $this->wbc_api->get_whocansee();
		$this->instructors		= $this->wbc_api->get_instructors();
		$this->course_filters 	= $this->wbc_api->get_courses();
                
        //Set parent defaults
        parent::__construct( array(

            'singular'  => __( 'Braincert Class', 	'wplms-braincert'),     //singular name of the listed records
            'plural'    => __( 'Braincert Classes', 'wplms-braincert'),    //plural name of the listed records
            'ajax'      => false ,    //does this table support ajax?
			//'screen'   => isset( $args['screen'] ) ? $args['screen'] : null,
        
        ) );

    }

	//student name, date of submission, date of approval or rejection, application linked course, application files 
    function get_columns(){
        $columns = array(
            'cb'        		=>	'<input type="checkbox" />', //Render a checkbox instead of text
            'class_id'     		=>  __('Class Id', 'wplms-braincert'),
            'class_title' 		=>  __('Class Title', 'wplms-braincert'),
            'course' 			=>  __('Course Title', 'wplms-braincert'),
            'date'				=>  __('Date', 'wplms-braincert'),
            'start_time' 		=>  __('Start Time', 'wplms-braincert'),
            'end_time' 			=>  __('End Time', 'wplms-braincert'),
            'end_date'			=>  __('End Date', 'wplms-braincert'),
            'access'			=>  __('Access', 'wplms-braincert'),
            'record'			=>  __('Records', 'wplms-braincert'),
            'shortcode'			=>  __('Shortcode', 'wplms-braincert'),
            'type' 				=>  __('Type', 'wplms-braincert'),
            'status' 			=>  __('Status', 'wplms-braincert'),
            'instructor' 		=>  __('Instructor', 'wplms-braincert'),
            'duration' 			=>  __('Duration', 'wplms-braincert'),
        );
        return $columns;
    }
	
	
	function extra_tablenav( $which = '' ) {

		if($which != 'top')
			return;
			
		$course_counts			= $this->wbc_api->get_courses_counts( array() );
		$args = array(
					'where' => $this->wbc_api->_where( 
								array(
									'status'	=> $this->status,
									'course_id'	=> $this->course,
									'instructor'=> $this->instructor,
									'type'		=> $this->type,
									'whocansee'	=> $this->whocansee,
									'search'	=> $this->search,
								)
							)
					);
		$status_counts			= $this->wbc_api->get_statuses_counts( $args );
		$instructor_counts		= $this->wbc_api->get_instructor_counts( $args );
		$access_counts			= $this->wbc_api->get_whocansee_counts( $args );
		$type_counts			= $this->wbc_api->get_class_type_counts( $args );
		
		?>
        <div id="filter-application" class="alignleft actions">
            <!-- For plugins, we also need to ensure that the form posts back to our current page -->
            <select id="status_filter" name="status" >
                <?php 
                foreach( $this->status_filters as $value => $label ){
                    echo '<option value="'.$value.'" '.selected( $this->status, $value,false ).'" >'.$label.' ('.( isset($status_counts[$value] ) ? $status_counts[$value] : "0").')</option>';
                }
                ?>
            </select>  
            <div class="alignleft select2-wrap">
				<?php
                    if ( $this->course_filters->have_posts() ) {
                        echo '<select name="course" id="course_filter" class="wbc_select2">';
                        echo "<option value='all'>".__( "All Courses", 'wplms-braincert')."  (".$course_counts['all'].")</option>";
                        while ( $this->course_filters->have_posts() ) {
                            $this->course_filters->the_post();
                            echo "<option value=".get_the_ID()."".selected( $this->course , get_the_ID() ).">".get_the_title( get_the_ID() )." (".( isset($course_counts[get_the_ID()] ) ? $course_counts[get_the_ID()] : '0').")</option>";
                        }
                        echo "</select>";
                    }
                    wp_reset_postdata();
                ?>
            </div>
			<div class="alignleft select2-wrap">
                <select id="instructor_filter" name="instructor" class="wbc_select2">
                    <?php 
                        echo '<option value="all">'.__( "All Instructors", 'wplms-braincert').' ('.$instructor_counts['all'].')</option>'; 
                        foreach( $this->instructors as $instructor ){
                            echo '<option value="'.$instructor->ID.'" '.selected( $this->instructor , $instructor->ID, false ).'" >'.$instructor->display_name.' ('.( isset($instructor_counts[$instructor->ID] ) ? $instructor_counts[$instructor->ID] : "0").')</option>';
                        }
                    ?>
                </select>
            </div>
            
            <select id="type_filter" name="type" >
                <?php 
                    echo '<option value="all">'.__( "All", 'wplms-braincert').' ('.$type_counts['all'].')</option>';
                    foreach( $this->type_filters as $value => $label ){
                        echo '<option value="'.$value.'" '.selected( $this->type, $value,false ).'>'.$label.' ('.( isset($type_counts[$value] ) ? $type_counts[$value] : "0").')</option>';
                    }
                ?>
            </select>
            
            <select id="access_filter" name="access" >
                <?php 
                    echo '<option value="all">'.__( "All", 'wplms-braincert').' ('.$access_counts['all'].')</option>';
                    foreach( $this->access_filters as $value => $label ){
                        echo '<option value="'.$value.'" '.selected( $this->whocansee, $value, false ).'>'.$label.' ('.( isset($access_counts[$value] ) ? $access_counts[$value] : "0").')</option>';
                    }
                ?>
            </select>
            
            <input type="hidden" name="orderby" value="<?php echo $this->orderby; ?>" >            
            <input type="hidden" name="order"	value="<?php echo $this->order; ?>" >
            
            <input type="submit" name="" value="<?php _e( "Apply Filter", 'wplms-braincert') ?>" class="button">
            <!-- Now we can render the completed list table -->
        </div>
        
        <?php
	}
	
	public function process_bulk_action() {
		
		$action 		= 'bulk-' . $this->_args['plural'];
        $current_action = $this->current_action();
		
		$this->wbc_api->process_bulk_action( $action, $current_action );

    }
	
	function get_bulk_actions() {
        $actions = array(
            'cancel'	=>	__( "Cancel", 'wplms-braincert' ),
            'delete'    => 	__( 'Delete', 'wplms-braincert' ),
        );
        return $actions;
    }
	
    function get_sortable_columns() {
        $sortable_columns = array(
            'class_title'   => 	array('title',false),  
			'date'     		=> 	array('date',false),
			'start_time'	=>	array('start_ts',true),
			'ispaid'		=>	array('ispaid',false),
			'status'		=>	array('status',false),
           // 'username'     		=> array('login',false),     //true means it's already sorted
        );
        return $sortable_columns;
    }
	
	function column_cb($item) {
        return sprintf(
            '<input type="checkbox" name="class_id[]" value="%s" />', $item->class_id
        );    
    }


    function column_class_id($item){
		return sprintf('%1$s', $item->class_id);
    }

    function column_class_title($item) {

    	$actions = array(
            'edit' 		=> sprintf('<a href="'.BACKEND_URL.'admin.php?page=add_class&class_id=%1$s">'.__( 'Edit', 'wplms-braincert' ).'</a>',$item->class_id),
            'delete' 	=> sprintf('<span class="delete-link" data-id="%1s"><a href="#" class="deleteclass">'.__( 'Delete', 'wplms-braincert' ).'</a></span>',$item->class_id),
        );

        if( $item->start_ts > time() ){
			if( $item->isCancel == 1 || $item->isCancel == 2 ){
				$actions['activateClass'] = sprintf('<span class="cancel-link active-%1$s"  data-value="0" data-id="%1$s" ><a href="'."#".'" class="activate">'.__( 'Activate class', 'wplms-braincert' ).'</a></span>',$item->class_id);
			}
	
			if( $item->is_recurring == 1  &&  $item->isCancel == 0 ){
				$actions['cancelRecurring'] = sprintf('<span class="cancel-link cancle-%1$s" data-value="2" data-recurring="%2$s" data-id="%1s" data-recurring="%2$s"><a href="'."#".'" class="cancelall">'.__( 'Cancel all classes', 'wplms-braincert' ).'</a></span>',$item->class_id, $item->is_recurring );
			}
			
			if( $item->isCancel == 0 ){
				$actions['cancelCurrent'] = sprintf('<span class="cancel-link cancle-%1$s"  data-value="1" data-id="%1$s"><a href="'."#".'" class="cancel">'.__( 'Cancel', 'wplms-braincert' ).'</a></span>',$item->class_id);
			}
		}
		
		if( $this->wbc_api->_can( 'launch', array( 'status' => $item->status , 'course_id' => $item->course_id )) ){
			$actions['learner-preview']  = sprintf('<span class="user_preview" data-id="%1$s" data-instructor="%2$s" data-courseid="%3$s" data-isTeacher="0"><a href="'."#".'" class="learner-preview">'.__( 'Learner preview', 'wplms-braincert' ).'</a></span>', $item->class_id, get_current_user_id(), $item->course_id);
	
			$actions['instructor-preview']  = sprintf('<span class="user_preview"  data-id="%1$s" data-instructor="%2$s" data-courseid="%3$s" data-isTeacher="1"><a href="'."#".'" class="instructor-preview">'.__( 'Instructor preview', 'wplms-braincert' ).'</a></span>', $item->class_id, $item->instructor_id, $item->course_id);
		}
		
		if( $this->wbc_api->_can( 'view_attendance', array( 'status' => $item->status , 'course_id' => $item->course_id )) )
        	$actions['attendance']  = sprintf('<span class="attendance"  data-value="0" data-id="%1$s"><a href="'."#".'" class="class-attendance">'.__( 'Attendance Report', 'wplms-braincert' ).'</a></span>',$item->class_id);   
        
    	return sprintf('%1$s <br />%2$s', $item->title,$this->row_actions($actions));
    }


    function column_course($item){
		//do_action('wplms_course_application_submission_users', $user->user_id, $value->ID); 
		$actions = array(
						'edit-course' => sprintf('<a href="%1$s" target="_blank">'.__( 'Edit', 'wplms-braincert' ).'</a>', get_edit_post_link( $item->course_id ) ),
						'course-attendance'  => sprintf('<a title="'.__('Click here to download attendance report','wplms-braincert').'" href="'."#".'" data-courseid="%1$s" class="course_attendance">'.__( 'Course Attendance Report', 'wplms-braincert' ).'</a>',$item->course_id),  
					);
        return sprintf('<a href="%2$s" target="_blank">%1$s <br />%3$s</a>%4$s', 
						get_the_post_thumbnail( $item->course_id,array( 50, 50) ),
						get_permalink( $item->course_id ),
						get_the_title( $item->course_id ),
						$this->row_actions($actions)
					);
    }
	
    function column_date($item){
		return sprintf('%1$s', $this->wbc_api->date( $item->start_ts, $this->timezone, $this->date_format ) );
    }
	
    function column_start_time($item){
      return sprintf('%1$s', $this->wbc_api->date( $item->start_ts, $this->timezone, $this->time_format ));
    }

    function column_end_time($item){
		return sprintf('%1$s', $this->wbc_api->date( $item->end_ts, $this->timezone, $this->date_format ));
    }

    function column_end_date($item){
    	return sprintf('%1$s', $this->wbc_api->date( $item->end_ts, $this->timezone, $this->time_format ));
    }

    function column_access($item){
    	if($item->whocansee == 0){
    		return sprintf('<span class="dashicons dashicons-unlock public" title="'.__( 'Public', 'wplms-braincert' ).'"></span>');
    	}
    	else{
    		return sprintf('<span class="dashicons dashicons-lock private" title="'.__( 'Private', 'wplms-braincert' ).'"></span>');
    	}
    	
    }

    function column_record($item){
    	$actions = array(
    					'class-recording'  => ($item->record == 1) ? sprintf('<span class="class_recording"  data-value="0" data-id="%1$s"><a href="'."#".'" class="recording">'.__( 'View', 'wplms-braincert' ).'</a></span>',$item->class_id) : '',

    					'view-recording'  => ($item->record == 1) ? sprintf('<span class="view_recording"  data-value="0" data-id="%1$s"><a href="'."#".'" class="recording">'.__( 'Manage', 'wplms-braincert' ).'</a></span>',$item->class_id) : ''
    				);

    	if( $item->record == 1 ){
    		return sprintf( __('Yes %1$s', 'wplms-braincert'), $this->row_actions( $actions ) );
    	}
    	return __('No' , 'wplms-braincert');
    	
    }

    function column_shortcode($item){
    	return sprintf('<a href="#" class="copy-click"  data-tooltip-text="Click to copy" data-tooltip-text-copied="✔ Copied to clipboard">[braincert_class id="%1$s"]</a>', $item->class_id);
    }

    function column_type($item){
    	if( $item->ispaid == 1 ){
    		return __('Paid', 'wplms-braincert');
    	}
    	return __('Free', 'wplms-braincert');
    }

    function column_status($item){
    	return sprintf('%1$s', $this->status_filters[ $item->status ] );
    }

    function column_instructor($item){
    	$user_meta = get_userdata($item->instructor_id);
    	$actions = array(
    					'edit-user'  => sprintf( '<span class="edit_user"><a href="'.get_edit_user_link( $item->instructor_id ).'" target="_blank">'.__( 'Edit', 'wplms-braincert' ).'</a></span>' ),
    					'view-user'  => sprintf( '<span class="view_user"><a href="'.bp_core_get_userlink( $item->instructor_id, false, true ).'" target="_blank">'.__( 'View', 'wplms-braincert' ).'</a></span>' )
    				);
    	return sprintf('%1$s%2$s', $user_meta->display_name, $this->row_actions( $actions ) );
    }

    function column_duration($item){
		return sprintf('%1$s Min.', $item->duration/60);
    }


    function prepare_items( ) {

		global $wpdb, $_wp_column_headers; //This is used only if making any database queries
		$user = get_current_user_id();

        $screen = get_current_screen();
		
		$columns = $this->get_columns();

		$_wp_column_headers[$screen->id] = $columns;	

		$hidden = array();

		$sortable = $this->get_sortable_columns();	
		
		$this->nonce = wp_create_nonce() ;
		
		$this->_column_headers = array( $columns, $hidden, $sortable );	
		
		$this->search = isset( $_REQUEST['s'] ) ? wp_unslash( trim( $_REQUEST['s'] ) ) : '';
		
		$this->orderby = isset( $_REQUEST['orderby'] ) ? $_REQUEST['orderby'] : 'start_ts';
		
		$this->order = isset( $_REQUEST['order'] ) ? $_REQUEST['order'] : 'ASC';
		
		$order = "FIELD(status,'live','upcoming','completed','expired','canceled') ";
		
		switch( $this->orderby ){
				
			case 'status':	
			$order .= "$this->order, start_ts $this->order";
			break;
			
			case 'date':	
			case 'start_ts':	
			default:
			$order .= "ASC, $this->orderby $this->order";
			break;
		}

		$screen_option = $screen->get_option('per_page', 'option');

		$this->per_page = get_user_meta($user, $screen_option, true);

		if ( empty ( $this->per_page ) || $this->per_page  < 1 ) {
			
			$this->per_page = $screen->get_option( 'per_page', 'default' );
		}
		
		$paged = $this->get_pagenum();
		
		$this->process_bulk_action();
		
		$where = $this->wbc_api->_where( array(
											'status'	=> $this->status,
											'course_id'	=> $this->course,
											'instructor'=> $this->instructor,
											'type'		=> $this->type,
											'whocansee'	=> $this->whocansee,
											'search'	=> $this->search,
										));

		$offset = ( $paged-1 ) * $this->per_page;
		
		$query = "SELECT $wpdb->wbc_classes.*, p.post_title as course_title
    		".$this->wbc_api->_status_case()."
		FROM $wpdb->wbc_classes
		INNER JOIN  $wpdb->posts p
				ON $wpdb->wbc_classes.course_id = p.ID
				
		".$where ."
		ORDER BY $order
		LIMIT $this->per_page OFFSET $offset";

        $this->items = $wpdb->get_results($query);

        $total_items = $wpdb->get_var("SELECT count(*) FROM $wpdb->wbc_classes
		INNER JOIN  $wpdb->posts p
				ON $wpdb->wbc_classes.course_id = p.ID
		".$where );

        /**
         * REQUIRED. We also have to register our pagination options & calculations.
         */
        $this->set_pagination_args( array(
            'total_items' => $total_items,                  //WE have to calculate the total number of items
            'per_page'    => $this->per_page,                     //WE have to determine how many items to show on a page
            'total_pages' => ceil( $total_items / $this->per_page )   //WE have to calculate the total number of pages
        ) );
    }

	public function no_items() {

		_e( "Didn't found any class matching your criteria. Please try again.", 'wplms-braincert' );
		
    }
}