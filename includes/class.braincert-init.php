<?php
/**
 *
 * @class       WPLMS_BrainCert_API
 * @author      Team WPGenius (Makarand Mane)
 * @category    Admin
 * @package     WPLMS-BrainCert/includes
 * @version     1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

require( 'class.braincert.php' );

class WPLMS_BrainCert_API extends Braincert{

	private function __construct(){
		
	} // END public function __construct.
	
	private function _post_to_db( $class_id ){
		
		$class_arr 	= json_decode( $this->getclassdata( array( 'class_id' => $class_id ) ) );
		
		if( isset( $class_arr->status ) && $class_arr->status  == 'error' )
			wp_send_json_error( array( 'msg'=>  $class_arr->error ) );	//If there is any error returned by braincert then just stop execution here.
		
		$class 		= $class_arr[0];
		
		$start_ts	= $this->convert_tz( $class->date.' '.$class->start_time, '', $class->timezone_country, 'U' );
		$end_ts		= $start_ts + $class->duration;
		
		return array(
					'class_id' 							=>  $class_id,
					'course_id'  						=> 	sanitize_text_field( $_POST['course'] ),
					'instructor_id' 					=> 	isset( $_POST['instructor_name']  ) ? sanitize_text_field( $_POST['instructor_name'] ) : get_current_user_id(),
					'location_id' 						=> 	sanitize_text_field( $_POST['location_id'] ),
					'title' 							=> 	sanitize_text_field( $_POST['title'] ),
					'description' 						=> 	sanitize_textarea_field( $_POST['description'] ),
					'date' 								=> 	$_POST['date'],
					'start_time' 						=> 	date( "H:i:s", strtotime( $class->start_time )),
					'end_time' 							=> 	date( "H:i:s", strtotime( $class->end_time )),
					'start_ts' 							=> 	$start_ts,
					'end_ts' 							=> 	$end_ts,
					'timezone' 							=> 	sanitize_text_field( $_POST['timezone'] ),
					'timezone_country'					=> 	$class->timezone_country,
					'timezone_label'					=> 	$class->timezone_label,
					'difftime'							=> 	$class->difftime,
					'is_recurring' 						=> 	sanitize_text_field( $_POST['is_recurring'] ),
					'repeat'							=> 	sanitize_text_field( $_POST['repeat'] ),
					'weekdays'							=> 	( isset( $class->weekdays ) && $class->weekdays ) ? $class->weekdays : '',
					'afterclasses'						=> 	sanitize_text_field( $_POST['afterclasses'] ),
					'end_classes_count' 				=> 	sanitize_text_field( $_POST['end_classes_count'] ),
					'end_date'							=> 	sanitize_text_field( $_POST['end_date'] ),
					'allow_change_interface_language' 	=> 	sanitize_text_field( $_POST['allow_change_interface_language'] ),
					'language' 							=> 	sanitize_text_field( $_POST['language'] ),
					'record' 							=> 	sanitize_text_field( $_POST['record'] ),
					'isRecordingLayout' 				=> 	sanitize_text_field( $_POST['isRecordingLayout'] ),
					'start_recording_auto' 				=> 	sanitize_text_field( $_POST['start_recording_auto'] ),
					'isControlRecording' 				=> 	sanitize_text_field( $_POST['isControlRecording'] ),
					'classroom_type' 					=> 	sanitize_text_field( $_POST['classroom_type'] ),
					'isCorporate' 						=> 	sanitize_text_field( $_POST['isCorporate'] ),
					'isPrivateChat' 					=> 	sanitize_text_field( $_POST['isPrivateChat'] ),
					'viewRecording' 					=> 	sanitize_text_field( $_POST['viewRecording'] ),
					'isScreenshare' 					=> 	sanitize_text_field( $_POST['isScreenshare'] ),
					'whocansee' 						=> 	sanitize_text_field( $_POST['whocansee'] ),
					'ispaid' 							=> 	sanitize_text_field( $_POST['ispaid'] ),
					'currency' 							=> 	sanitize_text_field( $_POST['currency'] ),
					'seat_attendees' 					=> 	sanitize_text_field( $_POST['seat_attendees'] ),
					'duration'							=>  $class->duration,
				);		
	}
	
	protected function _form_data( $class_id = '' ){
		if( $class_id ){
        	return $this->get_class( $class_id );
		} else
			return	array(
								
								'course_id'	=> 	isset( $_GET['course_id'] ) && !empty( $_GET['course_id'] ) ? $_GET['course_id'] : '',
								'instructor_id'=> get_current_user_id(),
								'location_id'=>  get_option( 'braincert_default_location',  15 ),
								'title'=> '',
								'description'=> '',
								'date'=> '',
								'start_time'=> '',
								'end_time'=> '',
								'timezone'=> get_option( 'braincert_default_timezone', 72 ),
								'is_recurring'=> 0,
								'repeat'=> '',
								'weekdays'=> '',
								'afterclasses'=> '',
								'end_classes_count'=> '',
								'end_date'=> '',
								'allow_change_interface_language'=> 1,
								'language'=> '',
								'record'=> 0,
								'isRecordingLayout'=> 0,
								'start_recording_auto'=> 1,
								'isControlRecording'=> 1,
								'isVideo'=> 1,
								'classroom_type'=> 0,
								'isCorporate'=> 0,
								'isPrivateChat'=> 0,
								'viewRecording'=> 1,
								'isScreenshare'=> 0,
								'whocansee'=> 1,
								'ispaid'=> 0,
								'currency'=> 'usd',
								'seat_attendees'=> '',
				);
	}

	protected function get_class( $class_id, $status = '' ){
		global $wpdb;
		if( $status )
			$status = $this->_status_case();
			
		$class_query = "SELECT * $status FROM $wpdb->wbc_classes WHERE class_id='".$class_id."'";
        return $wpdb->get_row( $class_query, ARRAY_A );
	}
	
	public function _status_case(){
		global $wpdb;
		return ", (CASE 1 WHEN $wpdb->wbc_classes.isComplete = 1 THEN 'completed' WHEN $wpdb->wbc_classes.isCancel != 0 THEN 'canceled' WHEN  end_ts > UNIX_TIMESTAMP( now() ) AND UNIX_TIMESTAMP( now() ) > start_ts THEN 'live'  WHEN start_ts > UNIX_TIMESTAMP( now() ) THEN 'upcoming'  ELSE 'expired' END) as status";
	}

	public function get_classes( $args = null){
		global $wpdb;
		
		$defaults = array(
			'page'		=> 1,
			'per_page'	=> get_option('braincert_class_per_page', 5),
			'status'	=> 'all',	
			'course_id'	=> 'all',		
			'instructor'=> 'all',
			'type'		=> 'all',
			'whocansee'	=> 'all',
			'search'	=> '',
			'student_id'=> '',			
		);
		$parsed_args = wp_parse_args( $args, $defaults );
		extract( $parsed_args );
		
		$where = $this->_where( $parsed_args );
		
		switch( $status ){
				
			case 'all':	
			$order = "FIELD(status,'live','upcoming','completed','expired','canceled'),  start_ts ASC";
			break;
			
			case 'live':			
			case 'upcoming':
			$order = "start_ts ASC";
			break;
			
			case 'live_upcoming':
			case 'completed':
			case 'expired':
			case 'canceled':
			default:
			$order = "start_ts DESC";
			break;
		}

		$offset = ( $page-1 ) * $per_page;
		
		$query = "SELECT $wpdb->wbc_classes.*, p.post_title as course_title
    		".$this->_status_case()."
		FROM $wpdb->wbc_classes
		INNER JOIN  $wpdb->posts p
				ON $wpdb->wbc_classes.course_id = p.ID
				
		".$where ."
		ORDER BY $order
		LIMIT $per_page OFFSET $offset";

        return $wpdb->get_results( $query, ARRAY_A );
	}
	
	public function _where( $args ){
			
		global $wpdb;
		extract( $args );
		$where = "WHERE $wpdb->wbc_classes.id = $wpdb->wbc_classes.id ";

		if ( $status != 'all'  ) {
			switch( $status ){
				
				case 'live':
				$where .= " AND ( end_ts > UNIX_TIMESTAMP( now() ) and UNIX_TIMESTAMP( now() ) > start_ts )";
				break;
				
				case 'live_upcoming':
				$where .= " AND  ( ( end_ts > UNIX_TIMESTAMP( now() ) and UNIX_TIMESTAMP( now() ) > start_ts ) OR start_ts > UNIX_TIMESTAMP( now() ) ) AND $wpdb->wbc_classes.isCancel = 0";
				break;				
				
				case 'upcoming':
				$where .= " AND  start_ts > UNIX_TIMESTAMP( now() ) AND $wpdb->wbc_classes.isCancel = 0";
				break;
				
				case 'completed':
				$where .= " AND $wpdb->wbc_classes.isComplete = 1";
				break;
				
				case 'expired':
				$where .= " AND end_ts < UNIX_TIMESTAMP( now() ) AND $wpdb->wbc_classes.isCancel = 0 AND $wpdb->wbc_classes.isComplete = 0";
				break;
				
				case 'canceled':
				$where .= " AND $wpdb->wbc_classes.isCancel != 0";
				break;
				
				default:
				break;
			}
		}

		if ( $course_id && $course_id != 'all'  ) 
			$where .= " AND $wpdb->wbc_classes.course_id = '".$course_id."'";

		if ( isset( $instructor ) && $instructor && $instructor != 'all' )
			$where .= " AND $wpdb->wbc_classes.instructor_id = '".$instructor."'";
					
		if ( $type != 'all' && is_numeric(  $type )  )			
			$where .= " AND $wpdb->wbc_classes.ispaid = '".$type."'";
			
		if ( $whocansee != 'all' && is_numeric(  $whocansee ) )
			$where .= " AND $wpdb->wbc_classes.whocansee = '".$whocansee."'";
			
		if ( $search != "") {

			if( filter_var($search, FILTER_VALIDATE_INT)){
				$where .= " AND ( $wpdb->wbc_classes.class_id LIKE '%".$search."%')";
			}else{
				$where .= " AND ( $wpdb->wbc_classes.title LIKE '%".$search."%')";
			}
		}
		
		if( isset( $student_id ) && $student_id && is_numeric(  $student_id ) ) {
			$course_ids = $this->get_student_courses(  $student_id );
			$where .= " AND $wpdb->wbc_classes.course_id IN ( ".( is_array( $course_ids ) ? implode( ', ', $course_ids ) : $course_ids )." ) ";
		}
		
		return $where;
	}

	protected function complete_class( $class_id ){
		if( $class_id ){
			global $wpdb;	
			$result = $wpdb->update( $wpdb->wbc_classes, array( 'isComplete' => 1 ), array( 'class_id' => $class_id ));	
			if( $result ){
				do_action( 'class_completed', $class_id );
				return true;
			}
		}
	}
	
	/**
	*	Insert new class to db. Class id is required
	*/
	protected function add_class( $class_id ){
		
		if( $class_id ){
			global $wpdb;		
			$data = $this->_post_to_db( $class_id );
						
			$result = $wpdb->insert( $wpdb->wbc_classes, $data);	
			if( $result ) {
				$this->schedule_cron( $data );
				do_action( 'class_scheduled', $class_id, $data );
				return "Class '".$data['title']. "' successfully added, row ID is ".$wpdb->insert_id;
			}
			else {
				return $wpdb->last_error;
			}
		}
	}

	/**
	*	Update new class data to db. Class id is required
	*/
	protected function update_class( $class_id ){
		if( $class_id ){
			global $wpdb;		
			$data = $this->_post_to_db( $class_id );
						
			$result = $wpdb->update( $wpdb->wbc_classes, $data, array( 'class_id' => $class_id ));	
			if( $result ) {
				$this->unschedule_cron( $class_id );
				$this->schedule_cron( $data );
				do_action( 'class_updated', $class_id, $data );
				return "Class '".$data['title']. "' successfully updated, row ID is ".$wpdb->insert_id;
			}
			else {
				return $wpdb->last_error;
			}
		}
	}

	/**
	*	Delete class. Class id is required
	*/
	protected function delete_class( $class_id ){
		if( $class_id ){
			global $wpdb;		
			$data = $this->get_class( $class_id );
			$result = $wpdb->delete( $wpdb->wbc_classes, array( 'class_id' => $class_id ));	
			if( $result ) {
				$this->unschedule_cron( $class_id );
				do_action( 'class_deleted', $class_id, $data );
				return "Class '".$default['title']. "' successfully deleted, row ID is ".$wpdb->insert_id;
			}
			else {
				return $wpdb->last_error;
			}
		}
	}

	/**
	*	Set class class status to inactive. Class id is required
	*/
	protected function cancel_class( $class_id ){

		if( $class_id ){
			global $wpdb;	
			$data = $this->get_class( $class_id );
			$state = isset( $_POST['isCancel'] ) ? $_POST['isCancel'] : 1;
			$result = $wpdb->update( $wpdb->wbc_classes, array( 'isCancel' => $state ), array( 'class_id' => $class_id ));	

			if( $result ){
				if( $state )
					$this->unschedule_cron( $class_id );
				else
					$this->schedule_cron( $data );
				do_action( 'class_canceled', $class_id, $data );
				return "Class '".$default['title']. "' successfully updated, row ID is ".$wpdb->insert_id;
			}
			else {
				return $wpdb->last_error;
			}
		}
	}
	
	/**
	*	Prepare and return URL to Launch or join class
	*/	
	protected function get_launchurl( $data, $user_id, $isTeacher = 0, $type = 'url' ){
		extract( $data );
		
		$args = array(
			'cid'		=>	$class_id,
			'userId'	=>	$user_id,
			'userName'	=>	bp_core_get_user_displayname($user_id),
			'isTeacher'	=>	$isTeacher,
			'lessonName'=>	$title,
			'courseName'=>	get_the_title( $course_id ),
		);
		$status_obj = json_decode( $this->getlaunchurl( $args ) ) ;
		
		if( isset( $status_obj->status ) && $status_obj->status == 'error' )
			return $status_obj->error;
		
		if( $type == 'url' )
			return $status_obj->launchurl ;
			
		return $status_obj;
	}	
	
	/**
	*	Prepare and return URL where joining class will be processed
	*/	
	protected function get_join_link( $class_id ){
		return esc_url( site_url().'/joinClass/'. $class_id );
	}
	
	/**
	*	Prepare and return URL where joining class will be processed
	*/	
	protected function get_launch_link( $class_id ){
		return esc_url( site_url().'/launchClass/'. $class_id );
	}
	
	/**
	*	Checks is current user is administrator
	*/
	public function _is_admin(){
		if(!is_user_logged_in())
			return false;
		return current_user_can( 'manage_options' ) ;
	}
	
	/***
	*	require course_id
	*/
	public function _is_instructor( $course_id = NULL, $user_id = NULL ){
		
		if(!is_user_logged_in())
			return false;

		if(empty($course_id)){
			global $post;
			$course_id = $post->ID;
		}
		if(empty($user_id))
			$user_id 		= bp_loggedin_user_id();		
		
		if(!empty($user_id) && !empty($course_id)){

			$instructor_ids = apply_filters('wplms_course_instructors',get_post_field('post_author', $course_id),$course_id);
		
			if(!is_array($instructor_ids)){			
				$instructor_ids = array($instructor_ids);			
			}
			
			if(empty($instructor_ids)){			
				$instructor_ids = array();			
			}
			
			if(in_array($user_id, $instructor_ids)){			
				return true;			
			}
					
		}		
		return false;
	}
	
	/**
	*	Checks is current user is administrator
	*/
	public function _is_student( $course_id = null, $user_id = null){
		return wplms_user_course_active_check( $user_id, $course_id );
	}
	
	/**
	*	Checks is current user is visitor or guest for course.
	*/
	public function _is_guest( $course_id = NULL ){
		if( !is_user_logged_in() || ( is_user_logged_in() && !$this->_is_student( $course_id ) && !$this->_is_instructor( $course_id ) && !$this->_is_admin( ) ) )
			return true;
		return false;
	}
	
	
	/**
	*	Checks is current user is visitor or guest for course.
	*/
	public function _can( $case, $class ){
		if( is_user_logged_in()  ){
			switch( $case ){
				
				case 'view_record':
					if( 
						in_array( $class['status'], array( 'expired', 'completed' ) ) && (
						( $class['record']== 1 && ($this->_is_instructor( $class['course_id'] ) || $this->_is_admin()  ) ) || 
						( $class['record']== 1 && $class['viewRecording']== 1 && $this->_is_student( $class['course_id'] ) ) 
						)
					)
						return true;
				break;
				
				case 'view_attendance':
					if( in_array( $class['status'], array( 'completed') ) && ( $this->_is_admin() || $this->_is_instructor( $class['course_id'] ) ) )
						return true;				 
				break;
				
				case 'join':
					if( !in_array( $class['status'], array( 'expired', 'completed', 'canceled' ) ) && ( $this->_is_student( $class['course_id'] ) || $class['whocansee'] == 0) )
						return true;				 
				break;
				
				case 'launch':
				case 'edit':					
					if( !in_array( $class['status'], array( 'expired', 'completed', 'canceled' ) ) && ( $this->_is_admin() || $this->_is_instructor( $class['course_id'] ) ) )
					return true;
				break;

				case 'manage_record':
					if( 
						( $this->_is_admin() ) 
					)
						return true;
				break;
				
				default :
				break;
			}			

		}
		return false;
	}
	
	
	/**
	*	Convert time to another timezone
	*	Require- Source time
	*	Require- Destination timezone
	*	Require- Source timezone Defaults to UTC0
	*	Require- format Defaults to Y-m-d H:i:s
	*	Returns- Converted time
	*/
	public function convert_tz( $time, $to_tz = 'UTC', $from_tz = 'UTC', $format = 'Y-m-d H:i:s' ){	
		$date = new DateTime( $time, new DateTimeZone( $from_tz ) );
		if( $format != "U") $date->setTimezone( new DateTimeZone( $to_tz ) );
		return $date->format( $format );		
	}

	protected function get_timezone_to_timestamp( $timezone, $datetime ){
		global $wpdb;
		$timezone = $wpdb->get_col( $wpdb->prepare( "SELECT timezone_country FROM $wpdb->wbc_timezones WHERE timezone_id=%d ", $timezone ) );
		return $this->convert_tz( $datetime, '', $timezone[0], 'U' );
	}

	protected function validate_schedule( $post ){
		$instructor_id = isset( $post['instructor_name']  ) ? sanitize_text_field( $post['instructor_name'] ) : get_current_user_id();
		$start_ts	= ( $this->get_timezone_to_timestamp( $post['timezone'], $post['date'].' '.$post['start_time'] ) ) + 1;
		$end_ts		= ( $this->get_timezone_to_timestamp( $post['timezone'], $post['date'].' '.$post['end_time'] ) ) - 1;
		if( $start_ts > $end_ts	){
			$end_date	= date('Y-m-d', strtotime( $post['date'] . ' +1 day'));
			$end_ts		= ( $this->get_timezone_to_timestamp( $post['timezone'], $end_date.' '.$post['end_time'] )) - 1;
		}
		
		if( isset( $post['cid'] ) && $post['cid'] )
			$where = 'AND class_id != '.$post['cid'];
		else
			$where ='';
		
		global $wpdb;
		$count = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) FROM $wpdb->wbc_classes WHERE instructor_id=%d AND ( %d BETWEEN start_ts AND end_ts OR 	
%d BETWEEN start_ts AND end_ts ) $where", $instructor_id , $start_ts, $end_ts ) );

		if( $count ){
			wp_send_json_error( array( 'msg' => __('You can not create another class on same time, when you have already another class scheduled. Keep 5 minutes gap between 2 classs.', 'wplms-braincert' ) ) );	
		}
	}
	
	/**
	*	Convert time to another timezone
	*	Require- Source time
	*	Require- Destination timezone
	*	Require- format Defaults to Y-m-d H:i:s
	*	Returns- Converted time
	*/
	public function date( $timestamp, $to_tz, $format = 'Y-m-d H:i:s' ){	
		$date = new DateTime( '@'.$timestamp );
		$date->setTimezone( new DateTimeZone( $to_tz ) );
		return $date->format( $format );		
	}

	public function get_statuses(){
		$statuses =	array(
							"all"		=>	__('All', 'wplms-braincert'),
							"live"		=>	__('Live', 'wplms-braincert'),
							"upcoming"	=>	__('Upcoming','wplms-braincert'),
							"completed"	=>	__('Completed', 'wplms-braincert'),
							"expired"	=>	__('Expired', 'wplms-braincert'),
							"canceled"	=>	__('Canceled', 'wplms-braincert'),
						);
		return apply_filters( 'braincert_status', $statuses );
	}	
	
	public function get_statuses_counts( $args = array() ){
		return $this->get_counts( 'status', $args );
	}

	public function get_whocansee(){
		$whocansee 	=	array(						
							1		=>	__('Private','wplms-braincert'),
							0		=>	__('Public','wplms-braincert'),
						);
		return apply_filters( 'braincert_whocansee', $whocansee );
	}	
	
	public function get_whocansee_counts( $args = array() ){
		return $this->get_counts( 'whocansee', $args );
	}

	public function get_class_types(){
		$class_types 	=	array(								
								0		=>	__('Free','wplms-braincert'),
								1		=>	__('Paid','wplms-braincert'),
							);
		return apply_filters( 'braincert_class_types', $class_types );
	}
	
	public function get_class_type_counts( $args = array() ){
		return $this->get_counts( 'ispaid', $args );
	}

	public function get_timezones(){
		$timezones 	=	array(
							28	=>	__('(GMT) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London','wplms-braincert'),
							30	=>	__('(GMT) Monrovia, Reykjavik','wplms-braincert'),
							72	=>	__('(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna','wplms-braincert'),
							13	=>	__('(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague','wplms-braincert'),
							53	=>	__('(GMT+01:00) Brussels, Copenhagen, Madrid, Paris','wplms-braincert'),
							14	=>	__('(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb','wplms-braincert'),
							71	=>	__('(GMT+01:00) West Central Africa','wplms-braincert'),
							83	=>	__('(GMT+02:00) Amman','wplms-braincert'),
							21	=>	__('(GMT+02:00) Athens','wplms-braincert'),
							84	=>	__('(GMT+02:00) Beirut','wplms-braincert'),
							24	=>	__('(GMT+02:00) Cairo','wplms-braincert'),
							61	=>	__('(GMT+02:00) Harare, Pretoria','wplms-braincert'),
							27	=>	__('(GMT+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius','wplms-braincert'),
							35	=>	__('(GMT+02:00) Jerusalem','wplms-braincert'),
							86	=>	__('(GMT+02:00) Windhoek','wplms-braincert'),
							2	=>	__('(GMT+03:00) Baghdad','wplms-braincert'),
							31	=>	__('(GMT+03:00) Istanbul, Minsk','wplms-braincert'),
							49	=>	__('(GMT+03:00) Kuwait, Riyadh','wplms-braincert'),
							54	=>	__('(GMT+03:00) Moscow, St. Petersburg, Volgograd','wplms-braincert'),
							19	=>	__('(GMT+03:00) Nairobi','wplms-braincert'),
							87	=>	__('(GMT+03:00) Tbilisi','wplms-braincert'),
							34	=>	__('(GMT+03:30) Tehran','wplms-braincert'),
							1	=>	__('(GMT+04:00) Abu Dhabi, Muscat','wplms-braincert'),
							88	=>	__('(GMT+04:00) Baku','wplms-braincert'),
							9	=>	__('(GMT+04:00) Baku, Tbilisi, Yerevan','wplms-braincert'),
							89	=>	__('(GMT+04:00) Port Louis','wplms-braincert'),
							47	=>	__('(GMT+04:30) Kabul','wplms-braincert'),
							25	=>	__('(GMT+05:00) Ekaterinburg','wplms-braincert'),
							90	=>	__('(GMT+05:00) Islamabad, Karachi','wplms-braincert'),
							73	=>	__('(GMT+05:00) Islamabad, Karachi, Tashkent','wplms-braincert'),
							33	=>	__('(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi','wplms-braincert'),
							62	=>	__('(GMT+05:30) Sri Jayawardenepura','wplms-braincert'),
							91	=>	__('(GMT+05:45) Kathmandu','wplms-braincert'),
							42	=>	__('(GMT+06:00) Almaty, Novosibirsk','wplms-braincert'),
							12	=>	__('(GMT+06:00) Astana, Dhaka','wplms-braincert'),
							41	=>	__('(GMT+06:30) Rangoon','wplms-braincert'),
							59	=>	__('(GMT+07:00) Bangkok, Hanoi, Jakarta','wplms-braincert'),
							50	=>	__('(GMT+07:00) Krasnoyarsk','wplms-braincert'),
							17	=>	__('(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi','wplms-braincert'),
							46	=>	__('(GMT+08:00) Irkutsk, Ulaan Bataar','wplms-braincert'),
							60	=>	__('(GMT+08:00) Kuala Lumpur, Singapore','wplms-braincert'),
							70	=>	__('(GMT+08:00) Perth','wplms-braincert'),
							63	=>	__('(GMT+08:00) Taipei','wplms-braincert'),
							65	=>	__('(GMT+09:00) Osaka, Sapporo, Tokyo','wplms-braincert'),
							77	=>	__('(GMT+09:00) Seoul','wplms-braincert'),
							75	=>	__('(GMT+09:00) Yakutsk','wplms-braincert'),
							10	=>	__('(GMT+09:30) Adelaide','wplms-braincert'),
							4	=>	__('(GMT+09:30) Darwin','wplms-braincert'),
							20	=>	__('(GMT+10:00) Brisbane','wplms-braincert'),
							5	=>	__('(GMT+10:00) Canberra, Melbourne, Sydney','wplms-braincert'),
							74	=>	__('(GMT+10:00) Guam, Port Moresby','wplms-braincert'),
							64	=>	__('(GMT+10:00) Hobart','wplms-braincert'),
							69	=>	__('(GMT+10:00) Vladivostok','wplms-braincert'),
							15	=>	__('(GMT+11:00) Magadan, Solomon Is., New Caledonia','wplms-braincert'),
							44	=>	__('(GMT+12:00) Auckland, Wellington','wplms-braincert'),
							26	=>	__('(GMT+12:00) Fiji, Kamchatka, Marshall Is.','wplms-braincert'),
							6	=>	__('(GMT-01:00) Azores','wplms-braincert'),
							8	=>	__('(GMT-01:00) Cape Verde Is.','wplms-braincert'),
							39	=>	__('(GMT-02:00) Mid-Atlantic','wplms-braincert'),
							22	=>	__('(GMT-03:00) Brasilia','wplms-braincert'),
							94	=>	__('(GMT-03:00) Buenos Aires','wplms-braincert'),
							55	=>	__('(GMT-03:00) Buenos Aires, Georgetown','wplms-braincert'),
							29	=>	__('(GMT-03:00) Greenland','wplms-braincert'),
							95	=>	__('(GMT-03:00) Montevideo','wplms-braincert'),
							45	=>	__('(GMT-03:30) Newfoundland','wplms-braincert'),
							3	=>	__('(GMT-04:00) Atlantic Time (Canada)','wplms-braincert'),
							105	=>	__('(GMT-04:00) Eastern Daylight  Time (US & Canada)','wplms-braincert'),
							57	=>	__('(GMT-04:00) Georgetown, La Paz, San Juan','wplms-braincert'),
							96	=>	__('(GMT-04:00) Manaus','wplms-braincert'),
							51	=>	__('(GMT-04:00) Santiago','wplms-braincert'),
							76	=>	__('(GMT-04:30) Caracas','wplms-braincert'),
							56	=>	__('(GMT-05:00) Bogota, Lima, Quito','wplms-braincert'),
							23	=>	__('(GMT-05:00) Eastern Time (US & Canada)','wplms-braincert'),
							67	=>	__('(GMT-05:00) Indiana (East)','wplms-braincert'),
							11	=>	__('(GMT-06:00) Central America','wplms-braincert'),
							16	=>	__('(GMT-06:00) Central Time (US & Canada)','wplms-braincert'),
							37	=>	__('(GMT-06:00) Guadalajara, Mexico City, Monterrey','wplms-braincert'),
							7	=>	__('(GMT-06:00) Saskatchewan','wplms-braincert'),
							68	=>	__('(GMT-07:00) Arizona','wplms-braincert'),
							38	=>	__('(GMT-07:00) Chihuahua, La Paz, Mazatlan','wplms-braincert'),
							40	=>	__('(GMT-07:00) Mountain Time (US & Canada)','wplms-braincert'),
							52	=>	__('(GMT-08:00) Pacific Time (US & Canada)','wplms-braincert'),
							104	=>	__('(GMT-08:00) Tijuana, Baja California','wplms-braincert'),
							48	=>	__('(GMT-09:00) Alaska','wplms-braincert'),
							32	=>	__('(GMT-10:00) Hawaii','wplms-braincert'),
							58	=>	__('(GMT-11:00) Midway Island, Samoa','wplms-braincert'),
							18	=>	__('(GMT-12:00) International Date Line West','wplms-braincert'),

						);
		return apply_filters( 'braincert_timezones', $timezones );
	} 

	public function get_locations(){
		$locations 	= 	array(
							1		=>	__('US East (Dallas, TX)','wplms-braincert'),
							3		=>	__('US East (New York)','wplms-braincert'),
							8		=>	__('US East (Miami, FL)','wplms-braincert'),
							2		=>	__('US West (Los Angeles, CA)','wplms-braincert'),
							15		=>	__('Europe (Amsterdam, Netherlands)','wplms-braincert'),
							4		=>	__('Europe (Frankfurt, Germany)','wplms-braincert'),
							5		=>	__('Europe (London)','wplms-braincert'),
							9		=>	__('Europe (Milan, Italy)','wplms-braincert'),
							13		=>	__('Europe (Paris, France)','wplms-braincert'),
							6		=>	__('Asia Pacific (Bangalore, India)','wplms-braincert'),
							7		=>	__('Asia Pacific (Singapore)','wplms-braincert'),
							10		=>	__('Asia Pacific (Tokyo, Japan)','wplms-braincert'),
							14		=>	__('Asia Pacific (Hong Kong, China)','wplms-braincert'),
							11		=>	__('Middle East (Dubai, UAE)','wplms-braincert'),
							12		=>	__('Australia (Sydney)','wplms-braincert'),
						);
		return apply_filters( 'braincert_locations', $locations );
	}

	public function get_languages(){
		$languages 	=	array(
							1	=> __('arabic','wplms-braincert'),
							2	=> __('bosnian','wplms-braincert'),
							3	=> __('bulgarian','wplms-braincert'),
							4	=> __('catalan','wplms-braincert'),
							5	=> __('chinese-simplified','wplms-braincert'),
							6	=> __('chinese-traditional','wplms-braincert'),
							7	=> __('croatian','wplms-braincert'),
							8	=> __('czech','wplms-braincert'),
							9	=> __('danish','wplms-braincert'),
							0	=> __('dutch','wplms-braincert'),
							11	=> __('english','wplms-braincert'),
							12	=> __('estonian','wplms-braincert'),
							13	=> __('finnish','wplms-braincert'),
							14	=> __('french','wplms-braincert'),
							15	=> __('german','wplms-braincert'),
							16	=> __('greek','wplms-braincert'),
							17	=> __('haitian-creole','wplms-braincert'),
							18	=> __('hebrew','wplms-braincert'),
							19	=> __('hindi','wplms-braincert'),
							20	=> __('hmong-daw','wplms-braincert'),
							21	=> __('hungarian','wplms-braincert'),
							22	=> __('indonesian','wplms-braincert'),
							23	=> __('italian','wplms-braincert'),
							24	=> __('japanese','wplms-braincert'),
							25	=> __('kiswahili','wplms-braincert'),
							26	=> __('klingon','wplms-braincert'),
							27	=> __('korean','wplms-braincert'),
							28	=> __('lithuanian','wplms-braincert'),
							29	=> __('malayalam','wplms-braincert'),
							30	=> __('malay','wplms-braincert'),
							31	=> __('maltese','wplms-braincert'),
							32	=> __('norwegian-bokma','wplms-braincert'),
							33	=> __('persian','wplms-braincert'),
							34	=> __('polish','wplms-braincert'),
							35	=> __('portuguese','wplms-braincert'),
							36	=> __('romanian','wplms-braincert'),
							37	=> __('russian','wplms-braincert'),
							38	=> __('serbian','wplms-braincert'),
							39	=> __('slovak','wplms-braincert'),
							40	=> __('slovenian','wplms-braincert'),
							41	=> __('spanish','wplms-braincert'),
							42	=> __('swedish','wplms-braincert'),
							43	=> __('tamil','wplms-braincert'),
							44	=> __('telugu','wplms-braincert'),
							45	=> __('thai','wplms-braincert'),
							46	=> __('turkish','wplms-braincert'),
							47	=> __('ukrainian','wplms-braincert'),
							48	=> __('urdu','wplms-braincert'),
							49	=> __('vietnamese','wplms-braincert'),
							50	=> __('welsh','wplms-braincert'),
						);
		return apply_filters( 'braincert_interface_languages', $languages );
	}

	public function get_currency(){
		$currency 	=	array(
							"aud"	=>	__('AUD','wplms-braincert'),
							"cad"	=>	__('CAD','wplms-braincert'),
							"eur"	=>	__('EUR','wplms-braincert'),
							"gbp"	=>	__('GBP','wplms-braincert'),
							"nzd"	=>	__('NZD','wplms-braincert'),
							"usd"	=>	__('USD','wplms-braincert'),
						);
		return apply_filters( 'braincert_currency', $currency );
	}

	public function get_class_repeat_types(){
		$class_repeat_types	= array(
								1	=>	__('Daily (all 7 days)'),
								2	=>	__('6 Days(Mon-Sat)'),
								3	=>	__('5 Days(Mon-Fri)'),
								4	=>	__('Weekly'),
								5	=>	__('Once every month'),
								6	=>	__('On selected days'),
							);
		return apply_filters( 'braincert_class_repeat_types', $class_repeat_types );
	}

	public function get_distinct_courses(){
		global $wpdb;
		return $wpdb->get_col( "SELECT DISTINCT course_id FROM {$wpdb->wbc_classes}" );
	}

	public function get_courses( $args = array( 'all' => false )  ){
		extract( $args );
		
		if( isset($student_id ) && $student_id && is_numeric(  $student_id ) ) 
			$course_ids = $this->get_student_courses(  $student_id );
		else if( isset($instructor ) && $instructor && is_numeric(  $instructor ) ) 
			$course_ids = $this->get_instructor_courses( $instructor );
		else if( !$all )
			$course_ids = $this->get_distinct_courses();
		else
			$course_ids = array();

		$courses = new WP_Query( 
					   array(	
								'post_type' 		=> 'course',
								'posts_per_page' 	=> -1,
								'order'				=> 'DESC',
								'post_status' 		=> 'publish',
								'post__in'			=>	$course_ids,
							)
					);
        return $courses;
	}
	
	public function get_courses_counts( $args = array() ){
		return $this->get_counts( 'course', $args );
	}

	public function get_instructors( $course_id = '' ){

		if( $course_id ){
			$wplms_api = bp_course_notifications::$instance;
            return $wplms_api->get_instructors( $course_id );
		}
		return get_users( array( 'role__in' => array( 'administrator', 'instructor' ) ) );
	}
	
	public function get_instructor_counts( $args = array() ){
		return $this->get_counts( 'instructor', $args );
	}
	
	public function get_instructor_courses( $instructor_id ){
		global $wpdb;
		return $wpdb->get_col( $wpdb->prepare( "SELECT DISTINCT course_id FROM {$wpdb->wbc_classes}  WHERE $wpdb->wbc_classes.instructor_id = %d ", $instructor_id ) );
	}
	
	public function get_student_courses( $student_id ){
		global $wpdb;
		return $wpdb->get_col( $wpdb->prepare( "SELECT meta_key FROM {$wpdb->usermeta} WHERE user_id = %d AND meta_key REGEXP '^[0-9]+$' AND meta_value > UNIX_TIMESTAMP( NOW() )", $student_id ) );
	}
	
	protected function get_course_active_students( $course_id ){
		global $wpdb;
		$query = $wpdb->prepare( "SELECT user_id FROM {$wpdb->usermeta} WHERE meta_key = %d AND meta_value > UNIX_TIMESTAMP( NOW() )", $course_id );
		return $wpdb->get_col( $query );
	}
	
	protected function get_course_classes_count( $course_id ){
		global $wpdb;
		return $wpdb->get_var(  $wpdb->prepare(  "SELECT COUNT(*)  FROM {$wpdb->wbc_classes}  WHERE $wpdb->wbc_classes.course_id = %d ", $course_id ) );
	}
	
	private function get_counts( $type, $args = array() ){
		global $wpdb;
		
		$defaults = array(
			'where'		=> '',
			'status'	=> 'all',	
			'course_id'	=> 'all',		
			'instructor'=> 'all',
			'whocansee'	=> 'all',
			'search'	=> '',
			'student_id'=> '',			
		);
		$parsed_args = wp_parse_args( $args, $defaults );
		extract( $parsed_args );
		
		if( !$where )
			$where = $this->_where( $parsed_args );

		$result = array();		
		$total = 0 ;
		
		switch( $type ){
		
			case 'status':
				$results = $wpdb->get_results(  $wpdb->prepare(  "SELECT ( CASE 1 WHEN isComplete = 1 THEN 'completed' WHEN isCancel != 0 THEN 'canceled' WHEN end_ts > UNIX_TIMESTAMP(NOW()) AND UNIX_TIMESTAMP(NOW()) > start_ts THEN 'live' WHEN start_ts > UNIX_TIMESTAMP(NOW()) THEN 'upcoming' ELSE 'expired'
						END) AS name,
						COUNT(*) as total
					FROM {$wpdb->wbc_classes}  $where
					GROUP BY name ", '' ), ARRAY_A );
			break;
			
			case 'ispaid':
				$results = $wpdb->get_results(  $wpdb->prepare(  "SELECT ispaid as name, COUNT(*) AS total
						FROM {$wpdb->wbc_classes}  $where
						GROUP BY name ", '' ), ARRAY_A );
			break;			
			
			case 'whocansee':
				$results = $wpdb->get_results(  $wpdb->prepare(  "SELECT whocansee as name, COUNT(*) AS total
						FROM {$wpdb->wbc_classes}  $where
						GROUP BY name ", '' ), ARRAY_A );
			break;
			
			case 'course':
			case 'courses':
				$results = $wpdb->get_results(  $wpdb->prepare(  "SELECT course_id as name, COUNT(*) AS total 
						FROM {$wpdb->wbc_classes}  $where
						GROUP BY name  ", '' ), ARRAY_A );
			break;
			
			case 'instructor':
			case 'instructors':
				$results = $wpdb->get_results(  $wpdb->prepare(  "SELECT instructor_id as name, COUNT(*) AS total 
						FROM {$wpdb->wbc_classes}  $where
						GROUP BY name  ", '' ), ARRAY_A );
			break;
			 
		}
		
		foreach( $results as $row ){
			$result[ $row['name'] ] = $row['total'] ;
			$total = $total + $row['total'];
		}	
		$result['all'] = $total ;		
		
		return $result;
		
	}

	public function get_localise( $action ){
		switch( $action ){
			
			case 'class' :
				$args = array(
							'ajax_url'			=>	admin_url('admin-ajax.php').'?action=insert_class',
							'nonce'				=>	wp_create_nonce( 'class_security_nonce' ),
							'location_url'		=>	admin_url('admin.php').'?page=add_class&class_id=',
							'modal_url'			=>  admin_url('admin-ajax.php').'?action=create_front_class',
							'alert'				=>	__( 'Alert!', 'wplms-braincert' ),
							'edit_class'		=>	__( 'Edit class', 'wplms-braincert' ),
							'json_error'		=>	__( 'Please contact administrator!', 'wplms-braincert' ),
							'saving_msg'		=>	__( 'Saving Class...', 'wplms-braincert' ),
							'save_msg'			=>	__( 'Save', 'wplms-braincert' ),
						);
			break;
			
			case 'manage' :			
				$args = array(
							'ajax_url'			=>	admin_url('admin-ajax.php'),
							'nonce'				=>	wp_create_nonce( 'manage_security_nonce' ),
							'cancel_class'		=>	__( 'Class cancelled successfully.', 'wplms-braincert' ),
							'delete_class'		=>	__( 'Class deleted successfully.', 'wplms-braincert' ),
							'attendance_error'	=>	__( 'No records found.', 'wplms-braincert' ),
							'active_class'		=>	__( 'Class activated successfully.', 'wplms-braincert' ),
							'attendance_title'	=>	__( 'Attedance Report', 'wplms-braincert' ),
							'recording_title'	=>	__( 'View Recodings', 'wplms-braincert' ),
							'delete_title'		=>	__( 'Are You Sure, you want to delete this class?', 'wplms-braincert' ),
							'cancel_title'		=>	__( 'Are You Sure, you want to cancel this class?', 'wplms-braincert' ),
							'cancelall_title'	=>	__( 'Are You Sure, you want to cancel all recurring class?', 'wplms-braincert' ),
							'activate_title'	=>	__( 'Are You Sure, you want to activate this class?', 'wplms-braincert' ),
							'manage_title'		=>	__( 'Manage Recordings', 'wplms-braincert' ),
							'alert'				=>	__( 'Alert!', 'wplms-braincert' ),
							'remove_recoding'	=>	__( 'Class recording removed successfully!', 'wplms-braincert' ),
							'recoding_status'	=>	__( 'Changed class status successfully!', 'wplms-braincert' ),
							'recoding_status'	=>	__( 'Changed class status successfully!', 'wplms-braincert' ),
							'json_error'		=>	__( 'Please contact administrator!', 'wplms-braincert' ),
						);
			break;

			case 'common' :			
				$args = array(
							'ajax_url'			=>	admin_url('admin-ajax.php'),
							'nonce'				=>	wp_create_nonce( 'common_security_nonce' ),
							'load_more'			=>	__( 'No More Classes Available', 'wplms-braincert' ),
							'json_error'		=>	__( 'Please contact administrator!', 'wplms-braincert' ),
							'alert'				=>	__( 'Alert!', 'wplms-braincert' ),
						);
			break;

			case 'student' :				
				$args = array(							
						'ajax_url'			=>	admin_url('admin-ajax.php'),
						'nonce'				=>	wp_create_nonce( 'manage_security_nonce' ),
						'alert'				=>	__( 'Alert!', 'wplms-braincert' ),							
						'json_error'		=>	__( 'Please contact administrator!', 'wplms-braincert' ),							
						'preview_error'		=>	__( 'Sorry, can not show any preview', 'wplms-braincert' ),							
						'recording_title'	=>	__( 'View Recodings', 'wplms-braincert' ),		
					);

			break;
			
			default:
				$args = array();
			break;
			
		}
		return $args;
	}	
	
	protected function security_check( $action ){
		if ( isset( $_REQUEST['security'] ) && wp_verify_nonce( $_REQUEST['security'], $action ) ){
			return true;
		}
		wp_send_json_error( array( 'msg'=> __('Invalid security token sent.', 'wplms-braincert' ) ) );
	}	
	
	/**
	*	 schedule reminder mail cron job for single class
	*/
	private function schedule_cron( $class, $time_in_second = '' ){
		if( get_option('braincert_reminder_enabled' ) ){
			if( !$time_in_second ){
				$time = get_option('braincert_reminder_interval' );
				$time_in_second = $time * 60;
			}
				
			$args = array( (int)$class[ 'class_id'] );
			if( !wp_next_scheduled( 'wbc_class_reminder', $args ) )
				wp_schedule_single_event( $class[ 'start_ts' ] - $time_in_second, 'wbc_class_reminder', $args );
		}
	}	
	
	/**
	*	Unschedule reminder mail cron job for single class
	*/
	private function unschedule_cron( $class_id ){
		return wp_clear_scheduled_hook('wbc_class_reminder', array( (int)$class_id ) );
	}	
	
	/**
	*	Activate cron & schedules one time event for all upcomiong class
	*/
	protected function activate_cron( $time = '', $reschedule = 0 ){
		if( $reschedule ) 
			$this->deactivate_cron(); //Remove all cron Jobs
			
		if( !$time )
			$time = get_option('braincert_reminder_interval' );
		$time_in_second = $time * 60;
		
		global $wpdb;
		$args = array(
			'status'	=>  'upcoming',
			'per_page'	=>	1000
		);
		$classes = $this->get_classes( $args );
		foreach ($classes as $class ) 
			$this->schedule_cron( $class, $time_in_second );
	}
	
	/**
	*	Deactivate cron & deletes all scheduled cron jobs
	*/
	protected function deactivate_cron(){	
		
		$hook	= 'wbc_class_reminder';
		$crons 	= _get_cron_array();
		foreach ( $crons as $timestamp => $cron ) {
			if ( isset( $cron[ $hook ] ) ) {
				foreach( $cron[ $hook ] as $key => $single_event )
					wp_unschedule_event( $timestamp, $hook, $single_event[ 'args' ] );
			}
		}
		
		/**
		*	Optional code using DB
		global $wpdb;
		$args = array(
			'status'	=>  'upcoming',
			'per_page'	=>	1000
		);
		$classes = $this->get_classes( $args );
		foreach ($classes as $class ) {
			$this->unschedule_cron( $class[ 'class_id'] );
		}
		*/
	}
	
	public function extract_trainer( $status_obj ){
		if( is_array( $status_obj ) ){
			foreach ($status_obj as $key => $value) {
				if( $value->isTeacher == 1 ){
					return bp_core_get_user_displayname($value->userId);
				}
			}
		}
	}	


} // END class WPLMS_BrainCert_API