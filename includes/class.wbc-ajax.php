<?php
/**
 *
 * @class       WPLMS_BrainCert_Ajax
 * @author      Team WPGenius (Makarand Mane)
 * @category    Admin
 * @package     WPLMS-BrainCert/includes
 * @version     1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class WPLMS_BrainCert_Ajax extends WPLMS_BrainCert_API{
	public static $instance;
	public static function init(){

	    if ( is_null( self::$instance ) )
	        self::$instance = new WPLMS_BrainCert_Ajax();
	    return self::$instance;
	}

	private function __construct(){
		
		add_action('wp_ajax_create_front_class',		array( $this, 'add_wbc_frontend_class' ) );		
		add_action('wp_ajax_insert_class',				array( $this, 'add_braincert_class' ) );
		add_action('wp_ajax_delete_class',				array( $this, 'delete_braincert_class' ) );
		add_action('wp_ajax_cancel_class', 				array( $this, 'cancel_braincert_class' ) );
		add_action('wp_ajax_attendance_report',			array( $this, 'class_attendance_report' ) );
		add_action('wp_ajax_course_attendance_report',	array( $this, 'course_attendance_report' ) );
		add_action('wp_ajax_user_previews',				array( $this, 'wbc_user_previews' ) );
		add_action('wp_ajax_view_recording',			array( $this, 'wbc_view_recording' ) );		
		add_action('wp_ajax_manage_recording',			array( $this, 'wbc_manage_recording' ) );
		add_action('wp_ajax_recording_status',			array( $this, 'wbc_recording_status' ) );
		add_action('wp_ajax_remove_recording',			array( $this, 'wbc_remove_recording' ) );
		add_action('wp_ajax_load_classes', 				array( $this, 'loadmore_classes_ajax_callback' ) );
		add_action('wp_ajax_nopriv_load_classes', 		array( $this, 'loadmore_classes_ajax_callback' ) );
		add_action('wp_ajax_load_single_class', 		array( $this, 'load_single_class_callback' ) );
		add_action('wp_ajax_nopriv_load_single_class',	array( $this, 'load_single_class_callback' ) );		
		add_action('wp_ajax_get_calender_events',		array( $this, 'wbc_get_calender_events' ) );
		
		add_action('wp_ajax_class_search_form',			'wbc_class_search_form' );
		add_action('wp_ajax_tynymce_search_results',	array( $this, 'wbc_tynymce_search_results' ) );
		
	} // END public function __construct

	
	public function add_wbc_frontend_class(){

		$this->security_check( 'class_security_nonce' );
		if( $this->_is_admin() || $this->_is_instructor( $_GET['course_id'] ) ){
		
			$class_id		= 	isset( $_GET['class_id'] ) && !empty( $_GET['class_id'] )	? $_GET['class_id'] :'';
			$course_id		= 	isset( $_GET['course_id'] ) && !empty( $_GET['course_id'] ) ? $_GET['course_id'] : '';
			$instructors 	= 	$this->get_instructors( $course_id );
			$locations 		= 	$this->get_locations();
			$timezones 		= 	$this->get_timezones();
			$class_repeates = 	$this->get_class_repeat_types();
			$languages 		= 	$this->get_languages();
			$access 		= 	$this->get_whocansee();
			$class_type 	= 	$this->get_class_types();
			$currency_arr	=	$this->get_currency();
			$front			= 	true;

			$data = $this->_form_data( $class_id );	
	        extract( $data );	
			
			echo '<div class="row">';
			echo '<div class="col-md-12">';
			include(WBC_DIR_PATH.'templates/form/wbc-class-shedule.php');
			?>
				<script type="text/javascript">
				    init_wbc_form();
				</script>
			<?php

			echo '</div>';
			echo '</div>';
		} else {
			wp_send_json_error( array( 'msg'=>   __( 'Invalid user.', 'wplms-braincert' ) ) );	
		}

		die();
	}
	
	
	//Schedule class and insert class to db
	public function add_braincert_class(){	
		
		if ( check_ajax_referer( 'wbc_class_check', '_classNonce', false ) ) {

			if( $this->_is_admin() || $this->_is_instructor( $_POST['course'] ) ){
			
				unset( $_POST['_classNonce'] );
				unset( $_POST['_wp_http_referer'] );
				unset( $_POST['class_id'] );
				
				$this->validate_schedule( $_POST );
				
				$status = $this->getclass( $_POST );	
				
				$status_obj = json_decode( $status );
				
				if( $status_obj->status == 'ok' ){
					//db entry
					if( isset($_POST['cid']) && !empty( $_POST['cid'] ) ){
						$status_obj->db_status = $this->update_class( $status_obj->class_id );
						wp_send_json_success( $status_obj );  
					}
					else{
						$status_obj->db_status = $this->add_class( $status_obj->class_id );
						wp_send_json_success( $status_obj );  
					}
					
				} else {
					wp_send_json_error( array( 'msg'=> $status_obj->error ) );
				}
			} else {
				wp_send_json_error( array( 'msg'=>   __( 'Invalid user.', 'wplms-braincert' ) ) );
			}	
		
		} else {
			wp_send_json_error( array( 'msg'=> __( 'Invalid security token sent.', 'wplms-braincert' ) ) );
		}
		wp_die();
	}
	
	public function delete_braincert_class(){

		$this->security_check( 'manage_security_nonce' );
    	if( $this->_is_admin() || $this->_is_instructor( $_POST['course_id'] ) ){
    	
			$class_id = sanitize_text_field( $_POST['class_id'] );
			$data = array(
						'cid'		=>	$class_id,
						'format'	=>	'',
					);
			$status = $this->removeclass( $data );	
			
			$status_obj = json_decode( $status );
			
			if( $status_obj->status == 'ok'  ){
				//db entry
					$status_obj->db_status = $this->delete_class( $status_obj->class_id );
					wp_send_json_success( $status_obj );  
				
			} else {
				wp_send_json_error( array( 'msg'=> $status_obj->error ) );	
			}
		
		}  else {
			wp_send_json_error( array( 'msg'=>   __( 'Invalid user.', 'wplms-braincert' ) ) );
		}	
          	
	}

	public function cancel_braincert_class(){
		$isCancel = sanitize_text_field($_POST['isCancel']);
		$this->security_check( 'manage_security_nonce' );
    	if( $this->_is_admin() || $this->_is_instructor( $_POST['course_id'] ) ){

			$class_id = sanitize_text_field( $_POST['class_id'] );
			$data = array(
						'cid'		=>	$class_id,
						'format'	=>	'',
						'isCancel'	=>	$isCancel,
					);
			$status = $this->cancelclass( $data );	
			
			$status_obj = json_decode( $status );
			
			if( $status_obj->status == 'ok'  ){
				//db entry
				$status_obj->db_status = $this->cancel_class( $status_obj->class_id );
				if( $isCancel == 1 || $isCancel == 2){
					wp_send_json_success( 
						array(
							'class_id'		=>	$class_id,
							'activateClass' => '<span class="cancel-link active-'.$class_id.'"  data-value="0" data-id="'.$class_id.'" ><a href="'."#".'" class="cancelclass">'.__( 'Activate class', 'wplms-braincert' ).'</a></span>'
						)
					); 
				}else{
					wp_send_json_success( 
						array(
							'class_id'		=>	$class_id,
							'cancelCurrent' => '<span class="cancel-link cancle-'.$class_id.'"  data-value="0" data-id="'.$class_id.'" ><a href="'."#".'" class="cancelclass">'.__( 'Cancel', 'wplms-braincert' ).'</a></span>'
						)
					);
				}
				
			} else {
				
				wp_send_json_error( array( 'msg'=> $status_obj->error ) );	
			}
		}  else {
			wp_send_json_error( array( 'msg'=>   __( 'Invalid user.', 'wplms-braincert' ) ) );
		}	
	}

	public function course_attendance_report()
	{
		$this->security_check( 'manage_security_nonce' );
		if( $this->_is_admin() || $this->_is_instructor( $_POST['course_id'] ) ){
			global $wpdb;
			$args = array(
				'status'	=>  'completed',
				'course_id' =>	$_POST['course_id']
			);
			$classes 			= $this->get_classes( $args );
			if( count($classes) > 0 ){
				$html .= '<div class="row">';
				$html .= '<div class="col-md-12">';
				$html .= '<table class="wbc-course-report col-md-12">';
				$html .= '<thead>';
				$html .= '<th><span>'.__( 'class Id', 'wplms-braincert' ).'</span></th>';
				$html .= '<th><span>'.__( 'class Duration', 'wplms-braincert' ).'</span></th>';
				$html .= '<th><span>'.__( 'Start Time', 'wplms-braincert' ).'</span></th>';
				$html .= '<th><span>'.__( 'End Time', 'wplms-braincert' ).'</span></th>';
				$html .= '<th><span>'.__( 'User Name', 'wplms-braincert' ).'</span></th>';
				$html .= '<th><span>'.__( 'Teacher', 'wplms-braincert' ).'</span></th>';
				$html .= '<th><span>'.__( 'Conducted By', 'wplms-braincert' ).'</span></th>';
				$html .= '</thead>';
				$html .= '<tbody>';
		        foreach ($classes as $key=>$value) 
		        {
		        	$class_id = sanitize_text_field($value['class_id']);
					$data = array(
								'classId'		=>$class_id,
							);
					$status = $this->attendanceReport( $data );	
					
					$status_obj = json_decode( $status );
					$trainer 	= $this->extract_trainer($status_obj);
					if( is_array($status_obj) ){
						
						foreach ($status_obj as $key => $value) {
							$html .= '<tr class="course-attendance-list">';
							$html .= '<td>';
							$html .= '<span>'.$value->classId.'</span>';
							$html .= '</td>';
							$html .= '<td>';
							$html .= '<span>'.$value->duration.'</span>';
							$html .= '</td>';
							$time_in = $time_out = '';
							foreach ($value->session as $k => $v) {
								$time_in	.= $v->time_in.'<br>';
								$time_out	.= $v->time_out.'<br>';
							}
							$html .= '<td>';
							$html .= '<span>'.$time_in.'</span>';
							$html .= '</td>';
							$html .= '<td>';
							$html .= '<span>'.$time_out.'</span>';
							$html .= '</td>';
							$html .= '<td>';
							$html .= '<span>'.bp_core_get_user_displayname($value->userId).'</span>';
							$html .= '</td>';
							if( $value->isTeacher == 1 ){
								$html .= '<td>';
								$html .= '<span>'.__( "Yes", 'wplms-braincert' ).'</span>';
								$html .= '</td>';
							}else{
								$html .= '<td>';
								$html .= '<span>'.__( "No", 'wplms-braincert' ).'</span>';
								$html .= '</td>';
							}
							$html .= '<td>';
							$html .= '<span>'.$trainer.'</span>';
							$html .= '</td>';
							$html .= '</tr>';
						}
						

					}
					
		        }
		        $html .= '</tbody>';
				$html .= '</table>';
				$html .= '</div>';
				$html .= '</div>';
		        $html .= '<form action="'.esc_url( admin_url('admin-post.php') ).'" method="post">';
				$html .= '<input type="hidden" name="security" value="'.wp_create_nonce( 'download_courseattendace_nonce' ).'">';
				$html .= '<input type="hidden" name="courseId" value="'.$_POST['course_id'].'">';
				$html .= '<input type="hidden" name="action" value="download_courseattendace_report">';
				$html .= '<div class="download_report course_class_report"><button type="submit" class="download button">'.__( "Download", 'wplms-braincert' ).'</div></button>';
				$html .= '</form>'; 
		        wp_send_json_success( array( 'html'	=>	$html ) );

		    }else {
	        	wp_send_json_error( array( 'msg'=>   __( 'No record found', 'wplms-braincert' ) ) );
	        }
    	} else {
			wp_send_json_error( array( 'msg'=>   __( 'Invalid user.', 'wplms-braincert' ) ) );
		}	
	}




	public function class_attendance_report(){

		$this->security_check( 'manage_security_nonce' );
		if( $this->_is_admin() || $this->_is_instructor( $_POST['course_id'] ) ){
    	
			$class_id = sanitize_text_field( $_POST['class_id'] );
			$data = array(
						'classId'		=>	$class_id,
					);
			$status = $this->attendanceReport( $data );	
			
			$status_obj = json_decode( $status );
			$trainer 	= $this->extract_trainer($status_obj);
			if( is_array($status_obj) ){
				$html .= '<div class="row">';
				$html .= '<div class="col-md-12">';
				$html .= '<table class="wbc-report col-md-12">';
				$html .= '<thead>';
				$html .= '<th><span>'.__( 'Class Id', 'wplms-braincert' ).'</span></th>';
				$html .= '<th><span>'.__( 'Class Duration', 'wplms-braincert' ).'</span></th>';
				$html .= '<th><span>'.__( 'Start Time', 'wplms-braincert' ).'</span></th>';
				$html .= '<th><span>'.__( 'End Time', 'wplms-braincert' ).'</span></th>';
				$html .= '<th><span>'.__( 'User Name', 'wplms-braincert' ).'</span></th>';
				$html .= '<th><span>'.__( 'Teacher', 'wplms-braincert' ).'</span></th>';
				$html .= '<th><span>'.__( 'Conducted By', 'wplms-braincert' ).'</span></th>';
				$html .= '</thead>';
				$html .= '<tbody>';
				
				foreach ($status_obj as $key => $value) {
					$html .= '<tr class="attendance-list">';
					$html .= '<td>';
					$html .= '<span>'.$value->classId.'</span>';
					$html .= '</td>';
					$html .= '<td>';
					$html .= '<span>'.$value->duration.'</span>';
					$html .= '</td>';
					$time_in = $time_out = '';
					foreach ($value->session as $k => $v) {
						$time_in	.= $v->time_in.'<br>';
						$time_out	.= $v->time_out.'<br>';
					}
					$html .= '<td>';
					$html .= '<span>'.$time_in.'</span>';
					$html .= '</td>';
					$html .= '<td>';
					$html .= '<span>'.$time_out.'</span>';
					$html .= '</td>';
					$html .= '<td>';
					$html .= '<span>'.bp_core_get_user_displayname($value->userId).'</span>';
					$html .= '</td>';
					if( $value->isTeacher == 1 ){
						$html .= '<td>';
						$html .= '<span>'.__( "Yes", 'wplms-braincert' ).'</span>';
						$html .= '</td>';
					}else{
						$html .= '<td>';
						$html .= '<span>'.__( "No", 'wplms-braincert' ).'</span>';
						$html .= '</td>';
					}
					$html .= '<td>';
					$html .= '<span>'.$trainer.'</span>';
					$html .= '</td>';
					$html .= '</tr>';
				}

				$html .= '</tbody>';
				$html .= '</table>';
				$html .= '<form action="'.esc_url( admin_url('admin-post.php') ).'" method="post">';
				$html .= '<input type="hidden" name="security" value="'.wp_create_nonce( 'download_courseattendace_nonce' ).'">';
				$html .= '<input type="hidden" name="classId" value="'.$class_id.'">';
				$html .= '<input type="hidden" name="courseId" value="'.$_POST['course_id'].'">';
				$html .= '<input type="hidden" name="action" value="download_attendace_report">';
				$html .= '<div class="download_report"><button type="submit" class="download button">'.__( "Download", 'wplms-braincert' ).'</div></button>';
				$html .= '</form>'; 
				$html .= '</div>';
				$html .= '</div>';

				wp_send_json_success( array( 'html'	=>	$html ) );

			}else {
				wp_send_json_error( array( 'msg'=> $status_obj->error ) );
			}

		}  else {
			wp_send_json_error( array( 'msg'=>   __( 'Invalid user.', 'wplms-braincert' ) ) );
		}	
	}

	public function wbc_user_previews(){

		$this->security_check( 'manage_security_nonce' );
		
		if( isset( $_POST['class_id'] ) &&  isset( $_POST['course_id'] ) && ( $this->_is_admin() || $this->_is_instructor( $_POST['course_id'] ) || $this->_is_student( $_POST['course_id'] ) ) ){
    	
			$class_id 	= 	sanitize_text_field( $_POST['class_id'] );
			$user_id 	= 	isset( $_POST['user_id'] ) ? sanitize_text_field( $_POST['user_id'] ) : get_current_user_id();
			$isTeacher	=	isset( $_POST['isTeacher'] ) ? sanitize_text_field( $_POST['isTeacher'] ) : 0;
			
			$data		= $this->get_class( $class_id );
			$status_obj = $this->get_launchurl( $data, $user_id , $isTeacher, 'obj' );
			
			if( $status_obj->status == 'ok'  ){
					wp_send_json_success( $status_obj );  
				
			} else {
				wp_send_json_error( array( 'msg' => $status_obj->error ) );	
			}
			
		} else {
			wp_send_json_error(  array( 'msg' => __( 'You are not authorised to access this class. Try again with right credientials.','wplms-braincert' ) ) );
		}
         
	}

	public function wbc_view_recording(){

		$this->security_check( 'manage_security_nonce' );

		$class_id 	= sanitize_text_field( $_POST['class_id'] );
		$class 		= $this->get_class( $class_id, true );

		if( $this->_can( 'view_record', $class ) ) {
			
			$data = array(
						'class_id'		=>	$class_id,
					);
			$status = $this->getclassrecording( $data );	
			
			$status_obj = json_decode( $status );

			if( is_array( $status_obj ) ){
				$html .= '<div class="video-area row">';
				$html .= '<div class="video-list-area col-md-6">';
				$html .= '<h4>'.__( 'Recordings List : ', 'wplms-braincert' ).'</h4>';
				$html .= '<select name="videourl" id="videourl">';
				foreach($status_obj  as $k => $v){
					$html .= '<option value="'.$v->record_path.'">'.__( 'Recording-', 'wplms-braincert' ).''.$k.'</option>';
				}
				$html .= '</select>';
				$html .= '</div>';
				$html .= '<div class="video-play-area col-md-6">';
				foreach($status_obj  as $k => $v){
					$html .= '<video id="my-video" class="video-js vjs-default-skin" controls>';
					$html .= '<source src="'.$v->record_url.'" type="video/mp4">';
					$html .= '</video>';
				}
				$html .= '</div>';
				$html .= '</div>';
				wp_send_json_success( array( 'html'	=>	$html ) ); 

			}else if( $status_obj->Recording ){
				wp_send_json_success( array( 'msg' => $status_obj->Recording ) ); 
			}else if( $status_obj->status == "error" ){
				wp_send_json_success( array( 'msg' => $status_obj->error ) );
			}
		}
		wp_send_json_error( array( 'msg'=>   __( 'Invalid user or No recording avalable now.', 'wplms-braincert' ) ) );		
	}

	public function wbc_manage_recording(){

		$this->security_check( 'manage_security_nonce' );

		$class_id = sanitize_text_field( $_POST['class_id'] );
		$class 		= $this->get_class( $class_id);
		if( $this->_can( 'manage_record', $class ) ) {

			$data = array(
						'class_id'		=>	$class_id,
					);
			$status = $this->getclassrecording( $data );	
			
			$status_obj = json_decode( $status );

			if( is_array( $status_obj ) ){
				$html .= '<div class="row">';
				$html .= '<div class="col-md-12">';
				$html .= '<table class="wbc-record col-md-12">';
				$html .= '<thead>';
				$html .= '<th><span>'.__( 'Record Id', 'wplms-braincert' ).'</span></th>';
				$html .= '<th><span>'.__( 'File Name', 'wplms-braincert' ).'</span></th>';
				$html .= '<th><span>'.__( 'Date Created', 'wplms-braincert' ).'</span></th>';
				$html .= '<th><span>'.__( 'Action', 'wplms-braincert' ).'</span></th>';
				$html .= '</thead>';
				$html .= '<tbody>';
				
				foreach ($status_obj as $key => $value) {
					$html .= '<tr class="record-list record-'.$value->id.'">';
					$html .= '<td class="center">';
					$html .= '<span>'.$value->id.'</span>';
					$html .= '</td>';
					$html .= '<td class="center">';
					$html .= ( $value->fname ) ? '<span>'.$value->fname.'</span>' : '<span>'.$value->name.'</span>';
					$html .= '</td>';
					$html .= '<td class="center">';
					$html .= '<span>'.$value->date_recorded.'</span>';
					$html .= '</td>';
					$html .= '<td class="center">';
					$html .= '<div class="vc_tooltip wbc_actions">';

					$html .= '<form action="'.esc_url( admin_url('admin-post.php') ).'" method="post">';
					$html .= '<input type="hidden" name="security" value="'.wp_create_nonce( 'download_recording_nonce' ).'">';
					$html .= '<input type="hidden" name="id" value="'.$value->id.'">';
					$html .= '<input type="hidden" name="name" value="'.$value->name.'">';
					$html .= '<input type="hidden" name="action" value="download_recording">';
					$html .= '<button type="submit" class="download"><i class="icon-download"></i></button>';
					$html .= '</form>';

					$html .= '<span class="vc_tooltiptext">'.__( 'Download', 'wplms-braincert' ).'</span>';
					$html .= '</div>';
					$html .= '<div class="vc_tooltip wbc_actions">';
	                $html .= '<a href="#" class="recording_status" onclick="recording_status()" data-id="'.$value->id.'">';
	                $html .= ($value->status == 0 ) ? '<i class="icon-circle-blank"></i>' : '<i class="icon-ok"></i>';	
	                $html .= '</a>';		
	                $html .= ($value->status == 0 ) ? '<span class="vc_tooltiptext">'.__( 'Publish', 'wplms-braincert' ).'</span>' : '<span 								class="vc_tooltiptext">'.__( 'Unpublish', 'wplms-braincert' ).'</span>';			
	                $html .= '</div>';			
	                $html .= '<div class="vc_tooltip wbc_actions">';			
	                $html .= '<a href="#" class="remove_recording" onclick="remove_recording()" data-id="'.$value->id.'"><i class="icon-trash"></i></a>';
	                $html .= '<span class="vc_tooltiptext">'.__( 'Remove', 'wplms-braincert' ).'</span>';
	                $html .= '</div>';
					$html .= '</td>';
					$html .= '</tr>';
				}
				
				$html .= '</tbody>';
				$html .= '</table>';
				$html .= '</div>';
				$html .= '</div>';
				wp_send_json_success( array( 'html'	=>	$html, 'obj' => $status_obj ) ); 

			}else {
				if( $status_obj->Recording ){
					wp_send_json_success( array('Recording' => $status_obj->Recording) ); 
				}
			} 
		} else {
			wp_send_json_error( array( 'msg'=>   __( 'Invalid user.', 'wplms-braincert' ) ) );
		}	
		
	}

	public function wbc_recording_status(){

		$this->security_check( 'manage_security_nonce' );
		$recording_id = sanitize_text_field( $_POST['recording_id'] );
		$data = array(
					'id'		=>	$recording_id,
				);
		$status = $this->changestatusrecording( $data );	
		
		$status_obj = json_decode( $status );

		if( $status_obj->status == 'ok' ){
			wp_send_json_success( $status_obj );
		}else{
			wp_send_json_error( $status_obj );
		}

	}

	public function wbc_remove_recording(){

		$this->security_check( 'manage_security_nonce' );
        	
		$recording_id = sanitize_text_field( $_POST['recording_id'] );
		$data = array(
					'id'		=>	$recording_id,
				);
		$status = $this->removeclassrecording( $data );	
		
		$status_obj = json_decode( $status );

		if( $status_obj->status == 'ok' ){
			wp_send_json_success( array( 'msg' => __( 'Class recording removed successfully!', 'wplms-braincert' ) ) );
		}else{
			wp_send_json_error(  array( 'msg' =>   $status_obj->error ) );
		}

	}

	function load_single_class_callback(){

		$this->security_check( 'common_security_nonce' );
		
		if( isset( $_POST['class_id'] ) ){
			
			$class_id		=	$_POST['class_id'] ;		
			$default_image 	= 	vibe_get_option('default_course_avatar');
			$timezones		= 	$this->get_timezones();		
			$statuses		=	$this->get_statuses();	
			$date_format 	=	get_option( 'date_format' );
			$time_format 	=	get_option( 'time_format' );
			$timezone 		= 	$_POST['timezone'];
			
			$class = $this->get_class( $class_id, true );
	
			if( $class ){
				include( WBC_DIR_PATH.'templates/class-single.php' );
			} else {
				_e( '<p>Invalid Class. Please check class id.</p>','wplms-braincert');	
			}
		}
		die();
	}

	function loadmore_classes_ajax_callback(){

		$this->security_check( 'common_security_nonce' );
		$default_image 		= vibe_get_option('default_course_avatar');
		$timezones			= 	$this->get_timezones();		
		$statuses			=	$this->get_statuses();	
		$date_format 		=	get_option( 'date_format' );
		$time_format 		=	get_option( 'time_format' );
		$timezone 			= 	$_POST['timezone'];

		$args['course_id' ]	=	$_POST['course_id'];
		$args['status']		= 	$_POST['status'];
		$args['page'] 		= 	$_POST['page'];
		$args['per_page'] 	=	$_POST['per_page'];
				
		if( $_POST['view'] == 'dashboard' ){
			$args['instructor'] =	$_POST['instructor'];
			$args['whocansee'] 	= 	$_POST['access'];	
			if( isset( $_POST['student_id'] ) )	
				$args['student_id'] 	= 	$_POST['student_id'];			
		}
		$classes = $this->get_classes( $args );

		if( count( $classes ) > 0 ){
			if( $_POST['view'] == 'course' ){
				foreach ( $classes as $class ) {
					$output 	=  	include( WBC_DIR_PATH.'templates/class-single.php' );
				}
			}else{
				foreach ( $classes as $class ) {
					$output 	=  	include( WBC_DIR_PATH.'templates/class-dashboard.php' );
				}
			}

			echo '<div class="col-md-12 text-center" id="load_more_row">
						<button name="btn_more" id="btn_more" class="button">'.__( 'Load More', 'wplms-braincert' ).'</button>
				</div>';
		} else {
			?>
			<div class="class-notification">
				<div class="message">
					<span><?php _e( 'No classes available.', 'wplms-braincert' ) ?></span>
				</div>
			</div>
			<?php
		}
		die();
	}
	
	function wbc_get_calender_events(){

		$this->security_check( 'common_security_nonce' );
		
		$args = array(
			'status'	=>  'live_upcoming',
			'per_page'	=>  200
		);		//processs your code

		if( current_user_can('student') )	
			$args['student_id'] 	= 	get_current_user_id();			
	
		if( current_user_can('instructor') )	
			$args['instructor'] 	= 	get_current_user_id();	

		$classes	=	$this->get_classes( $args );
		$statuses	=	$this->get_statuses();
		$access		=	$this->get_whocansee();
		$timezone 	= 	isset( $_REQUEST['timezone'] ) ? $_REQUEST['timezone'] :get_option('timezone_string');
		$date_time_format = get_option( 'date_format' ).' '.get_option( 'time_format' );
		$cal = array();
		foreach ($classes as $class ) {
			$cal[] = array(
				'title' =>	$class['title'],
				'url' =>	$this->_can( 'launch', $class ) ? $this->get_launch_link( $class[ 'class_id'] ) :  $this->get_join_link( $class[ 'class_id'] ),
				'start'	=>	$this->date( $class[ 'start_ts' ], $timezone, 'Y-m-d'  ).'T'.$this->date( $class[ 'start_ts' ], $timezone, 'H:i:s'  ),
				'end'	=>	$this->date( $class[ 'end_ts' ], $timezone, 'Y-m-d'  ).'T'.$this->date( $class[ 'end_ts' ], $timezone, 'H:i:s'  ),
				'description' => array(
					'class'	=> $class['title'],
				),
				'extendedProps'	=>	array(
					'popup'	=>	array(
						'classTitle'	=>	__('Class Title','wplms-braincert').' : '.$class['title'],
						'courseTitle'	=>	__('Course Title','wplms-braincert').' : '.get_the_title($class['course_id'] ),
						'duration'		=>	__('Duration','wplms-braincert').' : '.sprintf( __( '%1$s Min.', 'wplms-braincert' ),  $class['duration']/60 ),
						'startTime'		=>	__('Start Time','wplms-braincert').' : '.$this->date( $class[ 'start_ts' ], $timezone, $date_time_format  ),
						'teacher'		=>	__('Teacher','wplms-braincert').' : '.bp_core_get_user_displayname($class['instructor_id']),
						'status'		=>	__('Status','wplms-braincert').' : '.$statuses[$class['status']],
						'access'		=>	__('Access','wplms-braincert').' : '.$access[$class['whocansee']],
					),
				),
			);
		}
		
		echo json_encode(  $cal  );
		wp_die();
	}
	
	public function wbc_tynymce_search_results(){		

		$this->security_check( 'tynymce_security_nonce' );
		wbc_tyneMCE_class_list( array( 'search' => $_POST['search'], 'course_id' => $_POST['course_id'] ) );
		die;

	}

}// END class WPLMS_BrainCert_Ajax