<?php
/**
 *
 * @class       Braincert
 * @author      Team WPGenius (Makarand Mane)
 * @category    Admin
 * @package     WPLMS-BrainCert/includes
 * @version     1.0
 */

class Braincert
{
	private $api_key;
	private $apiendpoint = "https://api.braincert.com/v2";
	 
	private function __construct ( ) {
	}
	
	public function get_option() {
		$this->api_key		= get_option('braincert_api_key');
		if( get_option('braincert_api_baseurl') )
			$this->apiendpoint	= get_option('braincert_api_baseurl');
	}
	
	protected function getclassdata($data=array())	{
		$data['task'] = 'getclass';
		return $this -> sendHttpRequest($data);
	}

	protected function getclass($data=array()) {
				
		$data['weekdays'] = implode(',', $data['weekdays']);
	
		if($data['record'] == '1' && $data['start_recording_auto'] == '2'){
			$data['record'] = '2';
		}

		if($data['allow_change_interface_language']==0){
			$data['isLang']=$data['language'];
	 	}else{
	 		$data['isLang']=11;
	 	}


	 	if($data['location_id']){
	 		$data['isRegion'] = $data['location_id'] ;
	 	}

		if($data['location']){
			$data['isRegion'] = $data['location'] ;
		}	

		$data['isBoard']=$data['classroom_type'];

		$data['class_id']=$data['cid'];
 	
 		unset($data['location_id']);
 		unset($data['location']);
		unset($data['start_recording_auto']);
		unset($data['allow_change_interface_language']);
		unset($data['classroom_type']);
		unset($data['language']);

		$data['task'] = 'schedule';
		return $this -> sendHttpRequest($data);
	}
	
	protected function getClassList($data=array())	{
		if($data['class_id']){
			$data['task'] = 'getclass';	
		}else{
			$data['task'] = 'listclass';
		}
		return $this -> sendHttpRequest($data);
	}
	protected function getPrice($data=array())	{
		$data['task'] = 'addSchemes';
		return $this -> sendHttpRequest($data);
	}
	
	protected function getDiscount($data=array()){
		$data['task'] = 'addSpecials';
		return $this -> sendHttpRequest($data);
	}
	protected function listdiscount($data=array()){
		$data['task'] = 'listdiscount';
		return $this -> sendHttpRequest($data);
	}
	protected function listPrices($data=array()){
		 
		$data['task'] = 'listSchemes';
		return $this -> sendHttpRequest($data);
	}
	 
	public function getclassrecording($data=array()){
		$data['task'] = 'getclassrecording';
		return $this -> sendHttpRequest($data);
	}
	protected function removeclassrecording($data=array()){
		$data['task'] = 'removeclassrecording';
		return $this -> sendHttpRequest($data);
	}
	protected function changestatusrecording($data=array()){
		$data['task'] = 'changestatusrecording';
		 
		return $this -> sendHttpRequest($data);
	}

	protected function downloadRecord( $data=array() ){
		
		$data['task'] = sanitize_text_field('downloadRecord');
		$result_data =  $this -> sendHttpRequest($data);

	 	header('Content-Transfer-Encoding: binary');  // For Gecko browsers mainly
		header('Content-Type: application/octet-stream');
		header('Content-Length:'.strlen($result_data));
		header('Content-Disposition: attachment; filename="'.$data['name'].'"');
		echo $result_data;
		exit;

	}

	protected function removediscount($data=array()){
		$data['task'] = 'removediscount';
		return $this -> sendHttpRequest($data);
	}
	protected function removeprice($data=array()){
		$data['task'] = 'removeprice';
		return $this -> sendHttpRequest($data);
	}
	protected function getlaunchurl($data=array()){
		$data['class_id']=$data['cid'];
		$data['instructor_id']=$data['userId'];
		$data['title']=$data['userName'];
		//$data['userId']=rand();
		//var_dump($data['userId']);
		$data['task'] = 'getclasslaunch';
		return $this -> sendHttpRequest($data);
	}
	
	protected function removeclass($data=array()) {
		$data['task'] = 'removeclass';
		return $this -> sendHttpRequest($data);
	}
	protected function getrecording($data=array()) {
		$data['task'] = 'getrecording';
		return $this -> sendHttpRequest($data);
	}

	protected function cancelclass($data=array()) {
		$data['class_id']=$data['cid'];
		$data['task'] = 'cancelclass';
		return $this -> sendHttpRequest($data);
	}

	protected function attendanceReport($data=array()) {
		$data['task'] = 'getclassreport';
        return $this -> sendHttpRequest($data);
	}

	private function sendHttpRequest($data){
		if( !$this->api_key ) $this->get_option();
		$data['apikey'] = $this -> api_key;
	    $data_string = http_build_query($data);
	 	$this -> apiendpoint = $this -> apiendpoint;//."/".$data['task'];
		$ch = curl_init($this -> apiendpoint);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$result = curl_exec($ch);

		return $result;
	}
}