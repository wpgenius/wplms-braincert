<?php
/**
 *
 * @class       WPLMS_BrainCert_Settings
 * @author      Team WPGenius (Makarand Mane)
 * @category    Admin
 * @package     WPLMS-BrainCert/includes
 * @version     1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class WPLMS_BrainCert_Settings extends WPLMS_BrainCert_API{

	public static $instance;
	private $prefix		= 'braincert_';
	private $opt_grp	= 'braincert_api_';
	private $page		= 'braincert_settings';
	
	public static function init(){

	    if ( is_null( self::$instance ) )
	        self::$instance = new WPLMS_BrainCert_Settings();
	    return self::$instance;
	}

	private function __construct(){

		add_action( 'admin_menu', array($this,'braincert_settings_menu'), 11);
		add_action( 'admin_init', array($this,'braincert_register_settings'),10);
		
		add_action( 'update_option_'.$this->prefix.'reminder_enabled', 	array( $this,'class_reminder_update_cron_job'	), 10, 3);
		add_action( 'update_option_'.$this->prefix.'reminder_interval', array( $this,'class_reminder_update_cron_time'	), 10, 3);

	} // END public function __construct

	function braincert_settings_menu(){

		add_submenu_page(
			'braincert_classes',
			__('Braincert Classroom Settings','wplms-braincert' ), // page title
			__('Settings','wplms-braincert' ), // menu title
			'manage_options', // capability
			$this->page, // menu slug
			array( $this, 'braincert_api_settings')
		);
	}

	function braincert_register_settings() {
		
		//Register settings
	    register_setting( $this->opt_grp, $this->prefix.'api_key',			array( 'type' => 'string', 'default' => '', 'description' => 'Get API key here https://www.braincert.com/app/virtualclassroom' ) );
	    register_setting( $this->opt_grp, $this->prefix.'api_baseurl',		array( 'type' => 'string', 'default' => 'https://api.braincert.com/v2' ) );
	    register_setting( $this->opt_grp, $this->prefix.'class_per_page',	array( 'type' => 'string', 'default' => '5' ) );
	    register_setting( $this->opt_grp, $this->prefix.'reminder_enabled',	array( 'type' => 'boolean', 'default' => 0 ) );
	    register_setting( $this->opt_grp, $this->prefix.'reminder_interval',array( 'type' => 'number', 'default' => '30' ) );
	    register_setting( $this->opt_grp, $this->prefix.'default_location',	array( 'type' => 'string', 'default' => '15' ) );
	    register_setting( $this->opt_grp, $this->prefix.'default_timezone',	array( 'type' => 'string', 'default' => '72' ) );
	    register_setting( $this->opt_grp, $this->prefix.'api_frontend',		array( 'type' => 'boolean', 'default' => 0 ) );
	    register_setting( $this->opt_grp, $this->prefix.'delete_db',		array( 'type' => 'boolean', 'default' => 0 ) );		
	    register_setting( $this->opt_grp, $this->prefix.'delete_settings',	array( 'type' => 'boolean', 'default' => 0 ) );
		
		//Register sections
		add_settings_section( $this->prefix.'api_section',		__('BrainCert API','wplms-braincert'),			array( $this, 'braincert_api_section_title' ),	$this->page );
		add_settings_section( $this->prefix.'genaral',	 		__('General preference','wplms-braincert'),		array( $this, 'braincert_genaral_title' ),	$this->page );
		add_settings_section( $this->prefix.'email',	 		__('E-mail preference','wplms-braincert'),		array( $this, 'braincert_email_title' ),	$this->page );
		add_settings_section( $this->prefix.'form_defaults',	__('Class schedule form default preference','wplms-braincert'),	array( $this, 'braincert_form_defaults_title' ),	$this->page );
		add_settings_section( $this->prefix.'instructor', 		__('Instructor preference','wplms-braincert'),	array( $this, 'braincert_instructor_title' ),	$this->page );
		add_settings_section( $this->prefix.'remove_section', 	__('Uninstall preference','wplms-braincert'),	array( $this, 'braincert_remove_section_title' ),$this->page );
		
		//Add settings to section- braincert_api_section 
		add_settings_field( $this->prefix.'api_key',	__('BrainCert API Key :','wplms-braincert'), array( $this, 'braincert_api_key_field' ), 	$this->page, $this->prefix.'api_section', array( 'label_for' => $this->prefix.'api_key' ) );
		add_settings_field( $this->prefix.'api_baseurl',__('BrainCert Base URL:','wplms-braincert'), array( $this, 'braincert_api_baseurl_field' ), $this->page, $this->prefix.'api_section', array( 'label_for' => $this->prefix.'api_baseurl' ) );
		
		//Add settings to section- braincert_api_section 
		add_settings_field( $this->prefix.'class_per_page',	__('Class per page :','wplms-braincert'), array( $this, 'braincert_class_per_page_field' ), 	$this->page, $this->prefix.'genaral', array( 'label_for' => $this->prefix.'class_per_page' ) );
		
		//Class reminder settings
		add_settings_field( $this->prefix.'reminder_enabled',	__('Class reminder cron job:','wplms-braincert'), array( $this, 'braincert_reminder_enabled_field' ), 	$this->page, $this->prefix.'email', array( 'label_for' => 'reminder_enabled' ) );
		add_settings_field( $this->prefix.'reminder_interval',	__('Schedule reminder Email (in minutes):','wplms-braincert'), array( $this, 'braincert_reminder_interval_field' ), 	$this->page, $this->prefix.'email', array( 'label_for' => $this->prefix.'reminder_interval' ) );
		
		//Add settings to section- braincert_api_section 
		add_settings_field( $this->prefix.'default_location',	__('Default location :','wplms-braincert'), array( $this, 'braincert_default_location_field' ), 	$this->page, $this->prefix.'form_defaults', array( 'label_for' => $this->prefix.'default_location' ) );
		add_settings_field( $this->prefix.'default_timezone',	__('Default location :','wplms-braincert'), array( $this, 'braincert_default_timezone_field' ), 	$this->page, $this->prefix.'form_defaults', array( 'label_for' => $this->prefix.'default_timezone' ) );
		
		//Add settings to section- braincert_instructor 
		add_settings_field( $this->prefix.'api_frontend',__('Allow instructor to schedule classes from frontend:','wplms-braincert'), array( $this, 'braincert_api_frontend' ), $this->page, $this->prefix.'instructor', array( 'label_for' => 'api_frontend' ) );
		
		//Add settings to section- braincert_remove_section 
		add_settings_field( $this->prefix.'delete_db',__('Delete Classroom database table:','wplms-braincert'), array( $this, 'braincert_delete_db_field' ), $this->page, $this->prefix.'remove_section', array( 'label_for' => 'delete_db' ) );
		add_settings_field( $this->prefix.'delete_settings',__('Delete settings:','wplms-braincert'), array( $this, 'braincert_delete_settings_field' ), $this->page, $this->prefix.'remove_section', array( 'label_for' => 'delete_settings' ) );
		
	}
	
	function braincert_api_settings(){
		?>
        <div class="wrap">
    	
            <h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
            
            <form method="POST" action="options.php">
				<?php
					// output security fields for the registered setting "wporg"
					settings_fields( $this->opt_grp );
					// output setting sections and their fields
					// (sections are registered for "wporg", each field is registered to a specific section)
					do_settings_sections( $this->page );
					// output save settings button
					submit_button( __( 'Save Settings','wplms-braincert') );
                 ?>
            </form>
        </div>
        <?php
		
	}
	
	function braincert_api_section_title(){
		?>
		<p><?php _e( 'Get API details from https://www.braincert.com/app/virtualclassroom & put below.','wplms-braincert'); ?></p>
        <?php 
	}
	
	function braincert_genaral_title(){?>
		<p><?php _e( 'Choose general preference for braincert classes for WPLMS below.','wplms-braincert'); ?></p>
        <?php 
	}
	
	function braincert_email_title(){?>
		<p><?php _e( 'Choose preference for e-mail for braincert classes below.','wplms-braincert'); ?></p>
        <?php 
	}	
	
	function braincert_form_defaults_title(){?>
		<p><?php _e( 'Choose default preference for braincert class schedule form.','wplms-braincert'); ?></p>
        <?php 
	}
	
	function braincert_instructor_title(){
	}
	
	function braincert_remove_section_title(){
		?>
		<p><?php _e( 'Choose plugin uninstallation preference below.','wplms-braincert'); ?></p>
        <?php 
	}
	
	function braincert_api_key_field(){
		?>
       	<input type='text' name='<?php echo $this->prefix ?>api_key' id='<?php echo $this->prefix ?>api_key' value='<?php echo get_option( $this->prefix.'api_key' );?>' style="width: 300px;">
        <?php
	}
	
	function braincert_api_baseurl_field(){
		?>
       	<input type='text' name='<?php echo $this->prefix ?>api_baseurl' id='<?php echo $this->prefix ?>api_baseurl' value='<?php echo get_option( $this->prefix.'api_baseurl' ); ?>' style="width: 300px;">
        <?php
	}
	
	function braincert_class_per_page_field(){
		?>
       	<input type='number' min="1" max="50" name='<?php echo $this->prefix ?>class_per_page' id='<?php echo $this->prefix ?>class_per_page' value='<?php echo get_option( $this->prefix.'class_per_page' );?>' style="width: 300px;">
        <?php
	}
	
	function braincert_reminder_enabled_field(){
		?>
       	 <label><input type="radio" name="<?php echo $this->prefix ?>reminder_enabled" <?php checked( get_option( $this->prefix.'reminder_enabled' ), 1 ); ?> value="1" id="reminder_enabled"> <?php _e('Enable','wplms-braincert'); ?></label>
         <label><input type="radio" name="<?php echo $this->prefix ?>reminder_enabled" <?php checked( get_option( $this->prefix.'reminder_enabled' ), 0 ); ?> value="0"> <?php _e('Disable','wplms-braincert'); ?></label>
         <p>When you disable class reminder, you won't change time to schedule reminder email below.</p>
        <?php
	}
	
	function braincert_reminder_interval_field(){
		?>
       	<input type='number' min="5" max="7200" name='<?php echo $this->prefix ?>reminder_interval' id='<?php echo $this->prefix ?>reminder_interval' value='<?php echo get_option( $this->prefix.'reminder_interval' );?>' style="width: 300px;" <?php if( !get_option( $this->prefix.'reminder_enabled' ) ) echo 'readonly="readonly"';  ?> />
        <p>To send reminder to students for class, enter time in minutes. This field will be editable only when enable Class reminder. Accuracy of email depends upon site traffic and resources.</p>
        <?php
	}
	
	function braincert_default_location_field(){
		$locations		= $this->get_locations();	
		$location_id 	= get_option( $this->prefix.'default_location' );
		?>        
        <select name="<?php echo $this->prefix ?>default_location" id="<?php echo $this->prefix ?>default_location" class="wbc_select2" required style="width: 300px;">
            <option value="" disabled <?php echo $location_id ? 'selected' : ''; ?>><?php _e('Auto select nearest datacenter region','wplms-braincert'); ?></option>
            <?php foreach ($locations as $value => $label) { 
                echo '<option value="'.$value.'" '.selected( $location_id, $value, false).'>'.$label.'</option>';
            } ?>
        </select>
        <?php
	}
	
	function braincert_default_timezone_field(){
		$timezones	= $this->get_timezones();
		$timezone	= get_option( $this->prefix.'default_timezone' );
		?>
        <select name="<?php echo $this->prefix ?>default_timezone" id="<?php echo $this->prefix ?>default_timezone" class="wbc_select2" required style="width: 300px;">
			<?php foreach ($timezones as $value => $label) { ?>
                <option title="<?php echo $label; ?>" value="<?php echo $value; ?>" <?php selected( $value, $timezone); ?>><?php echo $label; ?></option>
            <?php } ?>
        </select>
        <?php
	}
	
	function braincert_api_frontend(){
		?>
       	 <input type="radio" name="<?php echo $this->prefix ?>api_frontend" <?php checked( get_option( $this->prefix.'api_frontend' ), 1 ); ?> value="1" id="api_frontend"> <?php _e('Yes','wplms-braincert'); ?>
         <input type="radio" name="<?php echo $this->prefix ?>api_frontend" <?php checked( get_option( $this->prefix.'api_frontend' ), 0 ); ?> value="0"> <?php _e('No','wplms-braincert'); ?>
        <?php
	}
	
	function braincert_delete_db_field(){
		?>
       	 <input type="radio" name="<?php echo $this->prefix ?>delete_db" <?php checked( get_option( $this->prefix.'delete_db' ), 1 ); ?> value="1"> <?php _e('Yes','wplms-braincert'); ?>
         <input type="radio" name="<?php echo $this->prefix ?>delete_db" <?php checked( get_option( $this->prefix.'delete_db' ), 0 ); ?> value="0" id="delete_db"> <?php _e('No','wplms-braincert'); ?>
        <?php	
	}
	
	function braincert_delete_settings_field(){
		?>
       	 <input type="radio" name="<?php echo $this->prefix ?>delete_settings" <?php checked( get_option( $this->prefix.'delete_settings' ), 1 ); ?> value="1"> <?php _e('Yes','wplms-braincert'); ?>
         <input type="radio" name="<?php echo $this->prefix ?>delete_settings" <?php checked( get_option( $this->prefix.'delete_settings' ), 0 ); ?> value="0" id="delete_settings"> <?php _e('No','wplms-braincert'); ?>
        <?php	
	}
	
	function class_reminder_update_cron_job( $old_value, $value, $option ){
		if( $value ) 
			$this->activate_cron();
		else 
			$this->deactivate_cron();
	}

	
	function class_reminder_update_cron_time( $old_value, $value, $option ){
		if( get_option( $this->prefix.'reminder_enabled' ) ) {
			$this->activate_cron( $value, 1 );
		}
	}

} // END class WPLMS_BrainCert_Settings