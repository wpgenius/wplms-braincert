<?php
/**
 *
 * @class       WPLMS_BrainCert_Actions
 * @author      Team WPGenius (Makarand Mane)
 * @category    Admin
 * @package     WPLMS-BrainCert/includes
 * @version     1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class WPLMS_BrainCert_Actions extends WPLMS_BrainCert_API{

	public static $instance;
	public static function init(){

	    if ( is_null( self::$instance ) )
	        self::$instance = new WPLMS_BrainCert_Actions();
	    return self::$instance;
	}

	private function __construct(){		
		$this->get_option();		
		//Frontend
		add_action( 'wp_enqueue_scripts',	array($this, 'enqueue_scripts'));
		//Add class widget after course description
		add_action( 'wplms_after_course_description',	array( $this, 'wbc_frontend_course' ));
		
		add_filter( 'wplms_touch_points', array( $this, 'wbc_touch_points' ), 10 );
		
		add_action( 'class_scheduled',	array( $this, 'class_scheduled_mail' ),	10,	2);
		add_action( 'class_updated',	array( $this, 'class_updated_mail' ),	10,	2);
		add_action( 'class_deleted',	array( $this, 'class_deleted_mail' ),	10,	2);
		add_action( 'class_canceled',	array( $this, 'class_canceled_mail' ),	10,	2);
		//class_reminder
		add_action( 'class_reminder',	array( $this, 'class_reminder_mail' ),	10,	2);		
		//class_reminder cron hook
		add_action( 'wbc_class_reminder',	array( $this, 'class_reminder_cron' ),	10 );
		
		add_filter( 'bp_email_use_wp_mail', '__return_true ' );
		
		add_action( 'template_redirect',array( $this, 'complete_class_action' ) );
		
		add_action('admin_post_download_recording',				array( $this, 'wbc_download_recording' ) );
		add_action('admin_post_download_attendace_report',		array( $this, 'wbc_download_courseattendace_report' ) );	
		add_action('admin_post_download_courseattendace_report',array( $this, 'wbc_download_courseattendace_report' ) );
		
	} // END public function __construct

	function enqueue_scripts( ){
		
		if( is_admin() )
			return;
			
		wp_enqueue_style('wbc_css',WBC_DIR_URL.'assets/css/style.css');
		
		if( is_singular( 'course' ) || bp_is_current_component( 'dashboard' ) ){
			
			//css
			//wp_enqueue_style( 'select2', WC()->plugin_url() . '/assets/css/select2.css' );
			
			wp_enqueue_script( 'infinite_scroll', WBC_DIR_URL. 'assets/js/infinite-scroll.pkgd.js', array('jquery'));
			wp_register_script( 'scroll-options', WBC_DIR_URL.'assets/js/scroll-options.js', array('jquery','infinite_scroll') );
			wp_localize_script( 'loadmore_ajax', 'loadmore_parameters', array('ajaxurl' => admin_url( 'admin-ajax.php' ),) );
			wp_enqueue_script( 'scroll-options' );
			//Class popup form
			wp_enqueue_style( 'jquery-confirm', WBC_DIR_URL.'assets/css/jquery-confirm.css' );
			wp_enqueue_script( 'jquery-confirm', WBC_DIR_URL.'assets/js/jquery-confirm.js',array('jquery'));
			
			//js
			if( $this->_is_admin() || current_user_can( 'instructor' ) ){
				//Form css
				wp_enqueue_style( 'jquery-ui-calendar', WBC_DIR_URL.'assets/css/jquery-ui-calendar.css' );
				wp_enqueue_style( 'jquery-timepicker', WBC_DIR_URL.'assets/css/jquery.timepicker.css' );				
				//Class popup form -js 
				wp_enqueue_script( 'modal-popup', WBC_DIR_URL.'assets/js/bootstrap-modal.js',array('jquery'));
				//Form JS
				wp_enqueue_script( 'jquery-ui-datepicker' ); 
				wp_enqueue_script( 'jquery-timepicker' ,WBC_DIR_URL.'assets/js/jquery.timepicker.js', array('jquery') );
				wp_enqueue_script( 'wbc-forms', WBC_DIR_URL.'assets/js/wbc-forms.js' ,array( 'jquery', /*'select2',*/ 'jquery-ui-datepicker', 'jquery-timepicker'));
				//Manage class js
				wp_enqueue_script( 'wbc-manage', WBC_DIR_URL.'assets/js/wbc-manage.js' ,array( 'jquery'));
	
				wp_localize_script( 'wbc-forms', 'wbc_form', $this->get_localise( 'class' ) );
				wp_localize_script( 'wbc-manage', 'wbc_manage', $this->get_localise( 'manage' ) );
			} elseif ( bp_is_current_component( 'dashboard' ) || $this->_is_student() ) {
				wp_enqueue_script( 'wbc-students', WBC_DIR_URL.'assets/js/wbc-students.js' ,array( 'jquery'));
-				wp_localize_script( 'wbc-students', 'wbc_student', $this->get_localise( 'student' ) );
			}
		}
		
		wp_enqueue_script( 'wbc-common', WBC_DIR_URL.'assets/js/wbc-common.js' ,array( 'jquery'));
		wp_localize_script( 'wbc-common', 'wbc_common', $this->get_localise( 'common' ) );

		if( bp_is_current_component( 'dashboard' ) ){
			//calender js css
			wp_enqueue_style('tooltip_css', 	WBC_DIR_URL.'assets/css/tooltip.css');		
			wp_enqueue_style('cal_core_css', 	WBC_DIR_URL.'assets/packages/core/main.css');		
			wp_enqueue_style('cal_list_css', 	WBC_DIR_URL.'assets/packages/list/main.css');
			wp_enqueue_style('cal_daygrid_css', WBC_DIR_URL.'assets/packages/daygrid/main.css');
			wp_enqueue_style('cal_timegrid_css',WBC_DIR_URL.'assets/packages/timegrid/main.css');
			//Calender JS		
			wp_enqueue_script('cal_core_js', 	WBC_DIR_URL.'assets/packages/core/main.js');			
			wp_enqueue_script('cal_list_js', 	WBC_DIR_URL.'assets/packages/list/main.js');					
			wp_enqueue_script('cal_daygrid_js', WBC_DIR_URL.'assets/packages/daygrid/main.js');				
			wp_enqueue_script('cal_timegrid_js',WBC_DIR_URL.'assets/packages/timegrid/main.js');				
			wp_enqueue_script('wbc-calender', 	WBC_DIR_URL.'assets/js/wbc-calender.js', array( 'jquery', 'wbc-common', 'cal_core_js', 'cal_daygrid_js', 'cal_timegrid_js', 'cal_list_js' ) );
		}
	}
	
	public function wbc_frontend_course(){
		
		global $post;
		$course_id 			= $post->ID;
		$class_count		= $this->get_course_classes_count( $course_id );
		
		if( $class_count < 1 && !$this->_is_admin() && !$this->_is_instructor( $course_id ) )
			return true;
		
		$status_filters 	= $this->get_statuses();
		$status_counts		= $this->get_statuses_counts( array('course_id' => $course_id) );
		?>
		<div class="wbc-class row single-wbc-class">
			<div class="col-md-12">
				<h3 class="heading">
					<span><?php _e('Course Classes','wplms-braincert') ?></span>
					<div class="classcount"><i class="fa fa-desktop"> </i>  <?php echo $class_count; ?></div>
				</h3>
				<?php if( $this->_is_guest() ){ ?>
					<div class="class-notification">
						<div class="message">
							<span><?php _e( 'Only course member can join or view classes.', 'wplms-braincert' ) ?></span>
							<?php if( !is_user_logged_in() ){ ?>
								<span><a class="lrm-login" href="<?php echo esc_url( wp_login_url( get_permalink() ) ); ?>" id="wbc_login"><strong><?php _e( 'Login if you are a student', 'wplms-braincert' ) ?></strong></a></span>
							<?php } ?> 
						</div>
					</div>
				<?php } ?>
				
				<div class="pull-left col-1">
					<form id="wbc_class_filters">
						<div class="class-filters">
							<div class="filter_by_status">
								<select name="status" onchange="refresh_filters();">
									<?php 
									foreach ($status_filters as $value => $label) {
										echo '<option value="'.$value.'" '.selected( $value, $status_filters, false ).'" >'.$label.' ('.( isset($status_counts[$value] ) ? $status_counts[$value] : "0").')</option>';
									}
									?>
								</select>
							</div>
						</div>
						<input type="hidden" name="per_page" value="<?php  echo get_option('braincert_class_per_page', 5); ?>" />
						<input type="hidden" name="course_id" value="<?php echo $course_id; ?>" />
						<input type="hidden" name="page" value="1" id="page_count" />                    
	                    <input type="hidden" name="view" value="course" id="class_view" />
						<input type="hidden" name="timezone" value="<?php echo get_option( 'timezone_string' ); ?>" id="class_timezone" />
					</form>
				</div>
				<div class="pull-right col-2 admin_classes">
					<?php if( $this->_is_admin() || $this->_is_instructor( $results['course_id'] ) ) { ?>
					<a title="><?php _e('Create New Class','wplms-braincert') ?>" data-toggle="modal" data-target="#classModal" onclick="wbc_create_class_modal(this)" data-id="<?php echo $course_id ?>" class="btn-sm btn-success button add-class-btn" id="front-class"><?php _e('New Class','wplms-braincert') ?></a>
					<?php } ?>
					<?php if( $this->_is_admin() || $this->_is_instructor( $results['course_id'] ) ) { ?>
					<a title="<?php _e('Click here to download attendance report','wplms-braincert') ?>"  data-courseid="<?php echo $course_id; ?>" href="#" class="add-class-btn btn-sm btn-success button course_attendance"><?php _e('Attendance Report','wplms-braincert') ?></a>
					<?php }?>
				</div>
			</div>

			<div class="dash-class-widget"></div>

			<div class="wbc-loading text-center">
			    <img id="loading-image" src="<?php echo WBC_DIR_URL.'assets/images/loading.gif' ?>"/>
			</div>
		</div>
		<?php
	}  
	
	public function wbc_touch_points( $settings ){
		
		$settings[ 'class_updates' ] = array(
									'label'	=> __('Class updates','wplms-braincert'),
									'name'	=>'class_updates',
									'value' => array(
										'student' => admin_url('edit.php?taxonomy=bp-email-type&term=class_scheduled&post_type=bp-email'),
										'instructor' => admin_url('edit.php?taxonomy=bp-email-type&term=class_scheduled&post_type=bp-email'),
									),
									'type' => 'touchpoint',
									'hook' => 'class_updates',
									'params'=> 1, 
							);
		$settings[ 'class_reminder' ] = array(
									'label'	=> __('Class reminder','wplms-braincert'),
									'name'	=>'class_reminder',
									'value' => array(
										'student' => admin_url('edit.php?taxonomy=bp-email-type&term=class_reminder&post_type=bp-email'),
										'instructor' => admin_url('edit.php?taxonomy=bp-email-type&term=class_reminder&post_type=bp-email'),
									),
									'type' => 'touchpoint',
									'hook' => 'class_reminder',
									'params'=> 1, 
							);
		return $settings;
	}
	
	private function process_mails( $situation, $course_id, $args, $touch_point, $data = array(), $link = 0 ){

		if( $link  )
			$args[ 'tokens' ][ 'class.link' ] =  $this->get_join_link( $data[ 'class_id' ] );
			
		if(class_exists('WPLMS_tips')){
            $wplms_settings = WPLMS_tips::init();
            if(!empty($wplms_settings->lms_settings) && !empty($wplms_settings->lms_settings['touch']))
            	$lms_settings = $wplms_settings->lms_settings['touch'];
            
        }
		
		if(!empty($lms_settings)){
						
			if( !empty($lms_settings[$touch_point]['student']['email'])){
				// get list of students
				$students = $this->get_course_active_students( $course_id );	
				// send args and user ID to receive email
				if( $students )
					foreach( $students as $student){
						$enable = get_user_meta( $student, 'student_'.$touch_point , true);
						if( $enable !== 'no'){
							$args[ 'tokens' ][ 'student.link' ] = esc_url( bp_core_get_user_domain($student).'/dashboard/' );
							bp_send_email( $situation, (int) $student, $args );
						}						
					}
			}
	
			if( !empty($lms_settings[$touch_point]['instructor']['email'])){
				// get class instructor
				$instructor  = $data[ 'instructor_id' ];
				$enable = get_user_meta( $instructor, 'instructor_'.$touch_point , true);
				if( $enable !== 'no'){
					$args[ 'tokens' ][ 'student.link' ] = esc_url( bp_core_get_user_domain($instructor).'/dashboard/' );
					bp_send_email( $situation, (int) $instructor, $args );
				}
					
			}
			
		}
		
	}
		
	public function class_scheduled_mail( $class_id, $data ){
		if ( $class_id ) {
			extract( $data );	  	
			// add tokens to parse in email
			$args = array(
				'tokens' => array(
					'class.title'	=> $title,
					'course.title'	=> get_the_title( $course_id ),
					'course.link'	=> get_permalink( $course_id ),
					'class.datetime'=> date_i18n( get_option( 'date_format' ).' '.get_option( 'time_format' ), strtotime( $date.' '.$start_time ) ),
					'class.timezone'=> $timezone_label,
				),
			);
			
			$this->process_mails( 'class_scheduled', $course_id, $args, 'class_updates', $data, 1);
		}	
	}
	
	
	public function class_updated_mail( $class_id, $data ){
		if ( $class_id ) {
			extract( $data );
			// add tokens to parse in email
			$args = array(
				'tokens' => array(
					'class.title'	=> $title,
					'course.title'	=> get_the_title( $course_id ),
					'course.link'	=> get_permalink( $course_id ),
					'class.datetime'=> date_i18n( get_option( 'date_format' ).' '.get_option( 'time_format' ), strtotime( $date.' '.$start_time ) ),
					'class.timezone'=> $timezone_label,
				),
			);
			
			$this->process_mails( 'class_updated', $course_id, $args, 'class_updates', $data, 1);
		}
	}
	
	public function class_deleted_mail( $class_id, $data ){
		if ( $class_id ) {
			extract( $data );
			// add tokens to parse in email
			$args = array(
				'tokens' => array(
					'class.title'	=> $title,
					'course.title'	=> get_the_title( $course_id ),
					'course.link'	=> get_permalink( $course_id ),
				),
			);
			
			$this->process_mails( 'class_deleted', $course_id, $args, 'class_updates', $data);
		}
	}
	
	public function class_canceled_mail( $class_id, $data ){
		if ( $class_id ) {
			extract( $data );
			// add tokens to parse in email
			$args = array(
				'tokens' => array(
					'class.title'	=> $title,
					'course.title'	=> get_the_title( $course_id ),
					'course.link'	=> get_permalink( $course_id ),
				),
			);
			
			$this->process_mails( 'class_canceled', $course_id, $args, 'class_updates', $data);
		}
	}
	
	public function class_reminder_mail( $class_id, $data ){
		if ( $class_id ) {
			extract( $data );
			// add tokens to parse in email
			$args = array(
				'tokens' => array(
					'class.title'	=> $title,
					'course.title'	=> get_the_title( $course_id ),
					'course.link'	=> get_permalink( $course_id ),
					'class.datetime'=> date_i18n( get_option( 'date_format' ).' '.get_option( 'time_format' ), strtotime( $date.' '.$start_time ) ),
					'class.timezone'=> $timezone_label,
				),
			);
			
			$this->process_mails( 'class_reminder', $course_id, $args, 'class_reminder', $data, 1);
		}
	}
	
	public function class_reminder_cron( $class_id ){
		
		if( get_option( 'braincert_reminder_enabled' ) ){ 
			$data = $this->get_class( $class_id );
			do_action( 'class_reminder', $class_id, $data );
		}
		else
			$this->deactivate_cron();
	}
	
	public function complete_class_action( $query ){		
		global $wp;
		if( array_key_exists( 'completeClass', $wp->query_vars ) ){
		
			if( isset( $_GET['roomId'] ) && $_GET['roomId']  ){
				$class_id = $_GET['roomId'];
				//If teacher then mark class as complete
				if( isset( $_GET['isTeacher'] ) && $_GET['isTeacher'] == 1  )
					$this->complete_class( $class_id );
				//If user is logged in then redirect to dashboard
				if( is_user_logged_in() ){
					$user_id = isset( $_GET['userId'] ) ? $_GET['userId'] : get_current_user_id();				
					if( wp_redirect( bp_core_get_user_domain( $user_id  ).'/dashboard/' ) );
					exit;						
				}
				//Else redirect to course
				$data = $this->get_class( $class_id );
				if( wp_redirect( get_permalink( $data['course_id'] ) ) )
					exit;
			}
			
			//If fails then redirect to home
			wp_redirect( home_url() );
			exit;
		}	
		
		if( array_key_exists( 'joinClass', $wp->query_vars ) ){
			$class_id =  get_query_var('joinClass');
			if( !is_user_logged_in() ){			
				if( wp_redirect( wp_login_url( $this->get_join_link( $class_id ) ) ) );
				exit;						
			}
			$class = $this->get_class( $class_id, true );
			$user_id = get_current_user_id();
			if( $this->_can( 'join', $class ) ) { 
				if(  wp_redirect( $this->get_launchurl( $class, $user_id ) )  );
					exit;
			}
			if( wp_redirect( get_permalink( $class['course_id'] ) ) )
				exit;
			//If fails then redirect to home
			wp_redirect( home_url() );
			exit;
			
		}	
		if( array_key_exists( 'launchClass', $wp->query_vars ) ){
			$class_id =  get_query_var('launchClass');
			if( !is_user_logged_in() ){			
				if(  wp_redirect( wp_login_url( $this->get_launch_link( $class_id ) ) )  );
				exit;						
			}
			$class = $this->get_class( $class_id, true );
			$user_id = get_current_user_id();
			if( $this->_can( 'launch', $class ) ) { 
				if(  wp_redirect( $this->get_launchurl( $class, $user_id , 1) )  );
					exit;
			} 
			if( wp_redirect( get_permalink( $class['course_id'] ) ) )
				exit;
			//If fails then redirect to home
			wp_redirect( home_url() );
			exit;
		}
		
		if( !is_user_logged_in() ){
			//redirect dashboard url to login first insted of 404
			$url = 'https://' . $_SERVER['SERVER_NAME'] .$_SERVER['REQUEST_URI'];
			if(  strpos( strtok( $url, '?' ), 'dashboard') !== false ){
				wp_redirect( esc_url( add_query_arg( array( 'redirect-to' => $url ), get_permalink( wc_get_page_id( 'myaccount' ) ) ) ) );
				exit();
			}
		}
	}

	public function wbc_download_recording(){

		if ( isset( $_REQUEST['security'] ) && wp_verify_nonce( $_REQUEST['security'], 'download_recording_nonce' ) ){
			$recording_id = sanitize_text_field( $_POST['recording_id'] );
			$name = sanitize_text_field( $_POST['name'] );
			$data = array(
						'id'		=>	$recording_id,
						'name'		=>	$name,
					);
			$status = $this->downloadRecord( $data );
		}
		wp_die( __( "Invalid security token sent.", 'wplms-braincert' ) );
	}

	function wbc_download_courseattendace_report(){
		if ( isset( $_REQUEST['security'] ) && wp_verify_nonce( $_REQUEST['security'], 'download_courseattendace_nonce' ) ){			
			if( $this->_is_admin() || $this->_is_instructor( $_POST['courseId'] ) ){
				
				$classes =  array( );
	
				if( isset( $_POST['classId'] ) && !empty( $_POST['classId'] ) ){
					$classes[] = array( 'class_id' => sanitize_text_field( $_POST['classId'] ) );
					$id	= __( "Class", 'wplms-braincert' ).'_'.sanitize_text_field( $_POST['classId'] );
				} else {
					global $wpdb;
					$args = array(
						'status'	=>  'completed',
						'per_page'	=>	10000
					);
					if( isset( $_POST['courseId'] ) && $_POST['courseId'] ){
						$args['course_id' ]	=	$_POST['courseId'];
					}
					$classes = $this->get_classes( $args );
					$id	= __( "Course", 'wplms-braincert' ).'_'.sanitize_text_field( $_POST['courseId'] );
				}
	
				header('Content-Type: application/csv');
				header('Content-Disposition: attachment; filename=Attendance_Report-'.$id.'-'.time().'.csv;');
	
				$f = fopen('php://output', 'w');
				fputcsv($f, array( __( "Class Id", 'wplms-braincert' ), __( "Class Duration", 'wplms-braincert' ), __( "Start Time - End Time", 'wplms-braincert' ), __( "User Name", 'wplms-braincert' ), __( "Teacher", 'wplms-braincert' ), __( "Conducted By", 'wplms-braincert' )), ',');
	
				foreach ($classes as $class ) 
				{
					$data 		= array( 'classId'		=> $class['class_id']);
					$status_obj	= $this->attendanceReport( $data );	
					$records	= json_decode( $status_obj );
					$trainer	= $this->extract_trainer( $records );
					
					if( is_array($records) ){
						
						foreach ($records as $record) {
							$session = '';
							foreach ( $record->session as $k => $v) {
								$session	.= $v->time_in.'-'.$v->time_out.';';
							}
	
							$arr = array(
									$record->classId, $record->duration, str_replace('&nbsp;', ' ', $session), bp_core_get_user_displayname($record->userId), ( $record->isTeacher == 1 ? 'Yes' : 'No') , $trainer
								);
	
							fputcsv($f, $arr, ',');
						}
					   
					}
				}
				fclose( $f );
				exit;
			}
			wp_die( __( "You are not authorised to download report. If you think it is techincal error, please contact administrator.", 'wplms-braincert' ) );	
		}
		wp_die( __( "Invalid security token sent.", 'wplms-braincert' ) );	
	}
		
} // END class WPLMS_BrainCert_Actions