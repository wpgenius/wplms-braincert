<?php
/**
 *
 * @class       WPLMS_BrainCert_DB
 * @author      Team WPGenius (Makarand Mane)
 * @category    Admin
 * @package     WPLMS-BrainCert/includes
 * @version     1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class WPLMS_BrainCert_DB extends WPLMS_BrainCert_API{

	public function __construct(){
		/*
		* https://code.tutsplus.com/tutorials/custom-database-tables-creating-the-table--wp-28124
		* #blog #gist
		*/
		add_action( 'init', array( $this, 'wplms_braincert_endpoint'), 1 );
		add_action( 'init', array( $this, 'wplms_braincert_classes_table'), 1 );
		add_action( 'switch_blog', array( $this, 'wplms_braincert_classes_table') );
		add_action( 'bp_core_install_emails', array( $this, 'reinstall_braincert_mails') );

	} // END public function __construct
	
	public function wplms_braincert_endpoint(){
		add_rewrite_endpoint( 'completeClass', EP_ROOT );
		
		add_rewrite_rule( '^joinClass/([0-9]+)/?',	'index.php?joinClass=$matches[1]', 'top');
		add_rewrite_rule( '^launchClass/([0-9]+)/?', 'index.php?launchClass=$matches[1]', 'top');
		add_rewrite_tag( '%launchClass%',	'([^&]+)' );
		add_rewrite_tag( '%joinClass%',		'([^&]+)' );
		
		if( !get_option('wbc_permalinks_flushed') ) { 
			flush_rewrite_rules(false);
			update_option('wbc_permalinks_flushed', 1);	 
		}
	}
	
	public function wplms_braincert_classes_table() {
		global $wpdb;
		$wpdb->wbc_classes = "{$wpdb->prefix}wplms_braincert_classes";
		$wpdb->wbc_timezones = "{$wpdb->prefix}wplms_braincert_timezones";
	}
	
	public function reinstall_braincert_mails() {
		$this->install_braincert_mails();
	}
	
	public function activate_braincert() {
		$this->install_braincert_classes_database();
		$this->install_braincert_mails();
		//as Cron jobs are not scheduled while plugin activation, I have added a cron job. It invokes a hook function for update option.
		wp_schedule_single_event( time() + 30 , 'update_option_braincert_reminder_enabled', array(  0, get_option('braincert_reminder_enabled' ), 'braincert_reminder_enabled' ) );
		update_option('wbc_permalinks_flushed', 0);
		flush_rewrite_rules();
	}
	
	public function deactivate_braincert() {
		$this->deactivate_cron();
		delete_option('wbc_permalinks_flushed');
		flush_rewrite_rules();
	}
	
	public function uninstall_braincert() {		
		$this->delete_braincert_classes_database();
		$this->delete_braincert_settings();
	}

	private function install_braincert_classes_database(){
		global $wpdb;
		
		$table_name = $wpdb->prefix . "wplms_braincert_classes"; 
		$charset_collate = $wpdb->get_charset_collate();
		$sql = array();
		
		$sql[] = "CREATE TABLE IF NOT EXISTS $table_name (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `class_id` int(11) NOT NULL COMMENT 'Class ID from Braincert',
				  `course_id` int(11) NOT NULL COMMENT 'WPLMS course ID',
				  `instructor_id` int(11) NOT NULL COMMENT 'User ID in WordPress of instructor',
				  `location_id` int(11) NOT NULL COMMENT 'Location ID from Braincert',
				  `title` text NOT NULL COMMENT 'Class Title',
				  `description` longtext NOT NULL COMMENT 'Class Description',
				  `date` date NOT NULL COMMENT 'Class Date',
				  `start_time` time NOT NULL COMMENT 'Class start time',
				  `end_time` time NOT NULL COMMENT 'Class end time',
				  `start_ts` int(11) DEFAULT NULL,
				  `end_ts` int(11) DEFAULT NULL,
				  `timezone` int(10) NOT NULL COMMENT 'Time Zone',
				  `timezone_country` text NOT NULL COMMENT 'Country name of timezone, required to calculate timestamp in PHP',
				  `timezone_label` text NOT NULL COMMENT 'Label of timezone country, just for display purpose',
				  `difftime` varchar(20) NOT NULL COMMENT 'GMT Time diffrence',
				  `isCancel` tinyint(2) NOT NULL DEFAULT '0' COMMENT 'Cancel class: 0.active, 1.cancel current class, 2.cancel recurring class',
				  `isComplete` tinyint(2) NOT NULL DEFAULT '0',
				  `is_recurring` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Recurring Class.1:Yes,0:No',
				  `repeat` int(11) DEFAULT NULL COMMENT 'When class repeats.1:Daily (all 7 days),2:6 Days(Mon-Sat),3:5 Days(Mon-Fri),4:Weekly,5:Once every month,6:On selected days',
				  `weekdays` text COMMENT 'When class repeats.1: Sun,2:Mon,3:Tue,4:Wed,5:Thu,6:Fri,7:Sat',
				  `afterclasses` tinyint(2) DEFAULT '0' COMMENT 'Ends after some class or end on some date.0:After, 1:Ends on particular date',
				  `end_classes_count` int(11) DEFAULT NULL COMMENT 'No of class count when recurring classes will ends',
				  `end_date` date DEFAULT NULL COMMENT 'End date for recurring class.1:Ends on',
				  `allow_change_interface_language` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Allow attendees to change interface language.1:Yes,2:No',
				  `language` int(11) NOT NULL DEFAULT '11' COMMENT 'Force Interface Language. Default to english',
				  `record` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Record this class.1:Yes,0:No',
				  `isRecordingLayout` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Recorded videos layout.0:Standard view,1:Enhanced view',
				  `start_recording_auto` int(11) DEFAULT '1' COMMENT 'Start recording automatically when class starts.2:Yes,1:No',
				  `isControlRecording` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Allow instructor to control recording.1:Yes,0:No',
				  `isVideo` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Video delivery.0:Multiple video files,1:Single video file',
				  `classroom_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Classroom type.0:whiteboard + audio/video + attendee list + chat,1:whiteboard + attendee list,2:whiteboard + attendee list + chat',
				  `isCorporate` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Enable webcam and microphone upon entry.0:No,1:Yes',
				  `isPrivateChat` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Enable private chat.0:Yes,1:No',
				  `viewRecording` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Allow View recording. 0:No, 1:Yes',
				  `isScreenshare` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Enable screen sharing.0:No,1:Yes',
				  `whocansee` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Access Mode. 0:Public, 1:Private',
				  `ispaid` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Class type. 0:Free, 1:Paid',
				  `currency` varchar(5) NOT NULL DEFAULT 'usd' COMMENT 'Currency',
				  `seat_attendees` int(11) NOT NULL COMMENT 'Maximum attendees',
				  `duration` smallint(6) NOT NULL COMMENT 'Duration received from API',
				  PRIMARY KEY (`id`),
				  UNIQUE KEY `class_id` (`class_id`),
				  KEY `course_id` (`course_id`),
				  KEY `instructor_id` (`instructor_id`)
				) ENGINE=InnoDB DEFAULT CHARSET=$charset_collate;
				COMMIT;";
			
		$table_name = $wpdb->prefix . "wplms_braincert_timezones";
		
		$sql[] = "CREATE TABLE IF NOT EXISTS $table_name (
				  `timezone_id` int(11) NOT NULL,
				  `timezone_country` varchar(50) NOT NULL,
				  `timezone_label` varchar(100) NOT NULL,
				  `difftime` varchar(20) NOT NULL,
				  `timezone_title` text NOT NULL
				) ENGINE=InnoDB DEFAULT  CHARSET=$charset_collate;";
				
		$sql[] = "INSERT INTO $table_name (`timezone_id`, `timezone_country`, `timezone_label`, `difftime`, `timezone_title`) VALUES
				(1, 'Asia/Dubai', 'Arabian Standard Time', '+04:00', '(GMT+04:00) Abu Dhabi, Muscat'),
				(2, 'Asia/Baghdad', 'Arabic Standard Time', '+03:00', '(GMT+03:00) Baghdad'),
				(3, 'Canada/Atlantic', 'Atlantic Standard Time', '-03:00', '(GMT-04:00) Atlantic Time (Canada)'),
				(4, 'Australia/Darwin', 'AUS Central Standard Time', '+09:30', '(GMT+09:30) Darwin'),
				(5, 'Australia/Canberra', 'AUS Eastern Standard Time', '+11:00', '(GMT+10:00) Canberra, Melbourne, Sydney'),
				(6, 'Atlantic/Azores', 'Azores Standard Time', '-01:00', '(GMT-01:00) Azores'),
				(7, 'Canada/Saskatchewan', 'Canada Central Standard Time', '-06:00', '(GMT-06:00) Saskatchewan'),
				(8, 'Atlantic/Cape_Verde', 'Cape Verde Standard Time', '-01:00', '(GMT-01:00) Cape Verde Is.'),
				(9, 'Asia/Baku', 'Caucasus Standard Time', '+04:00', '(GMT+04:00) Baku, Tbilisi, Yerevan'),
				(10, 'Australia/Adelaide', 'Cen. Australia Standard Time', '+10:30', '(GMT+09:30) Adelaide'),
				(11, 'America/Belize', 'Central America Standard Time', '-06:00', '(GMT-06:00) Central America'),
				(12, 'Asia/Dhaka', 'Central Asia Standard Time', '+06:00', '(GMT+06:00) Astana, Dhaka'),
				(13, 'Europe/Belgrade', 'Central Europe Standard Time', '+01:00', '(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague'),
				(14, 'Europe/Sarajevo', 'Central European Standard Time', '+01:00', '(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb'),
				(15, 'Pacific/Guadalcanal', 'Central Pacific Standard Time', '+11:00', '(GMT+11:00) Magadan, Solomon Is., New Caledonia'),
				(16, 'America/Chicago', 'Central Standard Time', '-05:00', '(GMT-06:00) Central Time (US & Canada)'),
				(17, 'Asia/Hong_Kong', 'China Standard Time', '+08:00', '(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi'),
				(18, 'Etc/GMT-12', 'Dateline Standard Time', '+12:00', '(GMT-12:00) International Date Line West'),
				(19, 'Africa/Addis_Ababa', 'E. Africa Standard Time', '+03:00', '(GMT+03:00) Nairobi'),
				(20, 'Australia/Brisbane', 'E. Australia Standard Time', '+10:00', '(GMT+10:00) Brisbane'),
				(21, 'Europe/Athens', 'E. Europe Standard Time', '+02:00', '(GMT+02:00) Athens'),
				(22, 'America/Sao_Paulo', 'E. South America Standard Time', '-03:00', '(GMT-03:00) Brasilia'),
				(23, 'America/New_York', 'Eastern Standard Time', '-04:00', '(GMT-05:00) Eastern Time (US & Canada)'),
				(24, 'Africa/Cairo', 'Egypt Standard Time', '+02:00', '(GMT+02:00) Cairo'),
				(25, 'Asia/Yekaterinburg', 'Ekaterinburg Standard Time', '+05:00', '(GMT+05:00) Ekaterinburg'),
				(26, 'Pacific/Fiji', 'Fiji Standard Time', '+12:00', '(GMT+12:00) Fiji, Kamchatka, Marshall Is.'),
				(27, 'Europe/Helsinki', 'FLE Standard Time', '+02:00', '(GMT+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius'),
				(28, 'Europe/London', 'GMT Standard Time', '+00:00', '(GMT) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London'),
				(29, 'America/Godthab', 'Greenland Standard Time', '-03:00', '(GMT-03:00) Greenland'),
				(30, 'Africa/Abidjan', 'Greenwich Standard Time', '+00:00', '(GMT) Monrovia, Reykjavik'),
				(31, 'Europe/Minsk', 'Istanbul, Minsk Standard Time', '+03:00', '(GMT+03:00) Istanbul, Minsk'),
				(32, 'Pacific/Honolulu', 'Hawaiian Standard Time', '-10:00', '(GMT-10:00) Hawaii'),
				(33, 'Asia/Kolkata', 'India Standard Time', '+05:30', '(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi'),
				(34, 'Asia/Tehran', 'Iran Standard Time', '+03:30', '(GMT+03:30) Tehran'),
				(35, 'Asia/Jerusalem', 'Jerusalem Standard Time', '+02:00', '(GMT+02:00) Jerusalem'),
				(37, 'America/Mexico_City', 'Mexico Standard Time', '-06:00', '(GMT-06:00) Guadalajara, Mexico City, Monterrey'),
				(38, 'America/Chihuahua', 'Mexico Standard Time', '-07:00', '(GMT-07:00) Chihuahua, La Paz, Mazatlan'),
				(39, 'America/Noronha', 'Mid-Atlantic Standard Time', '-02:00', '(GMT-02:00) Mid-Atlantic'),
				(40, 'America/Denver', 'Mountain Standard Time', '-06:00', '(GMT-07:00) Mountain Time (US & Canada)'),
				(41, 'Asia/Rangoon', 'Myanmar Standard Time', '+06:30', '(GMT+06:30) Rangoon'),
				(42, 'Asia/Novosibirsk', 'N. Central Asia Standard Time', '+07:00', '(GMT+06:00) Almaty, Novosibirsk'),
				(44, 'Pacific/Auckland', 'New Zealand Standard Time', '+13:00', '(GMT+12:00) Auckland, Wellington'),
				(45, 'Canada/Newfoundland', 'Newfoundland Standard Time', '-02:30', '(GMT-03:30) Newfoundland'),
				(46, 'Asia/Irkutsk', 'North Asia East Standard Time', '+08:00', '(GMT+08:00) Irkutsk, Ulaan Bataar'),
				(47, 'Asia/Kabul', 'Afghanistan Standard Time', '+04:30', '(GMT+04:30) Kabul'),
				(48, 'America/Anchorage', 'Alaskan Standard Time', '-08:00', '(GMT-09:00) Alaska'),
				(49, 'Asia/Riyadh', 'Arab Standard Time', '+03:00', '(GMT+03:00) Kuwait, Riyadh'),
				(50, 'Asia/Krasnoyarsk', 'North Asia Standard Time', '+07:00', '(GMT+07:00) Krasnoyarsk'),
				(51, 'America/Santiago', 'Pacific SA Standard Time', '-03:00', '(GMT-04:00) Santiago'),
				(52, 'America/Los_Angeles', 'Pacific Standard Time', '-07:00', '(GMT-08:00) Pacific Time (US & Canada)'),
				(53, 'Europe/Brussels', 'Romance Standard Time', '+01:00', '(GMT+01:00) Brussels, Copenhagen, Madrid, Paris'),
				(54, 'Europe/Moscow', 'Russian Standard Time', '+03:00', '(GMT+03:00) Moscow, St. Petersburg, Volgograd'),
				(55, 'America/Argentina/Buenos_Aires', 'SA Eastern Standard Time', '-03:00', '(GMT-03:00) Buenos Aires, Georgetown'),
				(56, 'America/Bogota', 'SA Pacific Standard Time', '-05:00', '(GMT-05:00) Bogota, Lima, Quito'),
				(57, 'America/La_Paz', 'SA Western Standard Time', '-04:00', '(GMT-04:00) Georgetown, La Paz, San Juan'),
				(58, 'Pacific/Samoa', 'Samoa Standard Time', '-11:00', '(GMT-11:00) Midway Island, Samoa'),
				(59, 'Asia/Bangkok', 'SE Asia Standard Time', '+07:00', '(GMT+07:00) Bangkok, Hanoi, Jakarta'),
				(60, 'Asia/Kuala_Lumpur', 'Malay Peninsula Standard Time', '+08:00', '(GMT+08:00) Kuala Lumpur, Singapore'),
				(61, 'Africa/Blantyre', 'South Africa Standard Time', '+02:00', '(GMT+02:00) Harare, Pretoria'),
				(62, 'Asia/Colombo', 'Sri Lanka Standard Time', '+05:30', '(GMT+05:30) Sri Jayawardenepura'),
				(63, 'Asia/Taipei', 'Taipei Standard Time', '+08:00', '(GMT+08:00) Taipei'),
				(64, 'Australia/Hobart', 'Tasmania Standard Time', '+11:00', '(GMT+10:00) Hobart'),
				(65, 'Asia/Tokyo', 'Tokyo Standard Time', '+09:00', '(GMT+09:00) Osaka, Sapporo, Tokyo'),
				(67, 'America/Indiana/Indianapolis', 'US Eastern Standard Time', '-04:00', '(GMT-05:00) Indiana (East)'),
				(68, 'America/Phoenix', 'US Mountain Standard Time', '-07:00', '(GMT-07:00) Arizona'),
				(69, 'Asia/Vladivostok', 'Vladivostok Standard Time', '+10:00', '(GMT+10:00) Vladivostok'),
				(70, 'Australia/Perth', 'W. Australia Standard Time', '+08:00', '(GMT+08:00) Perth'),
				(71, 'Africa/Algiers', 'W. Central Africa Standard Time', '+01:00', '(GMT+01:00) West Central Africa'),
				(72, 'Europe/Amsterdam', 'W. Europe Standard Time', '+01:00', '(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna'),
				(73, 'Asia/Karachi', 'West Asia Standard Time', '+05:00', '(GMT+05:00) Islamabad, Karachi, Tashkent'),
				(74, 'Pacific/Guam', 'West Pacific Standard Time', '+10:00', '(GMT+10:00) Guam, Port Moresby'),
				(75, 'Asia/Yakutsk', 'Yakutsk Standard Time', '+09:00', '(GMT+09:00) Yakutsk'),
				(76, 'America/Caracas', 'Venezuela Standard Time', '-04:00', '(GMT-04:30) Caracas'),
				(77, 'Asia/Seoul', 'Korea Standard Time', '+09:00', '(GMT+09:00) Seoul'),
				(83, 'Asia/Amman', 'Jordan Standard Time', '+02:00', '(GMT+02:00) Amman'),
				(84, 'Asia/Beirut', 'Middle East Standard Time', '+02:00', '(GMT+02:00) Beirut'),
				(86, 'Africa/Windhoek', 'Namibia Standard Time', '+02:00', '(GMT+02:00) Windhoek'),
				(87, 'Asia/Tbilisi', 'Georgian Standard Time', '+04:00', '(GMT+03:00) Tbilisi'),
				(88, 'Asia/Baku', 'Azerbaijan Standard Time', '+04:00', '(GMT+04:00) Baku'),
				(89, 'Indian/Mauritius', 'Mauritius Standard Time', '+04:00', '(GMT+04:00) Port Louis'),
				(90, 'Asia/Karachi', 'Pakistan Standard Time', '+05:00', '(GMT+05:00) Islamabad, Karachi'),
				(91, 'Asia/Kathmandu', 'Nepal Standard Time', '+05:45', '(GMT+05:45) Kathmandu'),
				(94, 'America/Argentina/Buenos_Aires', 'Argentina Standard Time', '-03:00', '(GMT-03:00) Buenos Aires'),
				(95, 'America/Montevideo', 'Montevideo Standard Time', '-03:00', '(GMT-03:00) Montevideo'),
				(96, 'America/Manaus', 'Central Brazilian Standard Time', '-04:00', '(GMT-04:00) Manaus'),
				(104, 'America/Tijuana', 'Pacific Standard Time (Mexico)', '-07:00', '(GMT-08:00) Tijuana, Baja California'),
				(105, 'America/New_York', 'Eastern Daylight Time', '-04:00', '(GMT-04:00) Eastern Daylight  Time (US & Canada)');";

		$sql[] = "ALTER TABLE $table_name
 				 ADD UNIQUE KEY `timezone_id` (`timezone_id`);";

		$sql[] = "ALTER TABLE $table_name
  					MODIFY `timezone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;COMMIT;";

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		
		foreach($sql as $query )
			dbDelta( $query );
	}
	
	private function install_braincert_mails() {
		
		$mails = array(
					//class scheduled
					array(
						'post_title'    => __( '[{{{site.name}}}] New Class is Scheduled.', 'wplms-braincert' ),
						'post_content'  => __( "<p>We're here to notify you that a new class <strong>{{class.title}}</strong> is scheduled under <strong><a href='{{course.link}}' target='_blank'>{{course.title}}</a></strong> course. The class starting on <strong>{{class.datetime}}</strong> based on <strong>{{class.timezone}}</strong>.</p>

<p>Click on this <a href='{{class.link}}' target='_blank'>link</a> to Join the Class directly or <a href='{{student.link}}' target='_blank'>visit</a> your dashboard to see a list of your upcoming classes.</p>", 'wplms-braincert' ),  // HTML email content.
						'post_excerpt'  => __( "Hi {{recipient.name}},
						
We're here to notify you that a new class {{class.title}} is scheduled under {{course.title}} course. The class starting on {{class.datetime}} based on {{class.timezone}}.

Click on this {{class.link}} to Join the Class directly or visit your dashboard ({{student.link}}) to see a list of your upcoming classes.

Kind Regards,
{{site.name}}", 'wplms-braincert' ),  // Plain text email content.
						'situation'		=> 'class_scheduled',
						'description'	=> 'An instructor schedules new class'
					),
					//Class updated
					array(
						'post_title'    => __( '[{{{site.name}}}] Class has been updated.', 'wplms-braincert' ),
						'post_content'  => __( "<p>We're here to notify you that the <strong>{{class.title}}</strong> class under <strong><a href='{{course.link}}' target='_blank'>{{course.title}}</a></strong> course has been updated. The class starting on <strong>{{class.datetime}}</strong> based on <strong>{{class.timezone}}</strong>.</p>

<p>Click on this <a href='{{class.link}}' target='_blank'>link</a> to Join the Class directly or <a href='{{student.link}}' target='_blank'>visit</a> your dashboard to see a list of your upcoming classes.</p>", 'wplms-braincert' ),  // HTML email content.
						'post_excerpt'  => __( "Hi {{recipient.name}},
						
We're here to notify you that the {{class.title}} class under {{course.title}} course has been updated. The class starting on {{class.datetime}} based on  {{class.timezone}}.

Click on this {{class.link}} to Join the Class directly or visit your dashboard ({{student.link}}) to see a list of your upcoming classes.

Kind Regards,
{{site.name}}", 'wplms-braincert' ),  // Plain text email content.
						'situation'		=> 'class_updated',
						'description'	=> 'An instructor updated class'
					),
					//Class deleted
					array(
						'post_title'    => __( '[{{{site.name}}}] Class has been Deleted.', 'wplms-braincert' ),
						'post_content'  => __( "<p>We're here to notify you that <strong>{{class.title}}</strong> class under <strong><a href='{{course.link}}' target='_blank'>{{course.title}}</a></strong> course has been deleted.</p>
						
<p>You can <a href='{{student.link}}' target='_blank'>visit</a> your dashboard to see a list of your upcoming classes.</p>", 'wplms-braincert' ),  // HTML email content.
						'post_excerpt'  => __( "Hi {{recipient.name}},
						
We're here to notify you that {{class.title}} class under {{course.title}} course has been deleted.

You can your dashboard ({{student.link}}) your dashboard to see a list of your upcoming classes.

Kind Regards,
{{site.name}}", 'wplms-braincert' ),  // Plain text email content.
						'situation'		=> 'class_deleted',
						'description'	=> 'An instructor deleted class'
					),
					//Class cancelled
					array(
						'post_title'    => __( '[{{{site.name}}}] Class has been Cancelled.', 'wplms-braincert' ),
						'post_content'  => __( "<p>We're here to notify you that <strong>{{class.title}}</strong> class under <strong><a href='{{course.link}}' target='_blank'>{{course.title}}</a></strong> course has been cancelled.</p>
						
<p>You can <a href='{{student.link}}' target='_blank'>visit</a> your dashboard to see a list of your upcoming classes.</p>", 'wplms-braincert' ),  // HTML email content.
						'post_excerpt'  => __( "Hi {{recipient.name}},
						
We're here to notify you that {{class.title}} class under {{course.title}} course has been cancelled.

You can your dashboard ({{student.link}}) your dashboard to see a list of your upcoming classes.

Kind Regards,
{{site.name}}", 'wplms-braincert' ),  // Plain text email content.
						'situation'		=> 'class_canceled',
						'description'	=> 'An instructor cancelled class'
					),
					//Class Reminder
					array(
						'post_title'    => __( '[{{{site.name}}}] Class reminder.', 'wplms-braincert' ),
						'post_content'  => __( "<p>We would like to remind you that <strong>{{class.title}}</strong> class is going to start soon. The class is related to the <strong><a href='{{course.link}}' target='_blank'>{{course.title}}</a></strong> course and starts at <strong>{{class.datetime}}</strong> based on <strong>{{class.timezone}}</strong>.</p>

<p>Use this <a href='{{class.link}}' target='_blank'>link</a> to Join the Class directly or <a href='{{student.link}}' target='_blank'>visit</a> your dashboard to see a list of your upcoming classes.</p>", 'wplms-braincert' ),  // HTML email content.
						'post_excerpt'  => __( "Hi {{recipient.name}},
						
We would like to remind you that {{class.title}} class is going to start soon. The class is related to the {{course.title}} course and starts at {{class.datetime}} based on {{class.timezone}}.

Click on this {{class.link}} to Join the Class directly or visit your dashboard ({{student.link}}) to see a list of your upcoming classes.

Kind Regards,
{{site.name}}", 'wplms-braincert' ),  // Plain text email content.
						'situation'		=> 'class_reminder',
						'description'	=> 'A class reminder for students'
					),
				);
				
		foreach( $mails as $mail ){
			$this->insert_braincert_mails( $mail );
		}
	}
	
	private function insert_braincert_mails( $mail = array() ) {
		
		if( empty( $mail ) )
			return ;
		
		extract( $mail );
		// Do not create if it already exists and is not in the trash
		$post_exists = post_exists( $post_title );
	 
		if ( $post_exists != 0 && get_post_status( $post_exists ) == 'publish' )
		   return;
	  
		// Create post object
		$email = array(
		  'post_title'		=> $post_title,
		  'post_content'	=> $post_content,  // HTML email content.
		  'post_excerpt'	=> $post_excerpt,  // Plain text email content.
		  'post_status'		=> 'publish',
		  'post_type'		=> bp_get_email_post_type() // this is the post type for emails
		);
	 
		// Insert the email post into the database
		$post_id = wp_insert_post( $email );
	 
		if ( $post_id ) {
			// add our email to the taxonomy term 'post_received_comment'
			// Email is a custom post type, therefore use wp_set_object_terms
	 
			$tt_ids = wp_set_object_terms( $post_id, $situation, bp_get_email_tax_type() );
			foreach ( $tt_ids as $tt_id ) {
				$term = get_term_by( 'term_taxonomy_id', (int) $tt_id, bp_get_email_tax_type() );
				wp_update_term( (int) $term->term_id, bp_get_email_tax_type(), array(
					'description' => $description,
				) );
			}
		}
	}
	
	private function delete_braincert_classes_database(){		
		if( get_option('braincert_delete_db') ){		
			global $wpdb;
			$wpdb->query(  "DROP TABLE IF EXISTS ".$wpdb->prefix . "wplms_braincert_classes ;" );
		}		
	}
	
	private function delete_braincert_settings(){	
		if( get_option('braincert_delete_settings') ){		
			unregister_setting( 'braincert_api', 'braincert_api_key' );
			unregister_setting( 'braincert_api', 'braincert_api_baseurl' );
			unregister_setting( 'braincert_api', 'braincert_api_frontend' );
			unregister_setting( 'braincert_api', 'braincert_class_per_page' );
			unregister_setting( 'braincert_api', 'braincert_default_location' );
			unregister_setting( 'braincert_api', 'braincert_default_timezone' );
			unregister_setting( 'braincert_api', 'braincert_reminder_enabled' );
			unregister_setting( 'braincert_api', 'braincert_reminder_interval' );
			unregister_setting( 'braincert_api', 'braincert_delete_db' );		
			unregister_setting( 'braincert_api', 'braincert_delete_settings' );
			delete_option( 'braincert_api_key' );
			delete_option( 'braincert_api_baseurl' );
			delete_option( 'braincert_api_frontend' );
			delete_option( 'braincert_class_per_page' );
			delete_option( 'braincert_default_location' );
			delete_option( 'braincert_default_timezone' );
			delete_option( 'braincert_reminder_enabled' );
			delete_option( 'braincert_reminder_interval' );
			delete_option( 'braincert_delete_db' );		
			delete_option( 'braincert_delete_settings' );
		}
	}

}
$wbcdb = new WPLMS_BrainCert_DB();