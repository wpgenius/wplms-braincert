<?php
/**
 *
 * @class       WPLMS_Dashboard_Classes
 * @author      Team WPGenius (Makarand Mane)
 * @category    Admin
 * @package     WPLMS-BrainCert/includes/widgets
 * @version     1.0
 */
 
class WPLMS_Dashboard_Classes extends WP_Widget {

	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
		$widget_ops = array( 
			'classname' => 'wplms_dashboard_classes',
			'description' => __('This widget can be used to show Braincert classes on WPLMS dashboard.', 'wplms-braincert'),
		);
		parent::__construct( 'wplms_dashboard_classes', __('DASHBOARD : BrainCert Classes', 'wplms-braincert'), $widget_ops );
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		// outputs the content of the widget
		
		global $wpdb;
		extract( $args );
		
		$status			= isset( $_REQUEST['status'] )		? $_REQUEST['status']		: 'all';
		$course_id		= isset( $_REQUEST['course_id'] )	? $_REQUEST['course_id']	: 'all';
		$instructor_id	= isset( $_REQUEST['instructor'] )	? $_REQUEST['instructor']	: 'all';
		$access_filter	= isset( $_REQUEST['access'] )		? $_REQUEST['access']		: 'all';
		
		//Our variables from the widget settings.
		$title 		= apply_filters('widget_title', $instance['title'] );
	    $width 		= $instance['width'];	

		$wbc_api	= WPLMS_BrainCert_Actions::$instance;
		$user_id	= get_current_user_id();

	    if(current_user_can( 'instructor'))
			$args['instructor'] = $user_id;

		if(current_user_can( 'student'))
			$args['student_id'] = $user_id;
			
		$courses 			= $wbc_api->get_courses( $args );
		$course_counts		= $wbc_api->get_courses_counts( $args );
		
		$args['status']		= $status;
		$args['course_id'] 	= $course_id;		
		$args['whocansee'] 	= $access_filter;
		if( !isset( $args['instructor'] ) && $wbc_api->_is_admin() )
			$args['instructor'] = $instructor_id ;
		
		$args				= array( 'where' => $wbc_api->_where( $args ));

		$status_filters 	= $wbc_api->get_statuses();
		$status_counts		= $wbc_api->get_statuses_counts( $args );
		$timezones			= $wbc_api->get_timezones();
				
		echo '<div class="'.$width.'"><div class="dash-widget leaderboard-chart">'.$before_widget;
		if ( $title )
			echo $before_title.$title.$after_title;
		
		//processs your code
			?>
			<div class="class-filters row">
            	<form id="wbc_class_filters">
                    <div class="col-md-12 filter_by_status">
                        <select name="status" onchange="filter_courses();">
                            <?php
                            foreach ($status_filters as $value => $label) {
                                echo '<option value="'.$value.'" '.selected( $value, $status,false ).'" >'.$label.' ('.( isset($status_counts[$value] ) ? $status_counts[$value] : "0").')</option>';
                            }
                            ?>
                        </select>
                    </div>
    
                    <div class="col-md-12 filter_by_course">
                    <?php 
                    if ($courses->have_posts()) {
                        echo '<select name="course_id" id="course" onchange="filter_courses();" class="wbc_select2">';
                            echo "<option value='all' ".selected( $course_id , 'all', false).">".__( "All Courses", 'wplms-braincert')."  (".$course_counts['all'].")</option>";
                                while ( $courses->have_posts() ) {
                                    $courses->the_post();
									if( $wbc_api->_is_admin() || ( isset($course_counts[get_the_ID()] ) && $course_counts[get_the_ID()] ) )
	                                   	echo "<option value='".get_the_ID()."' '".selected( $course_id , get_the_ID(), false)."'>".get_the_title(get_the_ID())." (".( isset($course_counts[get_the_ID()] ) ? $course_counts[get_the_ID()] : '0').")</option>";
                                }
                            echo "</select>";
                    }
                    wp_reset_postdata();
                    ?>
                    </div>
    
                    <?php if(current_user_can('administrator')){ 
						$instructor_filters	= $wbc_api->get_instructors(); 
                    	$instructor_counts	= $wbc_api->get_instructor_counts( $args );
                    ?>
                    <div class="col-md-12 filter_by_instructor">
                        <select name="instructor" onchange="filter_courses();" class="wbc_select2">
                            <?php 
                            echo '<option value="all" '.selected( $instructor_id , 'all', false).'>'.__( "All", 'wplms-braincert').' ('.$instructor_counts['all'].')</option>'; 
                            foreach( $instructor_filters as $instructor ){
                                echo '<option value="'.$instructor->ID.'" '.selected( $instructor->ID , $instructor_id, false ).'" >'.$instructor->display_name.' ('.( isset($instructor_counts[$instructor->ID] ) ? $instructor_counts[$instructor->ID] : "0").')</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <?php } else if(current_user_can('instructor')){ ?>
                    <input type="hidden" name="instructor" value="<?php  echo $user_id; ?>"  />
                    <?php } ?>
    
                    <?php if(current_user_can('administrator') || current_user_can('instructor')){ 
						$access_filters = $wbc_api->get_whocansee();  
                    	$access_counts	= $wbc_api->get_whocansee_counts( $args );
                    ?>
                    <div class="col-md-12 filter_by_access">
                        <select name="access" onchange="filter_courses();">
                            <?php 
                                echo '<option value="all" '.selected( $access_filter , 'all', false).'>'.__( "All", 'wplms-braincert').' ('.$access_counts['all'].')</option>';
                                foreach( $access_filters as $value => $label ){
                                    echo '<option value="'.$value.'" '.selected( $value,$access_filter,false ).'" >'.$label.' ('.( isset($access_counts[$value] ) ? $access_counts[$value] : "0").')</option>';
                                }
                            ?>
                        </select>
                    </div>
                    <?php } ?>
                                     
                    <?php if(current_user_can('student')){ ?>
                    <input type="hidden" name="student_id" value="<?php  echo $user_id; ?>"  />
                    <?php } ?>                    
                    <input type="hidden" name="per_page" value="<?php  echo get_option('braincert_class_per_page', 5); ?>" />
                    <input type="hidden" name="page" value="1" id="page_count" />
                    <input type="hidden" name="view" value="dashboard" id="class_view" />
                    <input type="hidden" name="timezone" value="<?php echo get_option( 'timezone_string' ); ?>" id="class_timezone" />                    
                </form>
				
                <div class="dash-class-widget"></div>
			</div>
			<div class="wbc-loading text-center">
			    <img id="loading-image" src="<?php echo WBC_DIR_URL.'assets/images/loading.gif' ?>"/>
			</div>
			<?php
			echo '</div>';
		echo $after_widget.'</div></div>';
			
	}

	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
		// outputs the options form on admin		
		$defaults = array( 
					'title'  => __('Braincert Classes','wplms-braincert'),
                    'width' => 'col-md-6 col-sm-12',
				);
		$instance = wp_parse_args( (array) $instance, $defaults );
		$title  = esc_attr($instance['title']);
        $width = esc_attr($instance['width']);
		 ?>
        <p>
          <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:','wplms-braincert'); ?></label> 
          <input class="regular_text" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
        </p>
        <p>
          <label for="<?php echo $this->get_field_id('width'); ?>"><?php _e('Select Width','wplms-braincert'); ?></label> 
          <select id="<?php echo $this->get_field_id('width'); ?>" name="<?php echo $this->get_field_name('width'); ?>">
          	<option value="col-md-3 col-sm-6" <?php selected('col-md-3 col-sm-6',$width); ?>><?php _e('One Fourth','wplms-braincert'); ?></option>
          	<option value="col-md-4 col-sm-6" <?php selected('col-md-4 col-sm-6',$width); ?>><?php _e('One Third','wplms-braincert'); ?></option>
          	<option value="col-md-6 col-sm-12" <?php selected('col-md-6 col-sm-12',$width); ?>><?php _e('One Half','wplms-braincert'); ?></option>
            <option value="col-md-8 col-sm-12" <?php selected('col-md-8 col-sm-12',$width); ?>><?php _e('Two Third','wplms-braincert'); ?></option>
             <option value="col-md-8 col-sm-12" <?php selected('col-md-9 col-sm-12',$width); ?>><?php _e('Three Fourth','wplms-braincert'); ?></option>
          	<option value="col-md-12" <?php selected('col-md-12',$width); ?>><?php _e('Full','wplms-braincert'); ?></option>
          </select>
        </p>
        <?php 
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 *
	 * @return array
	 */
	public function update( $new_instance, $old_instance ) {
		// processes widget options to be saved		
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['width'] = $new_instance['width'];
		return $instance;		
	}
}
add_action( 'widgets_init', function(){
	register_widget( 'WPLMS_Dashboard_Classes' );
});