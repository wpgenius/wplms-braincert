<?php
/**
 *
 * @class       WPLMS_Dashboard_Classes_Calender
 * @author      Team WPGenius (Makarand Mane)
 * @category    Admin
 * @package     WPLMS-BrainCert/includes
 * @version     1.0
 */

class WPLMS_Dashboard_Classes_Calender extends WP_Widget {

	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
		$widget_ops = array( 
			'classname' => 'wplms_dashboard_classes_calender',
			'description' => __('This widget can be used to show Braincert classes Calender on WPLMS dashboard.', 'wplms-braincert'),
		);
		parent::__construct( 'wplms_dashboard_classes_calender', __('DASHBOARD : BrainCert Calender', 'wplms-braincert'), $widget_ops );
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {

		// outputs the content of the widget
		extract( $args );
		
		//Our variables from the widget settings.
		$title = apply_filters('widget_title', $instance['title'] );
	    $width =  $instance['width'];		
				
		echo '<div class="'.$width.'"><div class="dash-widget leaderboard-chart">'.$before_widget;
		if ( $title )
			echo $before_title.$title.$after_title;
		?>        
        <div class="cal-loading wbc-loading text-center">
            <img id="loading-image" src="<?php echo WBC_DIR_URL.'assets/images/loading.gif' ?>"/>
        </div>

		<div id='calendar'></div>
		<?php
		
		echo $after_widget.'</div></div>';
			
	}

	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
		// outputs the options form on admin		
		$defaults = array( 
					'title'  => __('BrainCert Calender','wplms-braincert'),
                    'width' => 'col-md-6 col-sm-12',
				);
		$instance = wp_parse_args( (array) $instance, $defaults );
		$title  = esc_attr($instance['title']);
        $width = esc_attr($instance['width']);
		 ?>
        <p>
          <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:','wplms-braincert'); ?></label> 
          <input class="regular_text" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
        </p>
        <p>
          <label for="<?php echo $this->get_field_id('width'); ?>"><?php _e('Select Width','wplms-braincert'); ?></label> 
          <select id="<?php echo $this->get_field_id('width'); ?>" name="<?php echo $this->get_field_name('width'); ?>">
          	<option value="col-md-3 col-sm-6" <?php selected('col-md-3 col-sm-6',$width); ?>><?php _e('One Fourth','wplms-braincert'); ?></option>
          	<option value="col-md-4 col-sm-6" <?php selected('col-md-4 col-sm-6',$width); ?>><?php _e('One Third','wplms-braincert'); ?></option>
          	<option value="col-md-6 col-sm-12" <?php selected('col-md-6 col-sm-12',$width); ?>><?php _e('One Half','wplms-braincert'); ?></option>
            <option value="col-md-8 col-sm-12" <?php selected('col-md-8 col-sm-12',$width); ?>><?php _e('Two Third','wplms-braincert'); ?></option>
             <option value="col-md-8 col-sm-12" <?php selected('col-md-9 col-sm-12',$width); ?>><?php _e('Three Fourth','wplms-braincert'); ?></option>
          	<option value="col-md-12" <?php selected('col-md-12',$width); ?>><?php _e('Full','wplms-braincert'); ?></option>
          </select>
        </p>
        <?php 
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 *
	 * @return array
	 */
	public function update( $new_instance, $old_instance ) {
		// processes widget options to be saved		
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['width'] = $new_instance['width'];
		return $instance;		
	}
}
add_action( 'widgets_init', function(){
	register_widget( 'WPLMS_Dashboard_Classes_Calender' );
});