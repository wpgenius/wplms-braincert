<?php
/**
 *
 * @class       WPLMS_BrainCert_class_ShortCode
 * @author      Team WPGenius (Makarand Mane)
 * @category    Admin
 * @package     WPLMS-BrainCert/includes/shortcodes
 * @version     1.0
 */

function wplms_braincert_class( $atts ) {
	extract( shortcode_atts( array(
		'id'	=> '',
	), $atts ) );
	
	$html = '<div class="class-data-'.$id.'">
				<script type="text/javascript">
				load_class( '.$id.' );
				</script>
			</div>

			<div class="swbc-loading wbc-loading-'.$id.' text-center">
			    <img id="loading-image" src="'.WBC_DIR_URL.'assets/images/loading.gif"/>
			</div>';

	return $html;

}
add_shortcode( 'braincert_class', 'wplms_braincert_class' );

// Filter Functions with Hooks
function braincert_class_editor_buttons() {
	if ( ! current_user_can( 'edit_posts' ) && ! current_user_can( 'edit_pages' ) ) {
		return;
	}

	if ( get_user_option( 'rich_editing' ) !== 'true' ) {
		return;
	}

	add_filter( 'mce_external_plugins', 'braincert_class_editor_add_buttons' );
	add_filter( 'mce_buttons', 'braincert_class_register_buttons' );
	add_action ( 'after_wp_tiny_mce', 'braincert_class_tinymce_extra_vars' );
}
add_action('admin_head', 'braincert_class_editor_buttons');
add_action('wp_head', 'braincert_class_editor_buttons');

function braincert_class_editor_add_buttons( $plugin_array ) {
	$plugin_array['wbc_button'] = WBC_DIR_URL.'assets/js/braincert_class_tinymce_buttons.js';
	return $plugin_array;
}

function braincert_class_register_buttons( $buttons ) {
	array_push( $buttons, 'wbc_button' );
	return $buttons;
}

function braincert_class_tinymce_extra_vars() { ?>
	<script type="text/javascript">
		var wbc_tinyMCE = <?php echo json_encode(
			array(
				'button_name' 	=> esc_html__(' Braincert', 'wplms-braincert'),
				'button_title' 	=> esc_html__(' Braincert class shortcode', 'wplms-braincert'),
				'popup_title' 	=> esc_html__('Insert Braincert class shortocde', 'wplms-braincert'),
				'class_id_label'=> esc_html__('Enter Braincert class id', 'wplms-braincert'),
				'loading_image'	=> '<div class="wbc-loading text-center">
										<img id="loading-image" src="'.WBC_DIR_URL.'assets/images/loading.gif"/>
									</div>',
				'ajax_url'		=>	admin_url('admin-ajax.php'),
				'nonce'			=>	wp_create_nonce( 'tynymce_security_nonce' ),
			)
		);
    ?>;
    </script>
	<style type="text/css">
	.mce-class-search{}
	.wbc-loading{ position:absolute; left:0; right:0; top:80px; text-align:center;}
	.wbc-loading img#loading-image {width: 50px;height: auto;}
	.mce-class-search-wrap {
		top: 55px;
		left: 15px;
		right: 15px;
		height: 240px;
	}
	#class_name, .class-select {width: 98%;display: block;}
	.class-select{ background:#f1f1f1; padding:5px 10px; margin:3px 0;}
	.mce-class-select { padding: 10px 0; max-height: 275px; overflow-y: auto; overflow-x:hidden; }
	.class-select:hover {background: #cdcdcd;cursor: pointer;	}
	#class_name_label {	display: block;	}
	#wbc_class_id.required {border-color: red;border-width: 2px;}
	p.mce-notice{ margin-bottom:10px; white-space: initial; font-style: italic; font-size: 12px;}
	.mce-course-filter {  margin-top: 10px;}
	</style>
	<?php
}

function wbc_class_search_form(){
	
	$wbc_api	= WPLMS_BrainCert_Actions::$instance;
	$user_id	= get_current_user_id();

	if(current_user_can( 'instructor'))
		$args['instructor'] = $user_id;
		
	$courses 			= $wbc_api->get_courses( $args );
	$course_counts		= $wbc_api->get_courses_counts( $args );
	?>
    <div class="mce-container mce-abs-layout-item  mce-formitem mce-class-search-wrap">
    	<p class="mce-notice"><?php _e( 'Shortcode needs only class id as parameter. You can enter class id directly in above input OR you can search & filter class, then select class to enter class ID. Once you enter class ID, press OK.' );?></p>
        <div id="mce-class-search" class="mce-class-search mce-container-body mce-abs-layout">
        	<label for="class_name" id="class_name_label">Search class: <input class="class_name" id="class_name" type="text" placeholder="Enter class title or class id" /></label>
        </div>
        <div id="mce-course-filter" class="mce-course-filter mce-container-body mce-abs-layout">
        	<label for="class_name" id="class_name_label">Filter by course:  
			<?php 
			if ($courses->have_posts()) {
				echo '<select name="course_id" id="wbc_course_id" class="wbc_select2">';
					echo "<option value='all' ".selected( $course_id , 'all', false).">".__( "All Courses", 'wplms-braincert')."  (".$course_counts['all'].")</option>";
						while ( $courses->have_posts() ) {
							$courses->the_post();
							if( $wbc_api->_is_admin() || ( isset($course_counts[get_the_ID()] ) && $course_counts[get_the_ID()] ) )
								echo "<option value='".get_the_ID()."' '".selected( $course_id , get_the_ID(), false)."'>".get_the_title(get_the_ID())." (".( isset($course_counts[get_the_ID()] ) ? $course_counts[get_the_ID()] : '0').")</option>";
						}
					echo "</select>";
			}
			wp_reset_postdata();
			?>
            </label>
        </div>
        <div id="mce-class-select" class="mce-class-select">
        
        	<?php wbc_tyneMCE_class_list( array( 'status' => 'live_upcoming',  'per_page' => 20 ) ); ?>
			
        </div>
    </div>
    <?php
	die();
}
			
function wbc_tyneMCE_class_list( $args = array() ){
	
	$classes = WPLMS_BrainCert_Actions::$instance->get_classes( $args );
	
	if( count( $classes ) > 0 ){
		foreach ( $classes as $class ) {
			?>
        	<p class="class-select" data-class-id="<?php echo $class['class_id']; ?>"><?php echo $class['title'].' <em>('. get_the_title($class['course_id'] ).')</em>'; ?></p>	
			<?php
		}
	} else {
		echo '<p class="no-class">'.__( "No results for this criteria", 'wplms-braincert' ).'</p>';
	}
}