// JavaScript Document
(function() {
    tinymce.PluginManager.add('wbc_button', function(editor, url) {
        editor.addButton('wbc_button', {
            icon: ' dashicons-rest-api dashicons-before',
            text: wbc_tinyMCE.button_name,
            title: wbc_tinyMCE.button_title,
            onclick: function() {
                editor.windowManager.open({
                    title: wbc_tinyMCE.popup_title,
					width: jQuery( window ).width() * 0.6,
					// minus head and foot of dialog box
					height: (jQuery( window ).height() - 36 - 50) * 0.62,
					inline: 1,
					id: 'wbc-search-class',
                    body: [{
                        type: 'textbox',
                        id: 'wbc_class_id',
                        name: 'class_id',
                        label: wbc_tinyMCE.class_id_label,
                        value: '',
                        classes: 'wbc_class_id',
                    }, ],
                    onsubmit: function(e) {
						if( e.data.class_id )
							editor.insertContent( '[braincert_class id="' +e.data.class_id +'"]' );
						else{
							jQuery( '#wbc_class_id' ).addClass( 'required' );
							e.preventDefault();
						}
                    }
                });
				
				appendWBC_search();
            }
        });
    });
	
	
	function appendWBC_search(){
		jQuery( '#wbc-search-class-body' ).append( wbc_tinyMCE.loading_image );
		
		jQuery( '#wbc-search-class-body' ).on( 'click', '.class-select', function(){
			jQuery( '#wbc_class_id' ).val( jQuery( this ).data( 'class-id' ) );
		});
		
		//https://stackoverflow.com/questions/10318575/jquery-search-as-you-type-with-ajax
		var timeout_wbc_ID = null;

		function findMember(str) {
		  console.log('search: ' + str);
		}
	
		jQuery( '#wbc-search-class-body' ).on( 'keyup', '#class_name', function(e) {
			clearTimeout(timeout_wbc_ID);
			timeout_wbc_ID = setTimeout(() => findCLass( e.target.value ), 500);
		});
		
		jQuery( '#wbc-search-class-body' ).on( 'change', '#wbc_course_id', function(e) {
			jQuery( '#class_name' ).trigger( 'keyup' );
		});
		
		//#wbc-search-class-absend
		jQuery.ajax({
			type: "POST",
			url: wbc_tinyMCE.ajax_url,
			data: { 
				action: 'class_search_form',
				security:  wbc_tinyMCE.nonce,
			},
			complete: function(){
				jQuery('.wbc_select2').select2({});
			},
			success: function (result) {			   
				jQuery( '#wbc-search-class-body .wbc-loading' ).remove( );
				jQuery( '#wbc-search-class-body' ).append( result );
				jQuery( '#mce-class-select' ).height( jQuery('#wbc-search-class').height() -36 -50-200 );
			}
		});	
	}
	
	function findCLass( searchValue ){
		jQuery.ajax({
			type: "POST",
			url: wbc_tinyMCE.ajax_url,
			data: { 
				action: 'tynymce_search_results',
				security:  wbc_tinyMCE.nonce,
				search	:  searchValue,
				course_id:  jQuery( '#wbc_course_id' ).val(),
			},
			beforeSend: function() {
				jQuery( '#mce-class-select' ).html( wbc_tinyMCE.loading_image );
			},
			complete: function(){
			},
			success: function (result) {			   
				jQuery( '#mce-class-select' ).html( result );
			}
		});	
	}
})();