jQuery(document).ready(function () {
	// Delete class
	jQuery( document ).on( 'click', '.delete-link a', function( e ){
		var $this = jQuery(this);
		if($this.hasClass('deleteclass')){
			if ( confirm( wbc_manage.delete_title ) == false) {
				return false;
			}else{
				jQuery.ajax({
				   type: "POST",
				   url: wbc_manage.ajax_url+'?action=delete_class',
				   data: { 
							security: wbc_manage.nonce,
							class_id:$this.parent().attr('data-id'),
							course_id:$this.parent().attr('data-courseid'),
						 },
					beforeSend: function() {
		              	jQuery(".wbc-loading").show();
		           	},
		           	complete: function(){
					    jQuery(".wbc-loading").hide();
					},
				   success: function (result) {
				   		if( result.success == true ){
							jQuery.alert({ title: wbc_manage.alert, boxWidth: '60%', useBootstrap: false, content: wbc_manage.delete_class });
							window.location.reload();
						}else if( result.success == false ){
							jQuery.alert({ title: wbc_manage.alert, boxWidth: '60%', useBootstrap: false, content: result.data.msg });
							window.location.reload();
						}else {
							jQuery.alert({ title: wbc_manage.alert, boxWidth: '60%', useBootstrap: false, content: result.data.msg });
						}
				   },
				   error:function () {
				   		jQuery.alert({ title: wbc_manage.alert, boxWidth: '60%', useBootstrap: false, content: wbc_manage.json_error });
				   }
				});	
			}
			
		}
		
		e.preventDefault();
	});

	// Cancel and activate class
	jQuery( document ).on( 'click', '.cancel-link a', function( e ){
		var $this = jQuery(this);
		var class_title = $this.attr("class")+'_title';
		if ( confirm(wbc_manage[class_title]) == false) {
			return false;
		}else{
			jQuery.ajax({
			   type: "POST",
			   url: wbc_manage.ajax_url+'?action=cancel_class',
			   data: { 
						security: wbc_manage.nonce,
						class_id:$this.parent().attr('data-id'),
						isCancel:$this.parent().attr('data-value'),
						course_id:$this.parent().attr('data-courseid'),
					 },
				beforeSend: function() {
	              	jQuery(".wbc-loading").show();
	           	},
	           	complete: function(){
				    jQuery(".wbc-loading").hide();
				},
			   	success: function (result) {
					var class_id = result.data.class_id;
					if( result.success == true ){
						if( result.data.cancelCurrent ){
							jQuery( ".active-"+class_id ).replaceWith( result.data.cancelCurrent );
							jQuery.alert({ title: wbc_manage.alert, boxWidth: '60%', useBootstrap: false, content: wbc_manage.active_class });
						}else{
							jQuery( ".cancle-"+class_id ).replaceWith( result.data.activateClass );
							jQuery.alert({ title: wbc_manage.alert, boxWidth: '60%', useBootstrap: false, content: wbc_manage.cancel_class });
						}
					}else if( result.success == false ){
						jQuery.alert({ title: wbc_manage.alert, boxWidth: '60%', useBootstrap: false, content: result.data.msg });
					}else {
						jQuery.alert({ title: wbc_manage.alert, boxWidth: '60%', useBootstrap: false, content: result.data.msg });
					}
					
			   },
			   error:function () {
			   		jQuery.alert({ title: wbc_manage.alert, boxWidth: '60%', useBootstrap: false, content: wbc_manage.json_error });
			   }
			});	
		}
			
		e.preventDefault();
	});

	//Attendance report course_attendance
	jQuery( document ).on( 'click', '.course_attendance', function( e ){
		var $this = jQuery(this);
		jQuery.dialog({
			boxWidth: '60%',
    		useBootstrap: false,
    		smoothContent: true,
    		buttons: {
		        download: {
		            text: 'Download', 
		            action: function () {
		            	jQuery.alert('You clicked on "heyThere"');
		            }
		        },
		        ok: {
		            text: 'Ok', 
	            },
			},
			beforeSend: function() {
              	jQuery(".wbc-loading").show();
           	},
           	complete: function(){
			    jQuery(".wbc-loading").hide();
			},
		    content: function () {
		        var self = this;
		        self.setTitle( wbc_manage.attendance_title );

		        return jQuery.ajax({
		            url: wbc_manage.ajax_url+'?action=course_attendance_report',
		            method: 'POST',
		            data: { 
						security: wbc_manage.nonce,
						course_id:$this.attr('data-courseid'),
					},
		        }).done(function ( result ) {
		        	if( result.success == true ){ 
			        	self.setContent( result.data.html );
		        	}else if( result.success == false ){ 
			        	self.setContent( result.data.msg );
		        	}else{
		        		self.setContent( result.data.msg );
		        	}
		        	
		        }).fail(function(){
		            jQuery.alert({ title: wbc_manage.alert, boxWidth: '60%', useBootstrap: false, content: wbc_manage.json_error });
		        });

		    }
		});
		e.preventDefault();
	});


	//Attendance report
	jQuery( document ).on( 'click', '.attendance a', function( e ){
		var $this = jQuery(this);
		jQuery.dialog({
			boxWidth: '60%',
    		useBootstrap: false,
    		smoothContent: true,
    		buttons: {
		        download: {
		            text: 'Download', 
		            action: function () {
		            	jQuery.alert('You clicked on "heyThere"');
		            }
		        },
		        ok: {
		            text: 'Ok', 
	            },
			},
			beforeSend: function() {
              	jQuery(".wbc-loading").show();
           	},
           	complete: function(){
			    jQuery(".wbc-loading").hide();
			},
		    content: function () {
		        var self = this;
		        self.setTitle( wbc_manage.attendance_title );

		        return jQuery.ajax({
		            url: wbc_manage.ajax_url+'?action=attendance_report',
		            method: 'POST',
		            data: { 
						security: wbc_manage.nonce,
						class_id:$this.parent().attr('data-id'),
						course_id:$this.parent().attr('data-courseid'),
					},
		        }).done(function ( result ) {

		        	if( result.success == true ){
			        	self.setContent( result.data.html );
		        	}else if( result.success == false ){ 
			        	self.setContent( result.data.msg );
		        	}else{
		        		self.setContent( result.data.msg );
		        	}
		        	
		        }).fail(function(){
		            jQuery.alert({ title: wbc_manage.alert, boxWidth: '60%', useBootstrap: false, content: wbc_manage.json_error });
		        });
		    }
		});
		e.preventDefault();
	});

	//Learner preview and instructor preview
	jQuery( document ).on( 'click', '.user_preview a', function( e ){
		var $this = jQuery(this);
		jQuery.ajax({
			type: "POST",
			url: wbc_manage.ajax_url+'?action=user_previews',
			data: { 
					security	: wbc_manage.nonce,
					class_id	: $this.parent().attr('data-id'),
					course_id	: $this.parent().attr('data-courseid'),
					user_id		: $this.parent().attr('data-instructor'),
					isTeacher	: $this.parent().attr('data-isTeacher'),
				 },
			beforeSend: function() {
              	jQuery(".wbc-loading").show();
           	},
           	complete: function(){
			    jQuery(".wbc-loading").hide();
			},
			success:function(result) {
				if( result.success == true){
					window.open( result.data.launchurl );
				}else if( result.success == false ){
					jQuery.alert({ title: wbc_manage.alert, boxWidth: '60%', useBootstrap: false, content: result.data.msg });
				}else {
					jQuery.alert({ title: wbc_manage.alert, boxWidth: '60%', useBootstrap: false, content: result.data.msg });
				}
			},
			error:function() {
				jQuery.alert({ title: wbc_manage.alert, boxWidth: '60%', useBootstrap: false, content: wbc_manage.json_error });
			}
		});	
		
		e.preventDefault();	
	});
		
	//View recording
	jQuery( document ).on( 'click', '.class_recording a', function( e ){
		var $this = jQuery(this);
		jQuery.dialog({
			boxWidth: '60%',
    		useBootstrap: false,
    		smoothContent: true,
    		buttons: {
		        close: {
		            text: 'Close', 
	            },
			},
		    content: function () {
		        var self = this;
		        self.setTitle( wbc_manage.recording_title );
		        return jQuery.ajax({
		            url: wbc_manage.ajax_url+'?action=view_recording',
		            method: 'POST',
		            data: { 
						security: wbc_manage.nonce,
						class_id:$this.parent().attr('data-id'),
					},
		        }).done(function ( result ) {
		        	if( result.success == true ){
		        		if( result.data.msg ){
		        			self.setContent( result.data.msg );
		        		}else{
		        			recording_video();
		        			self.setContent( result.data.html );
		        		}
		        	}else if( result.success == false ){ 
			        	self.setContent( result.data.msg );
		        	}else{
		        		self.setContent( result.data.msg );
		        	}
		        	
		        }).fail(function(){
		            jQuery.alert({ title: wbc_manage.alert, boxWidth: '60%', useBootstrap: false, content: wbc_manage.json_error });
		        });
		    }
		});
		e.preventDefault();	
	});
		
	//Manage recording
	jQuery( document ).on( 'click', '.view_recording a', function( e ){
		var $this = jQuery(this);
		jQuery.dialog({
			boxWidth: '60%',
    		useBootstrap: false,
    		smoothContent: true,
    		buttons: {
		        close: {
		            text: 'Close', 
	            },
			},
		    content: function () {
		        var self = this;
		        self.setTitle( wbc_manage.manage_title );
		        return jQuery.ajax({
		            url: wbc_manage.ajax_url+'?action=manage_recording',
		            method: 'POST',
		            data: { 
						security: wbc_manage.nonce,
						class_id:$this.parent().attr('data-id'),
					},
		        }).done(function ( result ) {
		        	if( result.success == true ){
		        		if( result.data.Recording ){
		        			self.setContent( result.data.Recording );
		        		}else{
		        			recording_video();
		        			self.setContent( result.data.html );
		        		}
		        	}else{
		        		self.setContent(  result.data.msg  );
		        	}
		        	
		        }).fail(function(){
		            jQuery.alert({ title: wbc_manage.alert, boxWidth: '60%', useBootstrap: false, content: wbc_manage.json_error });
		        });
		    }
		});
		e.preventDefault();	
	});

	jQuery('.wbc_select2').select2({});

});



function recording_video(){
	jQuery('#videourl').on('change', function () {
		var videourl = jQuery('#videourl').val();
		var sources = [{"type": "video/mp4", "src": videourl}];
		player.pause();
		player.src(sources);
		player.load();
		player.play();
	});
}

jQuery(document).ready(function () {
	//Copy shortcode
	const links = document.querySelectorAll('.copy-click');
	const cls = {
		copied: 'is-copied',
		hover: 'is-hovered'
	}
			
	const copyToClipboard = str => {
		const el = document.createElement('input');
		str.dataset.copyString ? el.value = str.dataset.copyString : el.value = str.text;
		el.setAttribute('readonly', '');
		el.style.position = 'absolute';
		el.style.opacity = 0;
		document.body.appendChild(el);
		el.select();
		document.execCommand('copy');
		document.body.removeChild(el);
	}

	const clickInteraction = (e) => {
		e.preventDefault();
		copyToClipboard(e.target);
		e.target.classList.add(cls.copied);
		setTimeout(() => e.target.classList.remove(cls.copied), 1000);
		setTimeout(() => e.target.classList.remove(cls.hover), 700);  
	}

	Array.from(links).forEach(link => {
		link.addEventListener('click', e => clickInteraction(e));
		link.addEventListener('keypress', e => {
			if (e.keyCode === 13) clickInteraction(e);
		});

		link.addEventListener('mouseover', e => e.target.classList.add(cls.hover));
		link.addEventListener('mouseleave', e => {
			if (!e.target.classList.contains(cls.copied)) {
			 	e.target.classList.remove(cls.hover); 
			}
		});

	});
});

function recording_status(){
	//Recording status
	var $this = jQuery(".recording_status");
	jQuery.ajax({
		type: "POST",
		url: wbc_manage.ajax_url+'?action=recording_status',
		data: { 
				security: wbc_manage.nonce,
				recording_id:$this.attr('data-id'),
		},
		success:function(result) {
			if( result.success == true){
				jQuery.alert({ title: wbc_manage.alert, boxWidth: '60%', useBootstrap: false, content: wbc_manage.recoding_status });
				window.location.reload();
			}else{
				jQuery.alert({ title: wbc_manage.alert, boxWidth: '60%', useBootstrap: false, content: result.data.error });
			}
		},
		error:function() {
			jQuery.alert({ title: wbc_manage.alert, boxWidth: '60%', useBootstrap: false, content: wbc_manage.json_error });
		}
	});	

}

function remove_recording(){
	//Remove recording
	var $this = jQuery(".remove_recording");
	var recording_id = $this.attr('data-id');
	jQuery.ajax({
		type: "POST",
		url: wbc_manage.ajax_url+'?action=remove_recording',
		data: { 
				security: wbc_manage.nonce,
				recording_id:recording_id,
		},
		success:function(result) {
			if( result.success == true){
				jQuery.alert({ title: wbc_manage.alert, boxWidth: '60%', useBootstrap: false, content: result.data.msg });
				jQuery(".record-"+recording_id).remove();
			}else{
				jQuery.alert({ title: wbc_manage.alert, boxWidth: '60%', useBootstrap: false, content: result.data.msg });
			}
		},
		error:function() {
			jQuery.alert({ title: wbc_manage.alert, boxWidth: '60%', useBootstrap: false, content: wbc_manage.json_error });
		}
	});	

}