document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');

    var calendar = new FullCalendar.Calendar(calendarEl, {
    	plugins: [ 'dayGrid', 'timeGrid','list'],
        header: {
                left	: 'prev,next,today',
                center	: 'title',
                right	: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
            },

        defaultView: 'dayGridMonth',

        navLinks: true,

        eventLimit: true,

        /*eventRender: function(info) {
	      var tooltip = new Tooltip(info.el, {
	        title: info.event.extendedProps.description,
	        placement: 'top',
	        trigger: 'hover',
	        container: 'body'
	      });
	    },*/

        events: wbc_common.ajax_url+'?action=get_calender_events&security='+wbc_common.nonce+'&timezone='+Intl.DateTimeFormat().resolvedOptions().timeZone,
		
		eventMouseEnter: function (info) {
			var tis=info.el;
            var popup=info.event.extendedProps.popup;
            var tooltip = '<div class="tooltipevent" style="top:'+(jQuery(tis).offset().top-5)+'px;left:'+(jQuery(tis).offset().left+(jQuery(tis).width())/2)+'px"><div>' 
            				+ popup.classTitle + '</div><div>' + popup.courseTitle + '</div><div>'+ popup.duration + '</div><div>' 
            				+ popup.startTime + '</div><div>' + popup.teacher +'</div><div>' + popup.status + '</div><div>' +  popup.access + '</div></div>';
            var $tooltip = jQuery(tooltip).appendTo('body');
		    
		},

		eventMouseLeave: function(info) {
            jQuery(info.el).css('z-index', 8);
            jQuery('.tooltipevent').remove();
        },
		
		eventClick: function(info) {
		
			if (info.event.url) {
				window.open(info.event.url, "_blank");
				info.jsEvent.preventDefault(); // don't let the browser navigate
			}
		}

    });

    calendar.render();
	jQuery( '.cal-loading.wbc-loading' ).remove();
});