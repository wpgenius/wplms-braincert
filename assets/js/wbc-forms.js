function init_wbc_form(){	
	
	//select dropdowns
	jQuery('.wbc_select2').select2({});
	
	//old script from other plugin
    jQuery("#force_language").css('display','none');

    jQuery(".record_auto").css('display','none');

    jQuery("#currencycontainer").css('display','none');

    jQuery(".recurring_class").css('display','none');

    jQuery("#is_recurring_yes").click(function() {
        jQuery(".recurring_class").show("slow");
    });

    jQuery("#allow_change_language_1").click(function() {
        jQuery("#force_language").hide("slow");
    });
    
    jQuery("#allow_change_language_2").click(function() {
        jQuery("#force_language").show("slow");
    });

    jQuery(".record_1").click(function() {
        jQuery(".record_auto").show("slow");
    });

    jQuery(".record_2").click(function() {
        jQuery(".record_auto").hide("slow");
    });

    if(jQuery('.record_1').is(':checked')){
        jQuery(".record_auto").show("slow");
    }else{
        jQuery(".record_auto").hide("slow");
    }

    jQuery("#is_recurring_no").click(function() {
        jQuery(".weeklytotaldays").hide("slow");
        jQuery(".recurring_class").hide("slow");
    });

    if(jQuery('#is_recurring_yes').is(':checked')){
        jQuery(".recurring_class").show();
    }

    if(jQuery('#class_type_radio0').is(':checked')){
        jQuery("#currencycontainer").hide("slow");
    }

    jQuery( "#class_type_radio0").click(function() {
        jQuery("#currencycontainer").hide("slow");
    });

    jQuery( "#class_type_radio1").click(function() {
        jQuery("#currencycontainer").show("slow");
    });

    var repeat = jQuery( "#repeats" ).val();
    if(repeat=='6'){
        jQuery(".weeklytotaldays").show();  
    }else{
        jQuery(".weeklytotaldays").hide();
    }

    jQuery("#repeats").change(function() {
        var repeat = jQuery( "#repeats" ).val();
        if(repeat=='6'){
            jQuery(".weeklytotaldays").show();  
        }else{
            jQuery(".weeklytotaldays").hide();
        }
    });

    jQuery(function() {
        jQuery( "#datepicker" ).datepicker();
            jQuery( "#datepicker" ).datepicker( "option","minDate", 0, "dateFormat", "yy-mm-dd" );
    });
    jQuery(function() {
        jQuery( "#recurring_enddate" ).datepicker();
            jQuery( "#recurring_enddate" ).datepicker( "option","minDate", 0, "dateFormat", "yy-mm-dd" );
    });

    jQuery('#class_start_time').timepicker({ 'scrollDefault': 'now' });

    jQuery('#class_end_time').timepicker({ 'scrollDefault': 'now' });


    jQuery('#instructor_name').on("change", function(){
        var src = jQuery('#instructor_name').find(':selected').data('avatar');
        jQuery('#instructorthumb').attr("src", src);
    });

    jQuery('#course').on("change", function(){
        var src = jQuery('#course').find(':selected').data('avatar');
        jQuery('#coursethumb').attr("src", src);
    });
	
	jQuery( '#instructor_name' ).trigger( 'change' );

    jQuery("#showdata").hide();
    jQuery(".wbc_loading_img").addClass('hidden');
}

function setweekday(el) {
	if(! jQuery(el).parent('label').closest(".active").length ) {    
		jQuery(el).parent('label').addClass('active');
	}else{    
		jQuery(el).parent('label').removeClass('active');
	}   
}

function wbc_insert_class(){
    var form = jQuery("#wbc-form")
    form.on( 'submit',function(event) {
        jQuery.ajax({
            url: wbc_form.ajax_url,
            type: 'POST',
            data: form.serialize(),
            beforeSend: function() {
                jQuery("#wbc_save_button").html( wbc_form.saving_msg );
                jQuery("#wbc_save_button").attr("disabled", true);
                jQuery(".wbc_loading_img").removeClass( 'hidden' );
            },
            complete: function(){
                jQuery("#wbc_save_button").html( wbc_form.save_msg );
                jQuery("#wbc_save_button").attr("disabled", false);
                jQuery(".wbc_loading_img").addClass( 'hidden' );
            },
            success: function( result ){
                if(  result.success == true){
                    jQuery("#showdata").show();
                    //jQuery.alert({ title: wbc_form.alert, boxWidth: '60%', useBootstrap: false, content: wbc_form.insert_class });
                }else if(  result.success == false){
                    jQuery.alert({ title: wbc_form.alert, boxWidth: '60%', useBootstrap: false, content: result.data.msg });
                }else {
                    jQuery.alert({ title: wbc_form.alert, boxWidth: '60%', useBootstrap: false, content: result.data.msg });
                }
            },
            error:function(){
                jQuery.alert({ title: wbc_form.alert, boxWidth: '60%', useBootstrap: false, content: wbc_form.json_error });
            }
           
        });
        event.preventDefault();
  
    });
}

jQuery(document).ready(function (){ 
	init_wbc_form(); 
	wbc_insert_class();
});

function wbc_create_class_modal(input){
    var title= '';
    var url = '';
    
    var dataid = jQuery(input).attr('data-id');
    if( jQuery(input).attr('data-class-id') ){
        var dataclassid = jQuery(input).attr('data-class-id');
    }else{
        var dataclassid = '';
    }
    
    title   =   wbc_form.edit_class;
    url     =   wbc_form.modal_url+'&course_id='+dataid+'&class_id='+dataclassid+'&security='+wbc_form.nonce;

    jQuery.dialog({
        title: title,

        content: 'url:'+url,
        buttons: {
            cancel: function () {
                text: 'Close'
            },
        },

        onContentReady: function () {
           wbc_insert_class();
        },
        closeIcon: 'x',
        closeIconClass: 'fa fa-remove ',
        columnClass: 'col-md-12 wbc_create_class'
    });
  
}