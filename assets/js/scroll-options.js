var $loadMoreButton = jQuery('.loadmore');
$loadMoreButton.on( 'click', function() {
  
  $container.infiniteScroll('loadNextPage');
  
  $container.infiniteScroll( 'option', {
    loadOnScroll: true,
  });
 
  $loadMoreButton.hide();

});