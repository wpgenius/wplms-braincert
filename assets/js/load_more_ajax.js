jQuery(function($){ 
	$('.loadmore').click(function(){
 
		var button = $(this),
		    data = {
			'action': 'loadmore',

		};
 
		$.ajax({ 
			url : loadmore_parameters.ajaxurl,
			data : data,
			success : function( reponse,data ){
				if( data ) { 
					jQuery('#class_container').append(data);
					button.text( 'More posts' ).prev().before(data);
					console.log(data);
				}
			}
		});
	});
});