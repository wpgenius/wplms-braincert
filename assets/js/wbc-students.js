function recording_video(){
	jQuery('#videourl').on('change', function () {
		var videourl = jQuery('#videourl').val();
		var sources = [{"type": "video/mp4", "src": videourl}];
		player.pause();
		player.src(sources);
		player.load();
		player.play();
	});
}

jQuery(document).ready(function () {	
	//View recording
	jQuery( document ).on( 'click', '.class_recording a', function( e ){
		var $this = jQuery(this);
		jQuery.dialog({
			boxWidth: '60%',
    		useBootstrap: false,
    		smoothContent: true,
    		buttons: {
		        close: {
		            text: 'Close', 
	            },
			},
		    content: function () {
		        var self = this;
		        self.setTitle( wbc_student.recording_title );
		        return jQuery.ajax({
		            url: wbc_student.ajax_url+'?action=view_recording',
		            method: 'POST',
		            data: { 
						security: wbc_student.nonce,
						class_id:$this.parent().attr('data-id'),
					},
		        }).done(function ( result ) {
		        	if( result.success == true ){
		        		if( result.data.msg ){
		        			self.setContent( result.data.msg );
		        		}else{
		        			recording_video();
		        			self.setContent( result.data.html );
		        		}
		        	}else if( result.success == false ){ 
			        	self.setContent( result.data.msg );
		        	}else{
		        		self.setContent( result.data.msg );
		        	}
		        	
		        }).fail(function(){
		            jQuery.alert({ title: wbc_student.alert, boxWidth: '60%', useBootstrap: false, content: wbc_student.json_error });
		        });
		    }
		});
		e.preventDefault();	
	});

	//Learner preview and instructor preview
	jQuery( document ).on( 'click', '.user_preview a', function( e ){
		var $this = jQuery(this);
		jQuery.ajax({
			type: "POST",
			url: wbc_student.ajax_url+'?action=user_previews',
			data: { 
					security	: wbc_student.nonce,
					class_id	: $this.parent().attr('data-id'),
					course_id	: $this.parent().attr('data-courseid'),

				 },
			beforeSend: function() {
              	jQuery(".wbc-loading").show();
           	},
           	complete: function(){
			    jQuery(".wbc-loading").hide();
			},
			success:function( result ) {
				if( result.success == true){
					window.open( result.data.launchurl );
				}else if( result.success == false ){
					jQuery.alert({ title: wbc_manage.alert, boxWidth: '60%', useBootstrap: false, content: result.data.msg });
				}else {
					jQuery.alert({ title: wbc_manage.alert, boxWidth: '60%', useBootstrap: false, content: result.data.msg });
				}
			},
			error:function() {
				jQuery.alert({ title: wbc_student.alert, boxWidth: '60%', useBootstrap: false, content: wbc_student.json_error });
			}
		});	
		
		e.preventDefault();	
	});

});

function accordian_course(input) {
    togDiv = jQuery(input).data('id');
    jQuery(input).toggleClass('fa-minus');
    jQuery('.course-lesson-'+togDiv).toggleClass('show');
}