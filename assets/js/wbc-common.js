function accordian_course(input) {
    togDiv = jQuery(input).data('id');
    jQuery(input).toggleClass('fa-minus');
    jQuery('.course-lesson-'+togDiv).toggleClass('show');
}

function class_view_init(){
	jQuery("#page_count").val("1");
	jQuery(".dash-class-widget").html("");
	load_classes();
}

function load_classes(){
	var form = jQuery( "#wbc_class_filters" );
	jQuery.ajax({
		type: "POST",
		url: wbc_common.ajax_url+'?action=load_classes&security='+wbc_common.nonce,
		data: form.serialize(),
		dataType:"html",
		beforeSend: function() {
			jQuery("#btn_more").hide();
			jQuery(".wbc-loading").show();

		},
		complete: function(){
			jQuery("#btn_more").show();
			var page_count = jQuery("#page_count").val();
			page_count++;
			jQuery("#page_count").val( page_count );
			jQuery(".wbc-loading").hide();

		},
		success: function ( result ) {
			if( result != '' ){ 
				jQuery( "#load_more_row" ).remove();
				jQuery( ".dash-class-widget" ).append( result );
			}else{
				jQuery( "#btn_more" ).before( wbc_common.load_more );
				jQuery( "#btn_more" ).remove();

			}
		},
		error:function(){
            //jQuery.alert({ title: wbc_common.alert, boxWidth: '60%', useBootstrap: false, content: wbc_common.json_error });
        }
	});
		
}

jQuery( document ).ready( function () {
		
	var timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
	
	if( jQuery('#wbc_class_filters').length > 0 ){
		jQuery("#class_timezone").val(timeZone);
		
		jQuery( document ).on( 'click', '#btn_more', function( e ){
			e.preventDefault();
			load_classes();
		});
		
		jQuery( document ).on( 'submit', '#wbc_class_filters', function( e ){
			e.preventDefault();
			class_view_init();
		});
	
		class_view_init();
	}
	
	if( jQuery('#wbc_login').length > 0 ){
		jQuery( document ).on( 'click', '#wbc_login', function( e ){
			e.preventDefault();
			jQuery(".vbplogin").trigger("click");
		});
	}
});


//g_timer = null;
function refresh_filters(){
	jQuery( "#wbc_class_filters" ).trigger( 'submit' );
	/*
	clearTimeout(g_timer);
	g_timer = window.setTimeout( jQuery( "#wbc_class_filters" ).trigger( 'submit' ), 3000);
	console.log(g_timer);*/
}

function filter_courses(){	
	var params = {};	
	jQuery('#wbc_class_filters').find('select').each( function(){
		 params[jQuery(this).attr('name')] = jQuery(this).val();
			//query += "status="+jQuery(this).val();
	});	
	var queryString = Object.keys(params).map(key => key + '=' + params[key]).join('&');
	window.location.href = window.location.origin + window.location.pathname + '?'+queryString;
}


function load_class( class_id ){
	var timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
	jQuery.ajax({
		type: "POST",
		url: wbc_common.ajax_url+'?action=load_single_class',
		data: { 
				security: wbc_common.nonce,
				class_id: class_id,
				timezone: timeZone,
			 },
		dataType:"html",
		beforeSend: function() {
		},
		complete: function(){
			jQuery(".wbc-loading-"+class_id).remove();
		},
		success: function ( result ) {
			jQuery( ".class-data-"+class_id ).html( result );
		},
		error:function(){
            jQuery.alert({ title: wbc_common.alert, boxWidth: '60%', useBootstrap: false, content: wbc_common.json_error });
        }
	});
		
}