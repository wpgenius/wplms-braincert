<form class="form-horizontal form-validate" id="wbc-form" method="post" enctype="multipart/form-data">
    <?php if( $this->_is_admin() ){ ?>
	<div class="control-group">
        <label for="instructor_name" class="span1 hasTip"  title="<?php _e('Class Instructor:','wplms-braincert'); ?>"><?php _e('Class Instructor:','wplms-braincert'); ?></label>
            <div class="controls">
                <span style="display: inline-block;vertical-align: middle;"><img src="<?php echo esc_url( get_avatar_url( $instructor_id ) );?>" alt="<?php _e('instructor pic','wplms-braincert'); ?>" id="instructorthumb" style="width: 64px;height: 64px;" /></span>
                <select name="instructor_name" id="instructor_name" class="wbc_select2" required>
                    <?php 
                    foreach( $instructors as $instructor ){ 
                        if( !is_object( $instructor ))  $instructor = get_userdata( $instructor );                     
                        echo '<option value="'.$instructor->ID.'" '.selected( $instructor->ID , $instructor_id, false).' '."data-avatar='".esc_url(get_avatar_url($instructor->ID))."'".' '."data-id='".$instructor->ID."'".'>'.$instructor->display_name.'</option>';
                    }                    
                    ?>
                </select>
            </div>
    </div> 
    <?php } else if( $this->_is_instructor( $course_id, $instructor_id )) { ?>
    	<input type="hidden" id="instructor_name" name="instructor_name" value="<?php echo $instructor_id; ?>"/>
    <?php }?>

    <div class="control-group">
        <label for="location_id" class="span1 hasTip"  title="<?php _e('Location:','wplms-braincert'); ?>"><?php _e('Set Location:','wplms-braincert'); ?></label>
            <div class="controls">
	            <select name="location_id" id="location_id" class="wbc_select2" required>
                    <option value="" disabled <?php echo $location_id ? 'selected' : ''; ?>><?php _e('Auto select nearest datacenter region','wplms-braincert'); ?></option>
    	            <?php foreach ($locations as $value => $label) { 
    	            	echo '<option value="'.$value.'" '.selected( $location_id, $value, false).'>'.$label.'</option>';
    	            } ?>
				</select>
            </div>
    </div> 
    
	<?php if( $front ){ ?>
    <input type="hidden" id="course" name="course" value="<?php echo $course_id; ?>"/>
    <?php } else { ?>

    <div class="control-group">
        <label for="course" class="span1 hasTip" name="course_id" id="course_id"  title="<?php _e('Select Course:','wplms-braincert'); ?>"><?php _e('Select Course:','wplms-braincert'); ?></label>
            <div class="controls">
                <?php 
                $default_image = vibe_get_option('default_course_avatar');
                if ($courses->have_posts()) {
                    echo '<span class="course_img" style="display: inline-block;vertical-align: middle;">';
                    if( has_post_thumbnail( $course_id ) ){
                        echo '<img src="'.get_the_post_thumbnail_url( $course_id,"thumbnail" ).'" alt="'.__('course pic','wplms-braincert').'" id="coursethumb" style="width: 64px;height: 64px;" />';
                    }else{
                        echo '<img src="'.$default_image.'" alt="'.__('course pic','wplms-braincert').'" id="coursethumb" style="width: 64px;height: 64px;">';
                    }
                    echo '</span>';
                    echo '<select name="course" id="course" class="wbc_select2" required>';
                        echo "<option disabled value=>".__( "All Courses", 'wplms-braincert')."</option>";
                            while ( $courses->have_posts() ) {
                                $courses->the_post();
                                 if( has_post_thumbnail( $course_id ) ){
                                    echo '<option value="'.get_the_ID().'" "'.selected( $course_id , get_the_ID()).'" '."data-avatar='".esc_url( get_the_post_thumbnail_url( $course_id,"thumbnail" ) )."'".'>'.get_the_title(get_the_ID()).'</option>';
                                 }else{
                                    echo '<option value="'.get_the_ID().'" "'.selected( $course_id , get_the_ID()).'" '."data-avatar='". $default_image."'".'>'.get_the_title(get_the_ID()).'</option>';
                                 }
                            }
                        echo "</select>";
                }
                wp_reset_postdata();
                ?>
            </div>
    </div>
    
    <?php }?>

    <div class="control-group">
        <label for="title" class="span1 hasTip" title="<?php _e('Title:','wplms-braincert'); ?>"><?php _e('Title:','wplms-braincert'); ?></label>
            <div class="controls">
	          	<input type="text" placeholder="<?php _e('Title:','wplms-braincert'); ?>" id="title" name="title" value="<?php echo $title; ?>" required>
            </div>
    </div> 

    <div class="control-group"> 
        <label for="description" class="span1 hasTip" name="description" title="<?php _e('Class Description:','wplms-braincert'); ?>"><?php _e('Class Description:','wplms-braincert'); ?></label>
            <div class="controls">
            	<textarea rows="4" cols="50" name="description" id="description" placeholder="<?php _e('Description:','wplms-braincert'); ?>" ><?php echo $description ; ?></textarea>
            </div>
    </div>

    <div class="control-group">
        <label for="datepicker" class="span1 hasTip" title="<?php _e('Date','wplms-braincert'); ?>"><?php _e('Date:','wplms-braincert'); ?></label>
            <div class="controls">
	          	<input type="text" placeholder="<?php _e('Date','wplms-braincert'); ?>" id="datepicker" name="date" value="<?php echo $date ; ?>" required>
				<b><?php _e('(yyyy-mm-dd), Example: { 2014-09-04 }','wplms-braincert'); ?></b>
            </div>
    </div>

    <div class="control-group">
        <label for="class_start_time" class="span1 hasTip" title="<?php _e('Class start time','wplms-braincert'); ?>"><?php _e('From:','wplms-braincert'); ?></label>
            <div class="controls">
	       		<input type="text" data-format="hh:mm A" placeholder="<?php _e('From','wplms-braincert'); ?>" id="class_start_time" name="start_time" value="<?php echo $start_time ; ?>" required>
	            <b><?php _e('(hh:mmA), Example: { 09:50AM }','wplms-braincert'); ?></b>
        	</div>
    </div>
    <div class="control-group"> 
        <label for="class_end_time" class="span1 hasTip"  title="<?php _e('Class end time','wplms-braincert'); ?>"><?php _e('To:','wplms-braincert'); ?></label>
            <div class="controls">
	       		<input type="text" data-format="hh:mm A" placeholder="<?php _e('To','wplms-braincert'); ?>" id="class_end_time" name="end_time" value="<?php echo $end_time ; ?>" required>
	            <b><?php _e('(hh:mmA), Example: { 10:50AM }','wplms-braincert'); ?></b>
            </div>
    </div>

    <div class="control-group">
        <label for="timezone" class="span1 hasTip"  title="<?php _e('Time Zone:','wplms-braincert'); ?>"><?php _e('Time Zone:','wplms-braincert'); ?></label>
            <div class="controls">
                <select name="timezone" id="timezone" class="valid wbc_select2" required>
                	<?php foreach ($timezones as $value => $label) { ?>
                		<option title="<?php echo $label; ?>" value="<?php echo $value; ?>" <?php selected( $value, $timezone); ?>><?php echo $label; ?></option>
                	<?php } ?>
            	</select>
        	</div>
    </div>

    <div class="control-group recurring-class-group hidden hide">  
        <label class="span1 hasTip"  title="<?php _e('Recurring class','wplms-braincert'); ?>"><?php _e('Recurring Class:','wplms-braincert'); ?></label>
            <div class="controls">
				<label>
                    <input type="radio" id="is_recurring_yes" name="is_recurring" value="1" <?php checked( $is_recurring, 1); ?>><?php _e('Yes','wplms-braincert'); ?>   
                </label> 
                <label>
				    <input type="radio" id="is_recurring_no" name="is_recurring" value="0" <?php checked( $is_recurring, 0); ?>><?php _e('No','wplms-braincert'); ?> 
                </label>
            </div>
    </div>

    <div class="control-group recurring_class"> 
	    <label for="repeats" class="span1 hasTip"  title="<?php _e('class repeats','wplms-braincert'); ?>"><?php _e('When class repeats:','wplms-braincert'); ?></label>
            <div class="controls">
	            <select name="repeat" style="display: inline-block;" class="required wbc_select2 valid" id="repeats">
	            	<?php foreach ($class_repeates as $value => $label) { ?>
	            		<option value="<?php echo $value; ?>" <?php selected( $value, $repeat); ?>><?php echo $label; ?></option>
	            	<?php } ?>
	            </select>
            </div>
	</div>

	<div class="control-group weeklytotaldays">
        <label class="control-label"></label>
        <div class="weekdays_label">
            <?php $weekdays = explode(',', $weekdays); ?>
	        <label for="su" class="<?php echo in_array("1", $weekdays) ? 'active' :''; ?>" >
	            <input id="su" onclick="setweekday(this);" name="weekdays[]" type="checkbox" value="1" <?php checked( in_array(1, $weekdays), 1); ?> style="display:none;"><?php _e('Sun','wplms-braincert'); ?>
	        </label>

	        <label for="mo" class="<?php echo in_array("2", $weekdays) ? 'active' :''; ?>" >
	            <input id="mo"  onclick="setweekday(this);" name="weekdays[]" type="checkbox" value="2" <?php checked( in_array(2, $weekdays), 1); ?> style="display:none;"><?php _e('Mon','wplms-braincert'); ?>
	        </label>

	        <label for="tue" class="<?php echo in_array(3, $weekdays) ? 'active' :''; ?>" >
	            <input id="tue" onclick="setweekday(this);" name="weekdays[]"  type="checkbox" value="3" <?php checked( in_array(3, $weekdays), 1); ?> style="display:none;"><?php _e('Tue','wplms-braincert'); ?>
	        </label>

	        <label for="wed" class="<?php echo in_array(4, $weekdays) ? 'active' :''; ?>" >
	            <input id="wed" onclick="setweekday(this);" name="weekdays[]" type="checkbox" value="4" <?php checked( in_array(4, $weekdays), 1); ?> style="display:none;"><?php _e('Wed','wplms-braincert'); ?>
	        </label>

	        <label for="thu" class="<?php echo in_array(5, $weekdays) ? 'active' :''; ?>" >
	            <input id="thu"  onclick="setweekday(this);" name="weekdays[]" type="checkbox" value="5" <?php checked( in_array(5, $weekdays), 1); ?> style="display:none;"><?php _e('Thu','wplms-braincert'); ?>
	        </label>

	        <label for="fri" class="<?php echo in_array(6, $weekdays) ? 'active' :''; ?>" >
	            <input id="fri"  onclick="setweekday(this);" name="weekdays[]"  type="checkbox" value="6" <?php checked( in_array(6, $weekdays), 1); ?> style="display:none;"><?php _e('Fri','wplms-braincert'); ?>
	        </label>

	        <label for="sat" class="<?php echo in_array(7, $weekdays) ? 'active' :''; ?>" >
	            <input id="sat"  onclick="setweekday(this);" name="weekdays[]"  type="checkbox" value="7" <?php checked( in_array(7, $weekdays), 1); ?> style="display:none;"><?php _e('Sat','wplms-braincert'); ?>
	        </label>
        </div>
    </div> 


    <div class="control-group recurring_class">
        <label class="control-label" style="margin-left: 6px;"><?php _e('Ends:','wplms-braincert'); ?></label>
	        <div class="controls">
                <span style="padding-bottom: 8px; cursor: pointer;float:left" class="radio1 inline">
                    <label>
                        <input type="radio" class="validate-recurring required error" name="afterclasses" id="optionsRadios1" value="0" <?php checked( $afterclasses, 0); ?>>
                        <?php _e('After','wplms-braincert'); ?>&nbsp;
                    </label>
                </span>
		        <div class="input-append">
                    <input type="number" min="0" class="span3" value="1" <?php echo $end_classes_count; ?> name="end_classes_count" id="recurring_endclasses" style="min-height: 28px;">
                    <span class="add-on">Classes</span><?php _e(' (or)','wplms-braincert'); ?>
                </div>
		        <br><br>
                <label style="padding-bottom: 8px; cursor: pointer;" class="radio1 inline">
                    <input type="radio" class="validate-recurring required error" name="afterclasses" id="optionsRadios2" value="1" <?php checked( $afterclasses, 1); ?>>
                        <?php _e('Ends on','wplms-braincert'); ?>
			    </label>
		        <span>
                    <input type="text" class="span4" value="<?php echo $end_date; ?>" name="end_date" id="recurring_enddate" style="min-height: 28px;">
		        </span>
    	    </div>
    </div>

    <div class="control-group">
        <label class="span1 hasTip" title="<?php _e('Change interface language:','wplms-braincert'); ?>"><?php _e('Allow attendees to change interface language:','wplms-braincert'); ?></label>
            <div class="controls">
	            <label>
                    <input type="radio" id="allow_change_language_1" name="allow_change_interface_language" value="1" <?php checked( $allow_change_interface_language, 1); ?>><?php _e('Yes','wplms-braincert'); ?>
                </label>
                <label>
	               <input type="radio" id="allow_change_language_2" name="allow_change_interface_language" value="0"<?php checked( $allow_change_interface_language, 0); ?>><?php _e('No','wplms-braincert'); ?>
                </label>
            </div>
    </div>

    <div class="control-group" id="force_language" >
        <label for="language" class="span1 hasTip"  title="<?php _e('Force Interface Language:','wplms-braincert'); ?>"><?php _e('Force Interface Language:','wplms-braincert'); ?></label>
            <div class="controls">
                <select class="in-selection required form-control wbc_select2" id="language" name="language">
        			<?php  foreach($languages as $value=>$label){  ?>
                        <option value="<?php echo $value;?>" <?php selected( $value, $language); ?>><?php echo $label;?></option>
                    <?php } ?>
        	    </select>
                <br />
                <br />
            </div>
    </div>

    <div class="control-group"> 
        <label class="span1 hasTip"  title="<?php _e('Record class:','wplms-braincert'); ?>"><?php _e('Record this class:','wplms-braincert'); ?></label>
            <div class="controls">
				<label>
                    <input type="radio" class="record_1" name="record" value="1" <?php checked( $record, 1); ?>><?php _e('Yes','wplms-braincert'); ?>
                </label>
				<label>
                    <input type="radio" class="record_2" name="record" value="0" <?php checked( $record, 0); ?>><?php _e('No','wplms-braincert'); ?>
                </label>
            </div>
    </div>

    <div class="control-group record_auto">
        <label for="isRecordingLayout" class="span1 hasTip"  title="<?php _e('Recorded videos layout :','wplms-braincert'); ?>"><?php _e('Recorded videos layout :','wplms-braincert'); ?></label>
        <div class="controls">
            <select class="in-selection required form-control wbc_select2" id="isRecordingLayout" name="isRecordingLayout">
    			<option value="0" <?php selected( $isRecordingLayout, 0); ?>><?php _e('Standard view (Whiteboard, Videos and Chat view with no icons)','wplms-braincert'); ?></option>
                <option value="1" <?php selected( $isRecordingLayout, 1); ?>><?php _e(' Enhanced view (Entire browser tab with all the icons)','wplms-braincert'); ?></option>
    	    </select>
        </div>
    </div>

    <div class="control-group record_auto">
        <label class="span1 hasTip" title="<?php _e('Start recording automatically','wplms-braincert'); ?>"><?php _e('Start recording automatically when class starts:','wplms-braincert'); ?></label>
            <div class="controls">
	            <label>
                    <input type="radio" name="start_recording_auto" value="2" <?php checked( $start_recording_auto, 2); ?>><?php _e('Yes','wplms-braincert'); ?>&nbsp; &nbsp;    
	            </label>
                <label>
                    <input type="radio" name="start_recording_auto" value="1" <?php checked( $start_recording_auto, 1); ?>><?php _e('No','wplms-braincert'); ?>
                </label>
            </div>
    </div>

    <div class="control-group record_auto">
        <label class="span1 hasTip" title="<?php _e('Allow instructor to control recording','wplms-braincert'); ?>"><?php _e('Allow instructor to control recording:','wplms-braincert'); ?></label>
            <div class="controls">
                <label>
	               <input type="radio" name="isControlRecording" value="1" <?php checked( $isControlRecording, 1); ?>><?php _e('Yes','wplms-braincert'); ?>&nbsp; &nbsp;  
                </label>
                <label>  
	               <input type="radio" name="isControlRecording" value="0" <?php checked( $isControlRecording, 0); ?>><?php _e('No','wplms-braincert'); ?>
                </label>
            </div>
    </div>

    <div class="control-group record_auto">
        <label class="span1 hasTip" title="<?php _e('Video delivery:','wplms-braincert'); ?>"><?php _e('Video delivery:','wplms-braincert'); ?></label>
            <div class="controls">
	            <label>
                    <input type="radio" name="isVideo" value="0" <?php checked( $isVideo, 0); ?>><?php _e('Multiple video files','wplms-braincert'); ?>&nbsp; &nbsp;   
                </label>
                <label> 
	               <input type="radio" name="isVideo" value="1" <?php checked( $isVideo, 1); ?>><?php _e('Single video file','wplms-braincert'); ?>
                </label>
            </div>
    </div>

    <div class="control-group">
            <label class="span1 hasTip" title="<?php _e('Classroom type:','wplms-braincert'); ?>"><?php _e('Classroom type:','wplms-braincert'); ?></label>
            <div class="controls">
	        <label>
                <input type="radio" class="required" name="classroom_type" id="classroom_types" value="0" <?php checked( $classroom_type, 0); ?>><?php _e('whiteboard + audio/video + attendee list + chat','wplms-braincert'); ?>&nbsp; &nbsp;  
            </label>
            <label>
	            <input type="radio"  class="required" name="classroom_type" id="classroom_typeno" value="1" <?php checked( $classroom_type, 1); ?>><?php _e('whiteboard + attendee list','wplms-braincert'); ?>&nbsp; &nbsp;
            </label>
            <label>
	            <input type="radio"  class="required" name="classroom_type" id="classroom_type" value="2" <?php checked( $classroom_type, 2); ?>><?php _e('whiteboard + attendee list + chat','wplms-braincert'); ?>
            </label>
            </div>
    </div>

    <div class="control-group">
        <label class="span1 hasTip" title="<?php _e('Enable webcam and microphone upon entry','wplms-braincert'); ?>"><?php _e('Enable webcam and microphone upon entry:','wplms-braincert'); ?></label>
            <div class="controls">
                <label>
                    <input type="radio" name="isCorporate" value="1" <?php checked( $isCorporate, 1); ?>><?php _e('Yes','wplms-braincert'); ?>&nbsp; &nbsp;    
                </label>
                <label>
                    <input type="radio" name="isCorporate" value="0" <?php checked( $isCorporate, 0); ?>><?php _e('No','wplms-braincert'); ?>
                </label>
            </div>
    </div>

    <div class="control-group">
        <label class="span1 hasTip" title="<?php _e('Private chat','wplms-braincert'); ?>"><?php _e('Enable private chat:','wplms-braincert'); ?></label>
            <div class="controls">
                <label>
                    <input type="radio" name="isPrivateChat" value="0" <?php checked( $isPrivateChat, 0); ?>><?php _e('Yes','wplms-braincert'); ?>&nbsp; &nbsp; 
                </label>
                <label>   
                    <input type="radio" name="isPrivateChat" value="1" <?php checked( $isPrivateChat, 1); ?>><?php _e('No','wplms-braincert'); ?>
                </label>
            </div>
    </div>

    <div class="control-group">
        <label class="span1 hasTip" title="<?php _e('Screen sharing','wplms-braincert'); ?>"><?php _e('Enable screen sharing:','wplms-braincert'); ?></label>
            <div class="controls">
                <label>
                    <input type="radio" name="isScreenshare" value="1" <?php checked( $isScreenshare, 1); ?>><?php _e('Yes','wplms-braincert'); ?>&nbsp; &nbsp;    
                </label>
                <label>
                    <input type="radio" name="isScreenshare" value="0" <?php checked( $isScreenshare, 0); ?>><?php _e('No','wplms-braincert'); ?>
                </label>
            </div>
    </div>

    <div class="control-group record_auto">
        <label class="span1 hasTip" title="<?php _e('View Recording','wplms-braincert'); ?>"><?php _e('View Recording:','wplms-braincert'); ?></label>
            <div class="controls">
                <label>
                    <input type="radio" name="viewRecording" value="1" <?php checked( $viewRecording, 1); ?>><?php _e('Yes','wplms-braincert'); ?>&nbsp; &nbsp;    
                </label>
                <label>
                    <input type="radio" name="viewRecording" value="0" <?php checked( $viewRecording, 0); ?>><?php _e('No','wplms-braincert'); ?>
                </label>
            </div>
    </div>

    <div class="control-group">
        <label class="span1 hasTip" title="<?php _e('Access Mode','wplms-braincert'); ?>" ><?php _e('Access Mode:','wplms-braincert'); ?>  </label>
            <div class="controls">
            	<?php foreach ($access as $value => $label) { ?>
            		<label>
                        <input type="radio" name="whocansee" value="<?php echo $value; ?>" <?php checked( $value,$whocansee); ?> required><?php echo $label; ?>
                    </label> 
            	<?php } ?>
            </div>
    </div>

    <?php if( $this->_is_admin() ){ ?>
    <div class="control-group">  
        <label class="span1 hasTip"  title="<?php _e('Class Type','wplms-braincert'); ?>"><?php _e('Type:','wplms-braincert'); ?>  </label>
            <div class="controls">
            	<?php foreach ($class_type as $value => $label) { ?>
            		<label>
                        <input type="radio" name="ispaid" id="class_type_radio<?php echo $value;?>" value="<?php echo $value;?>" <?php checked( $value,$ispaid); ?> required><?php echo $label; ?>
                    </label>
            	<?php } ?>
            </div>

    </div>
    <?php } ?>

    <div class="control-group" style="margin: 0px;" id="currencycontainer">
        <label for="currency" class="span1 hasTip"  title="<?php _e('Currency','wplms-braincert'); ?>"><?php _e('Currency:','wplms-braincert'); ?></label>
            <div class="controls">
                <select style="width:100px;" id="currency" class="wbc_select2" name="currency">
                    <?php foreach($currency_arr as $value => $label) { ?>
                       <option value="<?php echo $value; ?>" <?php selected( $value, $currency); ?>><?php echo $label; ?></option>
                    <?php } ?>
                </select>
                <br />
                <br />
            </div>
        </div>

    <div class="control-group"> 
        <label for="seat_attendees" class="span1 hasTip"  title="<?php _e('Maximun attendees','wplms-braincert'); ?>"><?php _e('Max. attendees:','wplms-braincert'); ?></label>
            <div class="controls">
       			<input type="number" min="1" placeholder="<?php _e('Max. attendees:','wplms-braincert'); ?>" id="seat_attendees" name="seat_attendees" value="<?php echo  $seat_attendees; ?>" required>
            </div>
    </div>
    
    <?php wp_nonce_field( 'wbc_class_check', '_classNonce' ); ?>
    <input type="hidden"  id="cid" name="cid" value="<?php echo $class_id?>"/>
    <input type="hidden" id="task" name="task" value="schedule"/>
    <div class="control-group">
        <div class="controls">
            <button class="btn btn-primary"  name="save_form" id="wbc_save_button" type="submit" value="submit" style="font-weight: bold;"><?php _e('Save','wplms-braincert'); ?></button>
            <div class="wbc_loading_img hidden">
                <div class="loadingio-spinner-spin-1c87bm01t3z"><div class="ldio-e5qtbgqinyc">
                <div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div><div><div></div></div>
                </div></div>
            </div>
            <span id="showdata">
                 <span class="alert alert-success">
                    <strong><?php _e('Class Saved Successfully.','wplms-braincert'); ?></strong>
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                </span>
            </span>
        </div>
    </div>

   
</form>