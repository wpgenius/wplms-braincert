<div class="wbc-class single-wbc-class">
	<div class="col-md-12">
		<table class="table">
			<tr class="course_section">
				<td>
					<div class="course_details">
						<span class="pull-left col-1">
							<i class="fa fa-desktop"></i>
							<?php echo $class['title']; ?>
						</span>
						<span class="pull-right col-2" style="">
                        
							<?php if( $this->_can( 'edit', $class ) ) { ?>
                            
                                <a title="<?php _e('Edit Class','wplms-braincert'); ?>" onclick="wbc_create_class_modal(this)" data-id="<?php echo $class['course_id']; ?>" data-class-id="<?php echo $class['class_id']; ?>" href="#">
                                    <i class="fa fa-edit text-primary"></i>
                                </a>
                                
                                <span class="delete-link" data-id="<?php echo $class['class_id']; ?>" data-courseid="<?php echo $class['course_id']; ?>">
                                    <a title="<?php _e('Delete Class','wplms-braincert'); ?>" class="deleteclass" href="#"><i class="fa fa-trash-o text-danger"></i></a>
                                </span>   
                                
							<?php } ?>

							<?php if( $this->_can( 'view_attendance', $class ) ) { ?>                                                         
                                
                                <span class="attendance" data-id="<?php echo $class['class_id']; ?>" data-courseid="<?php echo $class['course_id']; ?>">
                                    <a href="#" title="<?php _e('Attendance report','wplms-braincert'); ?>" class="class-attendance"><i class="dashicons dashicons-text-page text-primary"></i></a>
                                </span>
                                
							<?php } ?>

							<?php if( $this->_can( 'view_record', $class ) ) { ?>
                            
								<span class="class_recording" data-id="<?php echo $class['class_id']; ?>" >
									<a title="<?php _e('View Recording','wplms-braincert'); ?>" class="recording" href="#"><i class="icon-play"></i></a>
								</span>
                                
							<?php } ?>

							<span class="time"><?php echo $this->date( $class[ 'start_ts' ], $timezone, $time_format  ); ?></span>
                            
							<span class="date"><?php echo $this->date( $class[ 'start_ts' ], $timezone, $date_format  ); ?></span> 
                            
							<span class="duration"><i class="fa fa-clock-o"> </i> <?php  printf( __( '%s Min.', 'wplms-braincert' ),  $class['duration']/60 ); ?></span>
                            
							<?php if( $this->_can( 'launch', $class )  ) { ?>
                            
                                <a title="<?php _e('Launch Class','wplms-braincert'); ?>" target="_blank" class="btn-sm btn-success instructor-preview" href="<?php echo $this->get_launch_link( $class['class_id']  ); ?>"><?php _e('Launch','wplms-braincert'); ?></a>
                                                         
							<?php }else if( $this->_can( 'join', $class ) ) { ?>
								
                                    <a title="<?php _e('Join Class','wplms-braincert'); ?>" target="_blank" class="learner-preview btn-sm btn-success" href="<?php echo $this->get_join_link( $class['class_id']  ); ?>"><?php _e('Join','wplms-braincert'); ?></a>
                                
							<?php }  ?>
                            
							<span class="<?php echo $class['status']; ?> status"><?php echo $statuses[ $class['status'] ]; ?></span>

						</span>
					</div>
					<div onclick="accordian_course(this)" data-id="<?php echo $class['class_id']; ?>" class="fa fa-plus fc-plus-icon"></div>
				</td>
			</tr>
			<tr class="course-lesson-<?php echo $class['class_id']; ?> hide course-accordian">
				<td class="curriculum-icon">
					<div>
						<span>
						<span class="accordian-content"><?php  _e('Class description : ','wplms-braincert'); ?></span>
						<span><?php echo $class['description']; ?></span>
						</span>
					</div>
					<div>
						<span>
						<span class="accordian-content"><?php  _e('Class timezone : ','wplms-braincert'); ?></span>
						<span><?php echo $timezones[$class['timezone']]; ?></span>
						</span>
					</div>
					<div>
						<span>
						<span class="accordian-content"><?php  _e('Class instructor : ','wplms-braincert'); ?></span>
						<span><?php echo bp_core_get_user_displayname( $class['instructor_id'] ); ?></span>
						</span>
					</div>
				</td>
			</tr>
		</table>
	</div>
</div>